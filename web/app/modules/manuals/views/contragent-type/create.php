<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\manuals\models\ContragentType */

$this->title = Yii::t('app', 'Create Contragent Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contragent Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contragent-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
