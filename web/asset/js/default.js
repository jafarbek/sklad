$(document).ready(function (event) {
    $('body').delegate('.view-modal-show', 'click', function(e) {
        e.preventDefault();
        let url=$(this).attr('href');
        $('.right-modal-all').html('');
        $('.right-modal-all').load(url);
        $('#kt_demo_panel_toggle').click();
    });
    $('input').change(function () {
        $('.click-button-ajax').click();
    });
    $('select').change(function () {
        $('.click-button-ajax').click();
    });

    $('.button-print').click(function () {
        $('.no-print').css('display', 'none');
        $('.no-print-flex').css('display', 'none');
        window.print();
        $('.no-print').css('display', 'block');
        $('.no-print-flex').css('display', 'flex');
        $('.d-flex').css('display', 'flex!important');
    });

   // $('.btn-sm-button').css({'padding':'0.3rem 0.3rem!important'});
});