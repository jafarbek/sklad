<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700',
        'asset/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.0.3',
        'asset/plugins/custom/jstree/jstree.bundle.css?v=7.0.3',

        'asset/plugins/global/plugins.bundle.css?v=7.0.3',
        'asset/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.3',
        'asset/css/style.bundle.css?v=7.0.3',
        'asset/css/themes/layout/header/base/light.css?v=7.0.3',
        'asset/css/themes/layout/header/menu/light.css?v=7.0.3',
        'asset/css/themes/layout/brand/dark.css?v=7.0.3',
        'asset/css/themes/layout/aside/dark.css?v=7.0.3',
//        'js/select2/select2-material.min.css',
        'js/select2/select2-krajee.min.css',
//        'js/select2/select2-krajee-bs4.min.css',
//        'js/select2/select2-default.min.css',
//        'js/select2/select2-classic.min.css',
//        'js/select2/select2-bootstrap.min.css',
//        'js/select2/select2-addl.min.css',
        // qo'shimcha css uchun
        'css/default.css',

    ];
    public $js = [
        'asset/js/default.js',
        'asset/plugins/global/plugins.bundle.js?v=7.0.3',
        'asset/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.3',
        'asset/js/scripts.bundle.js?v=7.0.3',
        'asset/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.0.3',
        'asset/js/pages/widgets.js?v=7.0.3',
        'asset/js/pages/crud/ktdatatable/base/html-table.js?v=7.0.3',
        'asset/js/pages/crud/forms/widgets/select2.js?v=7.0.3',
        //tree korinishini shakillantirish uchun
        "asset/js/pages/features/miscellaneous/treeview.js?v=7.0.3",
        'asset/plugins/custom/jstree/jstree.bundle.js?v=7.0.3',
        'asset/js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.6',



        'js/yii.activeForm.js',
        'js/yii.validation.js',
        'js/select2.js',
        'js/jquery.multipleInput.js',
    ];

    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
