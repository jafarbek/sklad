<?php
namespace app\models;

use app\components\behaviors\CommonBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_INACTIVE=2;
    const STATUS_SAVED=3;
    const STATUS_DELETED=4;
    const STATUS_PENDING=5;
    const IS_TRANSFER_RESIVING = 1;
    const IS_TRANSFER_OUTPUTING = 0;
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => CommonBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],

            ],
        ];
    }
    public function getfaol($status_id)
    {
        $faol=[1=>"Faol",2=>'Faol emas'];
        return $faol[$status_id];
    }
}