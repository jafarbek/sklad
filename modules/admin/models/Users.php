<?php

namespace app\modules\admin\models;

use app\models\BaseModel;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\base\Security;
/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fullname
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $address
 */
class Users extends BaseModel implements IdentityInterface
{
    public $item_name;
    public $check_password;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'username', 'password', 'address'], 'required'],
            [['address'], 'string'],
            [['status'], 'integer'],
            [['email'], 'email'],
            [['created_at','created_by','updated_at','updated_by'], 'integer'],
            [['fullname'], 'string', 'max' => 100],
            [['username', 'password', 'email'], 'string', 'length' => [4,60]],
            [['username','email'], 'unique'],

        ];
    }
    public function afterFind()
    {
        $id = $this->id;
        $itemName = AuthAssignment::find()->where(['user_id' => $id])->asArray()->all();
        $this->item_name = ArrayHelper::getColumn($itemName,'item_name');
        return parent::afterFind();
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fullname' => Yii::t('app', 'Full Name'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Email'),
            'status'=> Yii::t('app', 'Status'),
            'address' => Yii::t('app', 'Address'),

        ];
    }
    public function getUserRoles(){

    $depIds = self::find()
        ->select(['id'])
        ->where(['id' => $this->id])
        ->asArray()
        ->all();

    if(!empty($depIds)){

        $allIds = ArrayHelper::getColumn($depIds,'id');
        $departmentNames = AuthAssignment::find()
            ->select(['item_name'])
            ->where(['in','user_id', $allIds])
            ->asArray()
            ->all();
        $res = '';

        foreach ($departmentNames as $name){
            $res .= "<h6 class='badge badge-success' style='padding-bottom: 7px;'>{$name['item_name']}</h6>". " ";
        }

        return $res;
    }
    return null;
}
   public static function  getAssignment(){

        $model = AuthItem::find()->where(['type'=>1])->all();
        return ArrayHelper::map($model, 'name','name');
   }

    ////////////////////////////////////
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {

    }


    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }


    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        //  return $this->authKey;
    }


    public function validateAuthKey($authKey)
    {
        //  return $this->authKey === $authKey;
    }


    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public static function assignment2(){
        $client = AuthItem::find()->where(['type' => 1])
            ->asArray()
            ->all();
        return ArrayHelper::map($client, 'name' ,'name');
    }


}
