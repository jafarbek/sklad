<?php

namespace app\modules\admin\controllers;

use app\controllers\BaseController;
use app\modules\admin\models\AuthItemChild;
use Yii;
use app\modules\admin\models\AuthItem;
use app\modules\admin\models\AuthItemSearch;
use yii\db\Exception;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuthItemController implements the CRUD actions for AuthItem model.
 */
class AuthItemController extends BaseController
{
    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models = AuthItem::find()->where(['type' => 1])->all();
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'models' => $models
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->name]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $view = AuthItem::getAuthItemUniversal($id);
        if (!Yii::$app->request->post('AuthItem') && Yii::$app->request->post('role_name')){
            AuthItemChild::deleteAll(['parent' => Yii::$app->request->post('role_name')]);
            return $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post())) {
            $parent = Yii::$app->request->post('role_name');
            $children = Yii::$app->request->post('AuthItem');
            if (!empty($parent)){
                AuthItemChild::deleteAll(['parent' => $parent]);
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    foreach ($children as $child) {
                        $auth_item_child = new AuthItemChild();
                        $auth_item_child->parent = $parent;
                        $auth_item_child->child = $child;
                        if ($auth_item_child->save()) {
                            $saved = true;
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch(Exception $e){
                    Yii::info('Error Document'.$e->getMassage(),'save');
                }
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'view' => $view
        ]);
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
