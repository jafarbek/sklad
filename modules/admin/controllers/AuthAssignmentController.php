<?php

namespace app\modules\admin\controllers;

use app\controllers\BaseController;
use app\modules\admin\models\AuthItem;
use app\modules\admin\models\Users;
use Yii;
use app\modules\admin\models\AuthAssignment;
use app\modules\admin\models\AuthAssignmentSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuthAssignmentController implements the CRUD actions for AuthAssignment model.
 */
class AuthAssignmentController extends  BaseController
{

    /**
     * Lists all AuthAssignment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuthAssignmentSearch();
        $models =Yii::$app->db->createCommand("SELECT au.user_id, u.fullname, 
string_agg(concat('<span class=\"badge badge-success text-white\">',au.item_name,'</span>'),'&nbsp;') as item_name
FROM auth_assignment au
LEFT JOIN users u ON au.user_id::INTEGER = u.id
GROUP BY au.user_id,u.fullname;
	")->queryAll();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'models' => $models
        ]);
    }

    /**
     * Displays a single AuthAssignment model.
     * @param string $item_name
     * @param string $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($item_name, $user_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($item_name, $user_id),
        ]);
    }

    /**
     * Creates a new AuthAssignment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthAssignment();
        $users = Users::find()->all();
        $item = AuthItem::find()->where(['type' => 1])->all();
        $post = Yii::$app->request->post('AuthAssignment');
//        \yii\helpers\VarDumper::dump(Yii::$app->request->post(),10,true);exit();
        if (!empty($post) && Yii::$app->request->isPost) {
            try {
                $user_id = $post['user_id'];
                $item_name =  $post['item_child'];
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                foreach ($item_name as $name)
                {
                    $model = new AuthAssignment();
                    $model->setAttributes([
                        'user_id' => $user_id,
                        'item_name' => $name,
                    ]);
                    if ($model->save())
                    {
                        $saved = true;
                    }else{
                        $saved = false;
                        break;
                    }
                }
                if ($saved)
                {
                    $transaction->commit();
                    Yii::$app->session->setFlash("success",Yii::t('app',"Ma'lumot saqlandi"));
                    return $this->redirect(['index']);
                }else{
                    Yii::$app->session->setFlash('danger',Yii::t('app',"Ma'lumot saqlanmadi"));
                    $transaction->rollBack();
                }
            }catch(Exception $exception){
                Yii::info("Error AuthItemChild".$exception->getMessage(),'save');
            }
        }
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('create', [
                'model' => $model,
                'users' => $users,
                'item' => $item,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
            'users' => $users,
            'item' => $item,
        ]);
    }

    /**
     * Updates an existing AuthAssignment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $item_name
     * @param string $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($user_id)
    {

//        $model =Yii::$app->db->createCommand("SELECT au.user_id, u.fullname, string_agg(concat('<span class=\"badge badge-success\">',au.item_name,'</span>'),'&nbsp;') as item_name
//FROM auth_assignment au
//LEFT JOIN users u ON au.user_id::INTEGER = u.id WHERE au.user_id::INTEGER = {$user_id}
//GROUP BY au.user_id,u.fullname;
//	")->queryAll();
        $model = AuthAssignment::findOne(['user_id' => $user_id]);
        $users = Users::find()->all();
        $item = AuthItem::find()->where(['type' => 1])->all();
        $post = Yii::$app->request->post('AuthAssignment');
        if (!empty($post) && Yii::$app->request->isPost) {
            try {
                $user_id = $post['user_id'];
                $item_name =  $post['item_child'];
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $delete=Yii::$app->db->createCommand("DELETE FROM public.auth_assignment
	WHERE user_id::INTEGER={$user_id};")->queryAll();
                if ($delete)
                {
                    foreach ($item_name as $name)
                    {
                        $model = new AuthAssignment();
                        $model->setAttributes([
                            'user_id' => $user_id,
                            'item_name' => $name,
                        ]);
                        if ($model->save())
                        {
                            $saved = true;
                        }else{
                            $saved = false;
                            break;
                        }
                    }
                    if ($saved)
                    {
                        $transaction->commit();
                        Yii::$app->session->setFlash("success",Yii::t('app',"Ma'lumot saqlandi"));
                        return $this->redirect(['index']);
                    }else{
                        Yii::$app->session->setFlash('danger',Yii::t('app',"Ma'lumot saqlanmadi"));
                        $transaction->rollBack();
                    }
                }
            }catch(Exception $exception){
                Yii::info("Error AuthItemChild".$exception->getMessage(),'save');
            }
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'users' => $users,
            'item' => $item,
        ]);
    }

    /**
     * Deletes an existing AuthAssignment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $item_name
     * @param string $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRemove($user_id)
    {
        $model=Yii::$app->db->createCommand("DELETE FROM public.auth_assignment
	WHERE user_id::INTEGER={$user_id};")->queryAll();
        if ($model){
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the AuthAssignment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $item_name
     * @param string $user_id
     * @return AuthAssignment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($item_name, $user_id)
    {
        if (($model = AuthAssignment::findOne(['item_name' => $item_name, 'user_id' => $user_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
