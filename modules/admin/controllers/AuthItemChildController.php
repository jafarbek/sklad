<?php

namespace app\modules\admin\controllers;

use app\controllers\BaseController;
use app\modules\admin\models\AuthAssignment;
use app\modules\admin\models\AuthItem;
use Yii;
use app\modules\admin\models\AuthItemChild;
use app\modules\admin\models\AuthItemChildSearch;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * AuthItemChildController implements the CRUD actions for AuthItemChild model.
 */
class AuthItemChildController extends BaseController
{

    /**
     * Lists all AuthItemChild models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models = AuthItemChild::find()->all();
        $mod = Yii::$app->db->createCommand("SELECT parent, string_agg(concat('<p class=\"badge badge-success text-white\">',child,'</p>'),'&nbsp;') as child
	FROM public.auth_item_child GROUP BY parent;")->queryAll();
//        $mode = Yii::$app->db->createCommand("SELECT parent,GROUP_CONCAT(child,SEPARATOR ',')
//	FROM public.auth_item_child GROUP BY parent")->queryAll();
        $searchModel = new AuthItemChildSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'models' => $models,'mod' => $mod
        ]);
    }

    /**
     * Displays a single AuthItemChild model.
     * @param string $parent
     * @param string $child
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($parent, $child)
    {
        return $this->render('view', [
            'model' => $this->findModel($parent, $child),
        ]);
    }

    /**
     * Creates a new AuthItemChild model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItemChild();
        $parent =AuthItem::find()->where(['type'=>1])->all();
        $child = AuthItem::find()->asArray()->all();
        $post = Yii::$app->request->post('AuthItemChild');
        if (Yii::$app->request->isPost)
        {
            try {
                $transaction = Yii::$app->db->beginTransaction();
//                echo "<pre>";
//                print_r($post);
//                echo "</pre>";die();
                $saved = false;
                foreach ($post['child_array'] as $item){
                    $model = new AuthItemChild();
                    $model->setAttributes([
                        'parent' => $post['parent'],
                        'child' => $item,
                    ]);
                    if ($model->save()){
                        $saved = true;
                    }else{
                        $saved = false;
                        break;
                    }
                }
                if ($saved){
                    $transaction->commit();
                    Yii::$app->session->setFlash("success",Yii::t('app',"Ma'lumot saqlandi"));
                    return $this->redirect(['index']);
                }else{
                    Yii::$app->session->setFlash('danger',Yii::t('app',"Ma'lumot saqlanmadi"));
                    $transaction->rollBack();
                }
            }catch(Exception $exception){
                Yii::info("Error AuthItemChild".$exception->getMessage(),'save');
            }
        }
        if (Yii::$app->request->isAjax)
        {
            return $this->renderAjax('create', [
                'model' => $model,'parent' => $parent,'child' => $child
            ]);
        }
        return $this->render('create', [
            'model' => $model,'parent' => $parent,'child' => $child
        ]);
    }

    /**
     * Updates an existing AuthItemChild model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $parent
     * @param string $child
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($parent)
    {
        $model = AuthItemChild::findOne(['parent' => $parent]);
        $parent =AuthItem::find()->where(['type'=>1])->asArray()->all();
        $child = AuthItem::find()->asArray()->all();
        $post = Yii::$app->request->post('AuthItemChild');
        if (!empty($post) && Yii::$app->request->isPost) {
//            \yii\helpers\VarDumper::dump($post,10,true);exit();
            try {
                $parent = $post['parent'];
                $child_array =  $post['child_array'];
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $delete=Yii::$app->db->createCommand("DELETE FROM public.auth_item_child
	WHERE parent='{$parent}';")->queryAll();
                if ($delete)
                {
                    foreach ($child_array as $name)
                    {
                        $model = new AuthItemChild();
                        $model->setAttributes([
                            'parent' => $parent,
                            'child' => $name,
                        ]);
                        if ($model->save())
                        {
                            $saved = true;
                        }else{
                            $saved = false;
                            break;
                        }
                    }
                    if ($saved)
                    {
                        $transaction->commit();
                        Yii::$app->session->setFlash("success",Yii::t('app',"Ma'lumot saqlandi"));
                        return $this->redirect(['index']);
                    }else{
                        Yii::$app->session->setFlash('danger',Yii::t('app',"Ma'lumot saqlanmadi"));
                        $transaction->rollBack();
                    }
                }
            }catch(Exception $exception){
                Yii::info("Error AuthItemChild".$exception->getMessage(),'save');
            }
        }

        return $this->renderAjax('update', [
            'model' => $model,'parent' => $parent,'child'=> $child
        ]);
    }

    /**
     * Deletes an existing AuthItemChild model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $parent
     * @param string $child
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($parent, $child)
    {
        $this->findModel($parent, $child)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItemChild model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $parent
     * @param string $child
     * @return AuthItemChild the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($parent, $child)
    {
        if (($model = AuthItemChild::findOne(['parent' => $parent, 'child' => $child])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['massage'] = 'Error';
        $parent = Yii::$app->request->get('parent');
        if(!empty($parent)){
            $sql = "SELECT name FROM auth_item WHERE name not in ('{$parent}') ";
//            $sql = sprintf($sql,$parent);
            $itemChild = Yii::$app->db->createCommand($sql)->queryAll();
            if (!empty($itemChild)){
                $response['status'] = true;
                $response['data'] = $itemChild;
                $response['massage'] = "OK";
            }else{
                $response['massage'] = 'Empty';
            }
        }
        return $response;
    }
}
