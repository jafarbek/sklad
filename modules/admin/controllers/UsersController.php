<?php

namespace app\modules\admin\controllers;

use app\controllers\BaseController;
use app\models\User;
use app\modules\admin\models\AuthAssignment;
use Yii;
use app\modules\admin\models\Users;
use app\modules\admin\models\AuthItem;
use app\modules\admin\models\UsersSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends BaseController
{
    public function actionValidate()
    {
        $model = new Users();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $users = Users::find()->orderBy(['id'=>SORT_DESC])->all();
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users'=>$users,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Users model.
     * If creation is successful,
     * the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         $model = new Users();
         $models = new AuthAssignment();
         $post = Yii::$app->request->post();
        if (Yii::$app->request->isPost) {
            try {
                $transaction = Yii::$app->db->beginTransaction();
            if ($model->load(Yii::$app->request->post())) {
                $model->setPassword($model->password);
                $model->save();
                $modelId = $model->id;
                $AuthAssignment = $post['Users']['item_name'];
                if (!empty($AuthAssignment)) {
                        $saved = true;
                        foreach ($AuthAssignment as $items) {
                            $add = new AuthAssignment();
                            $add->item_name = $items;
                            $add->user_id = $modelId;
                            if ($add->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }

                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', 'Ma\'lumotlar saqlandi!');
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', 'Ma\'lumotlar saqlanmadi!');
                        }
                    }
                    return $this->redirect(['index']);
                }
            }catch (\Exception $e) {
                Yii::info('Error Data' . $e->getMessage(), 'save');
            }
        }
        if (Yii::$app->request->isAjax){
           return $this->renderAjax('create', [
                'model' => $model,
                'models' => $models,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
            'models' => $models,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isPost) {

                if ($model->load(Yii::$app->request->post())) {
                    if ((empty($post['new_password'])!=1)){
                        $model->password = Yii::$app->getSecurity()->generatePasswordHash($post['new_password']);
                    }
                    $model->save();
                    $modelId = $model->id;
                    $AuthAssignment = $post['Users']['item_name'];
                    try {
                        $transaction = Yii::$app->db->beginTransaction();
                if (!empty($AuthAssignment)) {
                    AuthAssignment::deleteAll(['user_id' => $modelId]);
                        $saved = true;
                        foreach ($AuthAssignment as $items) {
                            $add = new AuthAssignment();
                            $add->item_name = $items;
                            $add->user_id = $modelId;
                            if ($add->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }

                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', 'Ma\'lumotlar saqlandi!');
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', 'Ma\'lumotlar saqlanmadi!');
                        }
                    }
                    return $this->redirect(['index']);
                }catch (\Exception $e) {
                        Yii::info('Error Data' . $e->getMessage(), 'save');
                    }
            }
        }
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        AuthAssignment::deleteAll(['user_id'=>$id]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionUsername(){

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $id = Yii::$app->request->get('id');
        $username = Yii::$app->request->get('username');
        if ($id){
            $users = Users::find()->where(['username'=>$username])->andWhere(['not in','id', $id])->all();
        }else{
            $users = Users::find()->where(['username'=>$username])->all();
        }
        $response['message'] ;
        if(!empty($username) || !empty($email)){
            if (!empty($users)){
                $response['status'] = true;
                $response['data'] = $users;
                $response['message'] = 'OK';
            }
            else{
                $response['message'] = 'Empty';
                $response['data'] = Yii::$app->request->get();
            }
        }
        return $response;
    }
    public function actionEmail(){

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $id = Yii::$app->request->get('id');
        $email = Yii::$app->request->get('email');
        if ($id){
            $users = Users::find()->where(['email'=>$email])->andWhere(['not in','id', $id])->all();
        }else{
            $users = Users::find()->where(['email'=>$email])->all();
        }
        $response['message'] ;
        if(!empty($username) || !empty($email)){
            if (!empty($users)){
                $response['status'] = true;
                $response['data'] = $users;
                $response['message'] = 'OK';
            }
            else{
                $response['message'] = 'Empty';
                $response['data'] = Yii::$app->request->get();
            }
        }
        return $response;
    }
}
