<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\UserDepartment;
use app\modules\admin\models\UserDepartmentSearch;
use app\controllers\BaseController;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserDepartmentController implements the CRUD actions for UserDepartment model.
 */
class UserDepartmentController extends BaseController
{
    /**
     * Lists all UserDepartment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserDepartmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserDepartment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new NotFoundHttpException(Yii::t('app','Object not found'));
        }
    }

    /**
     * Creates a new UserDepartment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $model = new UserDepartment();
        if(Yii::$app->request->isPost){
            $data = Yii::$app->request->post();
            $user_id = $data['UserDepartment']['user_id'];
            $isAllSaved = false;
            if(!empty($data['UserDepartment']['department_receiving'])){
                foreach ($data['UserDepartment']['department_receiving'] as $departmentId){
                    $isAllSaved = false;
                    $modelLoop = new UserDepartment();
                    $modelLoop->user_id = $user_id;
                    $modelLoop->department_id = $departmentId;
                    $modelLoop->is_transfer =  $model::DEPARTMENT_RECEIVING;
                    if (empty(UserDepartment::find()->where(['user_id' => $user_id, 'department_id' => $departmentId, 'is_transfer' => $model::DEPARTMENT_RECEIVING])->asArray()->all())) {
                        if ($modelLoop->save()) {
                            $isAllSaved = true;
                            unset($modelLoop);
                        }
                    }
                }
            }
            if(!empty($data['UserDepartment']['department_outputting'])){
                foreach ($data['UserDepartment']['department_outputting'] as $departmentId){
                    $isAllSaved = false;
                    $modelLoop = new  UserDepartment();
                    $modelLoop->user_id = $user_id;
                    $modelLoop->department_id = $departmentId;
                    $modelLoop->is_transfer =  $model::DEPARTMENT_OUTPUTTING;
                    if (empty(UserDepartment::find()->where(['user_id' => $user_id, 'department_id' => $departmentId, 'is_transfer' => $model::DEPARTMENT_OUTPUTTING])->asArray()->all())) {
                        if ($modelLoop->save()) {
                            $isAllSaved = true;
                            unset($modelLoop);
                        }
                    }
                }
            }
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserDepartment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = UserDepartment::findOne(['user_id' => $id]);

        if(Yii::$app->request->isPost){
            $data = Yii::$app->request->post();
            UserDepartment::deleteAll(['user_id' => $data['UserDepartment']['user_id']]);
            $user_id = $data['UserDepartment']['user_id'];
            $isAllSaved = false;
            if(!empty($data['UserDepartment']['department_receiving'])){
                foreach ($data['UserDepartment']['department_receiving'] as $departmentId){
                    $isAllSaved = false;
                    $modelLoop = new UserDepartment();
                    $modelLoop->user_id = $user_id;
                    $modelLoop->department_id = $departmentId;
                    $modelLoop->is_transfer =  $model::DEPARTMENT_RECEIVING;
                    if ($modelLoop->save()) {
                        $isAllSaved = true;
                        unset($modelLoop);
                    }
                }
            }
            if(!empty($data['UserDepartment']['department_outputting'])){
                foreach ($data['UserDepartment']['department_outputting'] as $departmentId){
                    $isAllSaved = false;
                    $modelLoop = new  UserDepartment();
                    $modelLoop->user_id = $user_id;
                    $modelLoop->department_id = $departmentId;
                    $modelLoop->is_transfer =  $model::DEPARTMENT_OUTPUTTING;
                    if ($modelLoop->save()) {
                        $isAllSaved = true;
                        unset($modelLoop);
                    }
                }
            }
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }else{
            throw new NotFoundHttpException(Yii::t('app','Object not found'));
        }
    }

    /**
     * Deletes an existing UserDepartment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the UserDepartment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserDepartment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserDepartment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
