<?php

namespace app\modules\admin\controllers;

use Yii;
/**
 * Default controller for the `admin` module
 */
class DefaultController extends AppController
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionLogin(){
        return $this->render('login');
    }

    public function actionIndex()
    {
        return $this->redirect(['/site/index']);
    }

    public function actionSelectLang($lang)
    {
        $lang =Yii::$app->request->get('lang');
        Yii::$app->session->set('lang',$lang);
        Yii::$app->language = $lang;
        return $this->redirect(Yii::$app->request->referrer);
    }
}
