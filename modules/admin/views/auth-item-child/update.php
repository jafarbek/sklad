<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AuthItemChild */
/* @var $parent app\modules\admin\models\AuthItem */
/* @var $child app\modules\admin\models\AuthItem */

$this->title = Yii::t('app', 'Update Auth Item Child: {name}', [
    'name' => $model->parent,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Auth Item Children'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->parent, 'url' => ['view', 'parent' => $model->parent, 'child' => $model->child]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="auth-item-child-update">

    <?= $this->render('_form', [
        'model' => $model,'parent' => $parent,'child' =>$child
    ]) ?>

</div>
