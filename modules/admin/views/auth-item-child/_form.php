<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AuthItemChild */
/* @var $form yii\widgets\ActiveForm */
/* @var $parent */
/* @var $child */
?>
    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'parent')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map($parent,'name','name'),
        'options' => ['placeholder' => 'Select a state ...'],//,'multiple'=>true
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'child_array')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map($child,'name','name'),
        'options' => ['placeholder' => 'Select a state ...','multiple'=>true],//,'multiple'=>true
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-outline-primary btn-sm']) ?>

    <?php ActiveForm::end(); ?>

<?php
$url = \yii\helpers\Url::to(['ajax']);

$js = <<<JS

$(document).ready(function(e) {
    $('.modal-title').html('{$this->title}');
});
    $(document).ready(function() {
      $('#authitemchild-parent').change(function() {
       let parent = $(this).val();
       console.log(parent);
       $.ajax({
            type:"GET",
            data:{parent:parent},
            url:'$url',
            success:function(response) {
                if (response.status)
                    {
                        $("#authitemchild-child_array").html("");
                        response.data.map(function(item) {
                          let option = new Option(item.name);
						  option.setAttribute('value', item.name);
						  $("#authitemchild-child_array").append(option);
                        })
                    }
            }
       })
      })
    })
JS;
$this->registerJs($js);
?>
