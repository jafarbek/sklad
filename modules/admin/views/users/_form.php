<?php

use app\modules\admin\models\AuthItem;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

//use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Users */
/* @var $form yii\widgets\ActiveForm */
/* @var $models */
/* @var $password */
?>

<?php $form = ActiveForm::begin([
    'id' => $model->formName(),
    'method' => 'post',
//     'enableAjaxValidation' => true,
//     'validationUrl' => Url::toRoute('users/validate'),
]); ?>

<?= $form->field($model, 'id')->hiddenInput(['maxlength' => true,'id'=>'id'])->label(false)?>
<?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'username')->textInput(['maxlength' => true,'id'=>'username']) ?>
<span class="offcanvas user_info" style="color: red"><?=Yii::t('app', 'This username has already been taken.');?></span>

<?php
if ($password == true) {
    ?>
    <a class="change_password" style="color: #0a73bb"><?=Yii::t('app', 'Change password');?></a><br>
    <label for="new_password" class="offcanvas label_pasword"><?=Yii::t('app', 'New password');?></label>
    <input type="password" id="new_password" class="offcanvas form-control" name="new_password"><br>

    <?php
} else { ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

<?php } ?>
<?= $form->field($model, 'email')->textInput(['id'=>'email','type'=>'email']) ?>
<span class="offcanvas user_email" style="color: red"><?=Yii::t('app', 'This email has already been taken.');?></span>

<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'item_name')->widget(Select2::classname(), [
    'data' => $model::getAssignment(),
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' =>true,
        'prompt' =>Yii::t('app', 'Select'),
    ],
])->label('Rules'); ?>
<?= $form->field($model, 'status')->dropDownList([
    $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
    $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
]); ?>


<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary save_username']) ?>
<?php ActiveForm::end(); ?>
<br>

<?php
$url = Url::to(['username']);
$url1 = Url::to(['email']);
$js = <<< JS
$('body').delegate('.change_password', 'click', function(e) {
    e.preventDefault();
    $('#new_password').removeClass('offcanvas');
    $('.label_pasword').removeClass('offcanvas');
})
$(document).ready(function(e) {
        $('.modal-title').html('{$this->title}');
        $('.help-block').css({'color':'red'});
        
         
        $('#username').on('blur',function() {
           
          let  username = $(this).val(); 
          let  id = $('#id').val();
                $.ajax({
                    url:'{$url}',
                    type: 'GET',
                    data:{username:username,id:id},
                    success : function(response) {
                      if (response.status){
                          $('.save_username').prop('disabled',true);
                          $('.user_info').removeClass('offcanvas');
                      }else{
                          $('.save_username').prop('disabled',false);
                          $('.user_info').addClass('offcanvas');
                      }
                    }
                    
                })
        });
        $('#email').on('blur',function() {
           
          let  email = $(this).val(); 
          let  id = $('#id').val();
                $.ajax({
                    url:'{$url1}',
                    type: 'GET',
                    data:{email:email,id:id},
                    success : function(response) {
                      if (response.status){
                          $('.save_username').prop('disabled',true);                                
                          $('.user_email').removeClass('offcanvas');
                      }else{
                           $('.save_username').prop('disabled',false);
                          $('.user_email').addClass('offcanvas');
                      }
                    }
                    
                })
        });
        
});
JS;
$this->registerJs($js);

$css = <<<CSS
        .select2-container--krajee .select2-selection--multiple .select2-selection__choice{
           background-color: #0a73bb;
           color: white;
        }
       
CSS;
$this->registerCss($css)

?>

