<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Users */
/* @var $models app\modules\admin\models\AuthAssignment */

$this->title = Yii::t('app', 'Create user');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= $this->render('_form', [
        'model' => $model,
        'models' =>$models,
        'password' => false
    ]) ?>


<?php
$js = <<< JS
$(document).ready(function(e) {
        $('.modal-title').html('{$this->title}');
});
JS;
$this->registerJs($js)
?>