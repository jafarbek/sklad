<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
/* @var $users */
/* @var $item */
?>
    <!--begin::Form-->
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map($users,'id','fullname'),
        'size' => Select2::SMALL,
        'options' => ['placeholder' => 'Select a state ...'],//,'multiple'=>true
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'item_child')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map($item,'name','name'),
        'size' => Select2::SMALL,
        'options' => ['placeholder' => 'Select a state ...','multiple'=>true],//,'multiple'=>true
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary']) ?>
    <?php ActiveForm::end(); ?>
<?php
$js = <<<JS
$(document).ready(function(e) {
    $('.modal-title').html('{$this->title}');
});
JS;

$this->registerJs($js);

?>
