<?php

use app\modules\admin\models\Users;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\manuals\models\License */

$this->title = Yii::t('app', 'Licences: {name}', [
    'name' => $model->serial_number,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Licenses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'serial_number',
                'serial',
                [
                    'attribute' => 'department_id',
                    'label' => Yii::t('app', 'Department'),
                    'value' => function ($mod) {
                        $til = 'name_' . Yii::$app->language;
                        return $mod->department->$til;
                    }
                ],
                'order_number',
                [
                    'attribute' => 'item_category_id',
                    'label' => Yii::t('app', 'Item Category'),
                    'value' => function ($mod) {
                        $til = 'name_' . Yii::$app->language;
                        return $mod->itemCategory->$til;
                    }
                ],

                'order_date',
                'given_date',
                'expiration_date',
                'responsible',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if ($model->status == $model::STATUS_ACTIVE) {
                            return "<span class='badge badge-info'>Active</span>";
                        } else if ($model->status == $model::STATUS_INACTIVE){
                            return "<span class='badge badge-warning'>Inactive</span>";
                        }
                    }
                ],
                [
                    'attribute' => 'created_by',
                    'value' => function ($model) {
                        return (Users::findOne($model->created_by))
                            ? Users::findOne($model->created_by)->fullname . "<br><small>
                    <i style='font-size: small; color: green'>" . date('d.m.Y H:i', $model->created_at) . "</i></small>"
                            : $model->created_by;
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'updated_by',
                    'value' => function ($model) {
                        return (Users::findOne($model->updated_by))
                            ? Users::findOne($model->updated_by)->fullname . "<br><small>
                    <i style='font-size: small; color: red'>". date('d.m.Y H:i', $model->updated_at) . "</i></small>"
                            : $model->updated_by;
                    },
                    'format' => 'raw'
                ],

            ],
        ]) ?>
    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-sm btn-outline-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>


<?php
$js = <<<JS
$(document).ready(function(e) {
    $('.modal-title').html('{$this->title}');
});
JS;

$this->registerJs($js);

?>
