<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\structure\models\Regions */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'id' => $model->formName(),
    'method' => 'post',
    'enableAjaxValidation' => true,
    'validationUrl' =>Url::toRoute('regions/validate'),
]); ?>

    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->label(false)->hiddenInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([
        $model::STATUS_ACTIVE => Yii::t('app','Active'),
        $model::STATUS_INACTIVE => Yii::t('app','Inactive')
    ]) ?>

        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-outline-success btn-sm']) ?>

    <?php ActiveForm::end(); ?>


<?php
$js = <<< JS
$(document).ready(function(e) {
    $('.modal-title3').html('{$this->title}');
}); $('.help-block').css({'color':'red'});

JS;
$this->registerJs($js)
?>