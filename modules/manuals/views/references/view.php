<?php

use app\modules\admin\models\Users;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\manuals\models\References */

$this->title = $model['name_'.Yii::$app->language];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'References'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        //'id',
        'name_uz',
        'name_ru',
        'name_en',
        'token',
        'sort',
        'references_type_id',
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => function ($model) {
                if ($model->status == $model::STATUS_ACTIVE) {
                    return "<span class='badge badge-info'>Active</span>";
                } else if ($model->status == $model::STATUS_INACTIVE) {
                    return "<span class='badge badge-warning'>Inactive</span>";
                }
            }
        ],
        [
            'attribute' => 'created_by',
            'value' => function ($model) {
                return (Users::findOne($model->created_by))
                    ? Users::findOne($model->created_by)->fullname . "<br><small>
                    <i style='font-size: small; color: green'>" . date('d.m.Y H:i', $model->created_at) . "</i></small>"
                    : $model->created_by;
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'updated_by',
            'value' => function ($model) {
                return (Users::findOne($model->updated_by))
                    ? Users::findOne($model->updated_by)->fullname . "<br><small>
                    <i style='font-size: small; color: red'>". date('d.m.Y H:i', $model->updated_at) . "</i></small>"
                    : $model->updated_by;
            },
            'format' => 'raw'
        ],
    ],
]) ?>
<p>
    <?= Html::a(Yii::t('app', 'Delete'), ['remove', 'id' => $model->id], [
        'class' => 'btn btn-sm btn-outline-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>

<?php
$js = <<<JS
$(document).ready(function(e) {
    $('.modal-title').html('{$this->title}');
});
JS;

$this->registerJs($js);

?>
