<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\manuals\models\References */

$this->title = Yii::t('app', 'Update: {name}', [
    $til = "name_".Yii::$app->language,
    'name' => $model->$til,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'References'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="references-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
