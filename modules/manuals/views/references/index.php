<?php

use yii\helpers\Html;
use yii\grid\GridView;

$i = 0;
/* @var $this yii\web\View */
/* @var \app\modules\manuals\models\ReferencesSearch;
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models */

$this->title = Yii::t('app', 'References');

$unit = strtolower($model[0]['name_uz']);
if ($unit) {
    $this->params['breadcrumbs'][] = $this->title = strtoupper($unit);
} else {
    $this->params['breadcrumbs'][] = $this->title;
}

?>
<!-- Modal begin -->
<div class="modal fade" id="exampleModalCustomScrollable" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div data-scroll="false" data-height="400">
                <span class="modal-content-span">

                </span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal End   -->
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <a href="<?= \yii\helpers\Url::to(['references-type/index']) ?>"
                       class="btn btn-sm btn-success font-weight-bolder">
                        <span class="svg-icon svg-icon-md">
                        <svg xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        </span>
                        <?= Yii::t('app', 'Back') ?></a>
                    <div class="input-icon pl-5">
                        <input type="text" class="form-control" placeholder="Search..."
                               id="kt_datatable_search_query"/>
                        <span>
                            <i class="flaticon2-search-1 text-muted pl-6"></i>
                        </span>
                    </div>
                </div>
                <div class="card-toolbar">
                    <!--begin::Dropdown-->
                    <!--end::Dropdown-->
                    <!--begin::Button-->
                    <button href="<?= \yii\helpers\Url::to(['create', 'references_type_id' => $model[0]['references_type_id']]) ?>" class="btn btn-sm btn-primary font-weight-bolder view-modal-show">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span><?=Yii::t('app', 'Create')?></button>

                    <!--end::Button-->
                </div>
            </div>
            <br><div class="separator separator-solid"></div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table-bordered datatable datatable-bordered datatable-head-custom table-hover" id="kt_datatable">
                    <thead>
                    <tr>
                        <th title="id">#</th>
                        <th title="Name"><?= Yii::t('app', 'Name') ?></th>
                        <th title="Name"><?= Yii::t('app', 'Code') ?></th>
                        <th title="Actions"><?= Yii::t('app', 'Sort') ?></th>
                        <th><?= Yii::t('app', 'Act') ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <? if (!empty($models)) { ?>
                        <?php foreach ($models as $key): ?>
                            <tr>
                                <td><?= ++$i; ?></td>
                                <td><?= $key["name_".Yii::$app->language ] ?></td>
                                <td><?= $key['token'] ?></td>
                                <td><?= $key['sort'] ?></td>

                                <td>
                                    <a
                                       href="<?= \yii\helpers\Url::to(['update', 'id' => $key['id']]); ?>"
                                       class="btn btn-icon btn-xs btn-outline-primary update-one">
                                        <i class="la la-pencil-square-o"></i></a>
                                    <a
                                       href="<?= \yii\helpers\Url::to(['view', 'id' => $key['id']]); ?>"
                                       class="btn btn-icon btn-xs btn-outline-info view-one">
                                        <i class="la la-eye"></i></a>
                                    <?php
                                    echo Html::a('<i class="la la-trash-restore-alt"></i>', \yii\helpers\Url::to(['references/remove', 'id' => $key['id']]), [
                                        'class' => "btn btn-icon btn-xs btn-outline-danger",
                                        'data' => [
                                            'method' => 'post',
                                        ],
                                    ]);
                                    ?>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    <? } ?>
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
<button class="offcanvas" id="kt_demo_panel_toggle">sa</button>
<?php
$js = <<<JS
$('body').delegate('.update-one', 'click', function(e) {
    e.preventDefault();
    let url=$(this).attr('href');
    $('#kt_demo_panel_toggle').click(); 
    $('.right-modal-all').load(url); 
});
$('body').delegate('.view-one', 'click', function(e) {
    e.preventDefault();
    let url=$(this).attr('href');     
    $('#kt_demo_panel_toggle').click(); 
    $('.right-modal-all').load(url); 
});
JS;
$this->registerJs($js);

?>


