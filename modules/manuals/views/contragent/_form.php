<?php

use app\modules\manuals\models\Contragent;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\manuals\models\Contragent */
/* @var $form yii\widgets\ActiveForm */
/* @var $type_id */
?>

        <?php

        $form = ActiveForm::begin([
            'id' => $model->formName(),
            'method' => 'post',
//            'enableAjaxValidation' => true,
//            'validationUrl' =>Url::toRoute('contragent/validate'),
        ]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'add_info')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?>

         <?= $form->field($model, 'references_type_id')->widget(\kartik\select2\Select2::classname(), [
            'data' => Contragent::getReferencesTypeContragent(),
            'options' => ['placeholder' => Yii::t('app', 'Select')],
            'pluginOptions' => [
                'allowClear' => true,
              //  'prompt' =>Yii::t('app', 'Select'),
            ],

        ]); ?>
        <?= $form->field($model, 'status')->dropDownList([
            $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
            $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
<?php
$js = <<<JS
$(document).ready(function(e) {
    $('.modal-title').html('{$this->title}');
});
JS;

$this->registerJs($js);

?>
