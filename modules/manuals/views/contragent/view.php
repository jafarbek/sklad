<?php

use app\modules\admin\models\Users;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\manuals\models\Contragent */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contragents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="contragent-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'short_name',
            'add_info',
            'address',
            'director',
            'tel',
            [
                'attribute' => 'references_type_id',
                'label' =>Yii::t('app', 'Contragent'),
                'value' => function ($model){
                    $til ="name_".Yii::$app->language;
                    return $model->references['name_'.Yii::$app->language];
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->status == $model::STATUS_ACTIVE) {
                        return "<span class='badge badge-info'>Active</span>";
                    } else if ($model->status == $model::STATUS_INACTIVE) {
                        return "<span class='badge badge-warning'>Inactive</span>";
                    }
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    return !empty(Users::findOne($model->created_by))
                        ? Users::findOne($model->created_by)->fullname . "<br><small>
                    <p style='font-size: small; color: green'>" . date('d.m.Y H:i', $model->created_at) . "</p></small>"
                        : $model->created_by;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'updated_by',
                'value' => function ($model) {
                    return (Users::findOne($model->updated_by))
                        ? Users::findOne($model->updated_by)->fullname . "<br><small>
                    <p style='font-size: small; color: red'>". date('d.m.Y H:i', $model->updated_at) . "</p></small>"
                        : $model->updated_by;
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>
    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-sm btn-outline-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
<?php
$js = <<<JS
$(document).ready(function(e) {
    $('.modal-title').html('{$this->title}');
});
JS;

$this->registerJs($js);

?>