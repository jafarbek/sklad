<?php

use app\modules\manuals\models\Contragent;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manuals\models\ContragentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contragents');
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">

            </div>
            <div class="card-toolbar">
                <!--begin::Dropdown-->
                <!--begin::Button-->

                <button href="<?= \yii\helpers\Url::to(['contragent/create']) ?>" class="btn btn-sm btn-primary font-weight-bolder create-click">
                        <span class="svg-icon svg-icon-md">
                        <svg xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span><?=Yii::t('app', 'Create')?></button>

                <!--end::Button-->
            </div>
        </div>
        <div class="separator separator-solid"></div>
        <div class="card-body">
            <? $form = \yii\bootstrap\ActiveForm::begin(); ?>
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => '',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    'short_name',
                    'add_info',
                    'address',
                    'director',
                    'tel',
                    [
                        'attribute' => 'references_type_id',
                        'filter' => Contragent::getReferencesTypeContragent(),
                        'label' => Yii::t('app', 'Contragent'),
                        'options' => [
                            'width' => '130px;'
                        ],
                        'value' => function ($model) {
                            $til = "name_" . Yii::$app->language;
                            return $model->references->$til;
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'options' => [
                            'width' => '100px;'
                        ],
                        'filter' => [
                            0 => Yii::t('app', 'Inactive'),
                            1 => Yii::t('app', 'Active'),
                        ],
                        'value' => function ($model) {
                            if ($model->status == $model::STATUS_ACTIVE) {
                                return "<span class='badge badge-info'>Active</span>";
                            } else if ($model->status == $model::STATUS_INACTIVE) {
                                return "<span class='badge badge-warning'>Inactive</span>";
                            }
                        }
                    ],
                    //'created_at',
                    //'created_by',
                    //'updated_at',
                    //'updated_by',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'contentOptions' => ['style' => 'width:90px;'],
                        'header' => Yii::t('app', "Action"),
                        'headerOptions' => [
                            'style' => 'width:90px',
                        ],
                        'buttons' => [
                            'view' => function ($url, $data) {
                                return '<a href="' . $url . '" class="btn btn-outline-info btn-xs view-modal-show"><i class="la la-eye ml-1"></i></a>';
                            },
                            'delete' => function ($url, $data) {
                                return '<a href="' . $url . '" class="btn btn-outline-danger btn-xs" ><i class="la la-trash ml-1"></i></a>';
                            },
                            'update' => function ($url, $data) {
                                return '<a href="' . $url . '" class="btn btn-outline-primary btn-xs click-update"><i class="la la-pencil ml-1"></i></a>';
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>
            <input type="submit" class="offcanvas click-button-ajax">
            <? \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
<button id="kt_demo_panel_toggle" class="offcanvas"></button>
<?php
$js = <<<JS
  $('.create-click').click(function(e) {
        e.preventDefault();
        let url = $(this).attr('href');
        $('#kt_demo_panel_toggle').click();
        $('.right-modal-all').html('');
        $('.right-modal-all').load(url);
  });

    $('.click-update').click(function(e) {
        e.preventDefault();
        let url = $(this).attr('href');
        $('#kt_demo_panel_toggle').click();
        $('.right-modal-all').html('');
        $('.right-modal-all').load(url);
    });

JS;
$this->registerJs($js, \yii\web\View::POS_END);

