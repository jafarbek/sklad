<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\manuals\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'id' => $model->formName(),
    'method' => 'post',
    'enableAjaxValidation' => true,
    'validationUrl' => Url::toRoute('countries/validate'),
]); ?>

<?= $form->field($model, 'code')->textInput(['maxlength' => true])   ?>

<?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'status')->dropDownList([
    $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
    $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
]); ?>


<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-success']) ?>

<?php ActiveForm::end(); ?>
<?php
$js = <<<JS
$(document).ready(function(e) {
    $('.modal-title').html('{$this->title}');
    $('.help-block').css({'color':'red'});
});
JS;

$this->registerJs($js);

?>
