<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\modules\manuals\models\ExchangeRate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exchange-rate-form">

    <?php $form = ActiveForm::begin([
        'id' => $model->formName(),
        'method' => 'post',
        'enableAjaxValidation' => true,
        'validationUrl' =>Url::toRoute('exchange-rate/validate'),
    ]); ?>

    <?= $form->field($model, 'currency_id')->widget(Select2::classname(), [
        'data' => $model::getCurrencyItems(),
        'options' => ['placeholder' => 'Select a currency ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'to_currency_id')->widget(Select2::classname(), [
        'data' => $model::getCurrencyItems(),
        'options' => ['placeholder' => 'Select a currency ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'to_amount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$js = <<<JS
$(document).ready(function(e) {
    $('.modal-title').html('{$this->title}');
    $('.help-block').css({'color':'red'});
});
JS;

$this->registerJs($js);

