<?php

namespace app\modules\manuals\controllers;

use app\modules\warehouse\models\ItemCategory;
use Yii;
use app\modules\manuals\models\License;
use app\modules\manuals\models\LicenseSearch;
use app\controllers\BaseController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LicenseController implements the CRUD actions for License model.
 */
class LicenseController extends BaseController
{

    /**
     * Lists all License models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new LicenseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single License model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new License model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new License();
        $data = Yii::$app->request->post();
        if ($model->load($data)) {
            $model->order_date = date('Y-m-d', strtotime($model->order_date));
            $model->given_date = date('Y-m-d', strtotime($model->given_date));
            $model->expiration_date = date('Y-m-d', strtotime($model->expiration_date));
            if ($model->save()){
                return $this->redirect(['index']);
            }

        }

        return $this->render('create', [
            'model' => $model,

        ]);
    }

    public function actionCreateDepartment($dep)
    {
        $model = new License();
        $model->department_id = $dep;
        if ($model->load(Yii::$app->request->post())) {
            $model->order_date = date('Y-m-d', strtotime($model->order_date));
            $model->given_date = date('Y-m-d', strtotime($model->given_date));
            $model->expiration_date = date('Y-m-d', strtotime($model->expiration_date));
            if ($model->save())
                return $this->redirect(['../structure/department']);
        }

        return $this->render('create', [
            'model' => $model,
            'id' => $dep,
        ]);
    }


    /**
     * Updates an existing License model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateDepartment($dep, $id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['../structure/department']);
        }

        return $this->render('update', [
            'model' => $model,
            'dep' => $dep,
        ]);
    }

    /**
     * Deletes an existing License model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = $model::STATUS_DELETED;
        if ($model->save())
        {
            return $this->redirect(['index']);
        }
    }

    public function actionDeleteDepartment($dep, $id)
    {
        if ($this->findModel($id)->delete()){
            return true;
        }
        return false;
    }

    public function actionRemove($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the License model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return License the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = License::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
