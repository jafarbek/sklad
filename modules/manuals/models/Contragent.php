<?php

namespace app\modules\manuals\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contragent".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $short_name
 * @property string|null $add_info
 * @property int $references_type_id
 * @property string|null $address
 * @property string|null $director
 * @property string|null $tel
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property ReferencesType $referencesType
 */
class Contragent extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contragent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['references_type_id','name','director'], 'required'],
            [['references_type_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['references_type_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'add_info', 'address', 'director'], 'string', 'max' => 255],
            [['short_name', 'tel'], 'string', 'max' => 50],
            [['status'],'safe'],
            [['references_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => References::className(), 'targetAttribute' => ['references_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'short_name' => Yii::t('app', 'Short Name'),
            'add_info' => Yii::t('app', 'Add Info'),
            'references_type_id' => Yii::t('app', 'References Type'),
            'address' => Yii::t('app', 'Address'),
            'director' => Yii::t('app', 'Director'),
            'tel' => Yii::t('app', 'Tel'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[ReferencesType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReferences()
    {
        return $this->hasOne(References::className(), ['id' => 'references_type_id']);
    }

    /**
     * @return array
     * @var contragent
     */
    public static function getReferencesTypeContragent(){
      return ArrayHelper::map(References::find()->where(['references_type_id' => 11])->all(),'id','token');
    }
}
