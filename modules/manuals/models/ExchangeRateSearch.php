<?php

namespace app\modules\manuals\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\manuals\models\ExchangeRate;

/**
 * ExchangeRateSearch represents the model behind the search form of `app\modules\manuals\models\ExchangeRate`.
 */
class ExchangeRateSearch extends ExchangeRate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'currency_id', 'to_currency_id'], 'integer'],
            [['currency_id', 'to_currency_id'], 'safe'],
            [['amount', 'to_amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = \Yii::$app->request->post();
        $query = ExchangeRate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'currency_id' => $this->currency_id,
            'amount' => $this->amount,
            'to_currency_id' => $this->to_currency_id,
            'to_amount' => $this->to_amount,
        ]);
        $query
//            ->andFilterWhere(['ilike', 'currency_id', $this->currency_id])
            ->andFilterWhere(['like', 'amount', $this->amount])
//            ->andFilterWhere(['like', 'to_currency_id', $this->to_currency_id])
            ->andFilterWhere(['like', 'to_amount', $this->to_amount]);

        return $dataProvider;
    }
}
