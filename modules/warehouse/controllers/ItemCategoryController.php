<?php

namespace app\modules\warehouse\controllers;

use app\controllers\BaseController;
use Yii;
use app\modules\warehouse\models\ItemCategory;
use app\modules\warehouse\models\ItemCategorySearch;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ItemCategoryController implements the CRUD actions for ItemCategory model.
 */
class ItemCategoryController extends BaseController
{
    /**
     * Lists all ItemCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $tree = ItemCategory::getTreeItemCategory();
        return $this->render('index', [
            'tree' => $tree
        ]);
    }
    /**
     * Creates a new ItemCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionValidate()
    {
        $model = new ItemCategory();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
    public function actionCreate()
    {
        $model = new ItemCategory();
        $tree = ArrayHelper::map(ItemCategory::find()->all(), 'id', 'name_'.Yii::$app->language);


        $model->parent_id = Yii::$app->request->get('id') ? Yii::$app->request->get('id') : '';


        if ($model->load(Yii::$app->request->post())) {
            if ($model->parent_id == 0){
                $model->parent_id = null;
            }
            $model->save();
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('create', [
                'model' => $model,
                'tree' => $tree,
            ]);

        }

        return $this->render('create', [
            'model' => $model,
            'tree' => $tree,
        ]);
    }

    /**
     * Updates an existing ItemCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ItemCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteAjax(){
        $id = Yii::$app->request->get('id');
        $model = ItemCategory::find()->where(['parent_id' => $id])->andWhere(['!=', 'status', '4'])->all();
        $response = Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($model)){
            $model = $this->findModel($id);
            $model->status = $model::STATUS_DELETED;
            if ($model->save()) {
                $response = true;
            } else {
                $response = false;
            }
        } else {
            $response = false;
        }
        return $response;
    }

    /**
     * Finds the ItemCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ItemCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ItemCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
