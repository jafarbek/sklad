<?php

namespace app\modules\warehouse\controllers;

use app\controllers\BaseController;
use app\modules\manuals\models\Countries;
use app\modules\manuals\models\References;
use app\modules\warehouse\models\ItemCategory;
use Yii;
use app\modules\warehouse\models\Item;
use app\modules\warehouse\models\ItemSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends BaseController
{

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models = Item::find()->where(['!=', 'status', '4'])->all();
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'models' => $models
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item();
        $item_category = ItemCategory::getTreeItemCategory(null, true, null);
        $unit = ArrayHelper::map(References::find()->where(['references_type_id' => '9','status' => $model::STATUS_ACTIVE])->all(), 'id', 'name_'.Yii::$app->language);
        $country = ArrayHelper::map(Countries::find()->where(['status' => $model::STATUS_ACTIVE])->all(), 'id', 'name_'.Yii::$app->language);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->isAjax){
            return $this->renderAjax('create', [
                'model' => $model,
                'item_category' => $item_category,
                'unit' => $unit,
                'country' => $country
            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'item_category' => $item_category,
            'unit' => $unit,
            'country' => $country
        ]);
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $item_category = ItemCategory::getTreeItemCategory(null, null, $id);
        $unit = ArrayHelper::map(References::find()->where(['references_type_id' => '9'])->all(), 'id', 'name_'.Yii::$app->language);
        $country = ArrayHelper::map(Countries::find()->all(), 'id', 'name_'.Yii::$app->language);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update', [
                'model' => $model,
                'item_category' => $item_category,
                'unit' => $unit,
                'country' => $country
            ]);
        }
        return $this->render('update', [
            'model' => $model,
            'item_category' => $item_category,
            'unit' => $unit,
            'country' => $country
        ]);
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRemove($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = $model::STATUS_DELETED;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
