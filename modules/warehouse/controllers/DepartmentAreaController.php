<?php

namespace app\modules\warehouse\controllers;

use phpDocumentor\Reflection\Types\This;
use Yii;
use app\modules\warehouse\models\DepartmentArea;
use app\modules\warehouse\models\DepartmentAreaSearch;
use app\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DepartmentAreaController implements the CRUD actions for DepartmentArea model.
 */
class DepartmentAreaController extends BaseController
{
//    /**
//     * {@inheritdoc}
//     */
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
//        ];
//    }

    /**
     * Lists all DepartmentArea models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DepartmentAreaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $department_area_tree = DepartmentArea::getDepartmentAreaTree();
        $model = DepartmentArea::find()->where(['status' => $searchModel::STATUS_ACTIVE])->asArray()->all();
        $models = Yii::$app->db->createCommand('SELECT da.id, da.name, da.code, da.department_id, d.name_uz AS Dname_uz, d.name_en AS Dname_en, d.name_ru AS Dname_ru, da.parent_id,  da.add_info, da.status
	FROM public.department_area AS da
	LEFT JOIN public.department AS d ON d.id = da.department_id WHERE da.status != 4 ORDER BY da.id;')->queryAll();//(SELECT name FROM public.department_area AS da1 WHERE da1.parent_id = da.parent_id) AS parent_name,
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'department_area_tree' => $department_area_tree,
            'model' => $model,
            'models' => $models,
        ]);
    }

    /**
     * Displays a single DepartmentArea model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax)
        {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DepartmentArea model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DepartmentArea();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->isAjax)
        {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DepartmentArea model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        if(Yii::$app->request->isAjax)
        {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DepartmentArea model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionRemove($id)
    {
        $model = DepartmentArea::find()->where(['parent_id' =>$id])->count();
        if (empty($model))
        {
            $model = $this->findModel($id);
            $model->status = DepartmentArea::STATUS_DELETED;
            if ($model->save()){
            return $this->redirect(['index']);
            }
        }
    }

    /**
     * Finds the DepartmentArea model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DepartmentArea the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DepartmentArea::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionDeleteAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        $model = DepartmentArea::find()->where(['parent_id' => $id])->count();
        if (empty($model))
        {
            if ($this->findModel($id)->delete()){
                return true;
            }
            return false;
        }
        return false;

    }
}
