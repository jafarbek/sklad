<?php

namespace app\modules\warehouse\controllers;

use app\modules\warehouse\models\DepartmentArea;
use app\modules\warehouse\models\Item;
use app\modules\warehouse\models\ItemCategory;
use phpDocumentor\Reflection\Types\Self_;
use Yii;
use app\modules\warehouse\models\ItemBalance;
use app\modules\warehouse\models\ItemBalanceSearch;
use app\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ItemBalanceController implements the CRUD actions for ItemBalance model.
 */
class ItemBalanceController extends BaseController
{


    /**
     * Lists all ItemBalance models.
     * @return mixed
     */

    public function actionIndex()
    {
        $model = new ItemBalanceSearch();
        return $this->render('index', [
            'models' => $model
        ]);
    }
    public function actionReportItemBalance()
    {
        $model = new ItemBalance();
        return $this->render('report_item_balance',[
            'model' =>$model,
        ]);
    }

    /**
     * Displays a single ItemBalance model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }


    protected function findModel($id)
    {
        if (($model = ItemBalance::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDepartmentArea()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $d = Yii::$app->request->get('q');
        if (!empty($d)) {
            $department = DepartmentArea::find()
                ->where(['department_id' => $d, 'status' => DepartmentArea::STATUS_ACTIVE])
                ->asArray()
                ->all();
            if (!empty($department)) {
                $response['status'] = true;
                $response['data'] = $department;
                $response['message'] = 'OK';
            } else {
                $response['message'] = 'Empty';
            }
        }
        return $response;
    }
    public function  actionItemCategory(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $lang = Yii::$app->language;
        $response['status'] = false;
        $response['message'] = 'Error';
        $d = Yii::$app->request->get('q');///'status' => DepartmentArea::STATUS_ACTIVE
        if (!empty($d)) {
            $d = implode(',',$d);
            $sql = "SELECT
                           item.name_{$lang} as name,
                           item.id as id,
                           ic.id as item_cat
                           FROM item
                    left join item_category ic on item.category_id = ic.id
                    where  ic.id in ({$d}) and ic.status=".ItemCategory::STATUS_ACTIVE." and item.status=".Item::STATUS_ACTIVE." ";
            $model = Yii::$app->db->createCommand($sql)->queryAll();
            if (!empty($model)) {
                $response['status'] = true;
                $response['data'] = $model;
                $response['message'] = 'OK';
            } else {
                $response['message'] = 'Empty';
            }
        }
        return $response;
    }

    public function actionTbody()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $lang = Yii::$app->language;
        $response['status'] = false;
        $response['message'] = 'Error';
        $dep = Yii::$app->request->get('dep');
        $reg = Yii::$app->request->get('reg');
        $area = Yii::$app->request->get('area');
        $item = Yii::$app->request->get('item');
        $item_category= Yii::$app->request->get('item_category');
        $sql = "SELECT          
                                itb.id, 
                                item.name_{$lang} as item,
                                dep.name_{$lang} as department,
                                dep.id as dep_id,
                                itb.item_id,
                                dep.name_{$lang} as dep_name,
                                depa.name AS department_area, 
                                itb.inventory AS inventory, 
                                itb.reg_date AS reg_date, 
                                itb.price AS price, 
                                itb.price_currency AS price_currency, 
                                ref.name_{$lang} ref_name,
                                ref1.token,
                                item.stock_limit,
                                itb.status
                        FROM public.item_balance as itb
                        LEFT JOIN item ON item.id = itb.item_id
                        LEFT JOIN department as dep ON dep.id = itb.department_id
                        LEFT JOIN public.references AS ref1 ON dep.currency_id = ref1.id
                        LEFT JOIN department_area as depa ON depa.id = itb.department_area_id
                        LEFT JOIN document as doc ON doc.id = itb.document_id
                        LEFT JOIN public.references AS ref ON ref.id = item.unit_id
                        LEFT JOIN (SELECT * FROM item_balance AS i_b
                                    WHERE i_b.id in (SELECT MAX(i_b2.id)  FROM item_balance AS i_b2 GROUP BY i_b2.item_id)) AS i_b ON i_b.item_id = item.id
                        LEFT JOIN document_item as doci ON doci.id = itb.document_item_id
                        WHERE itb.id in (SELECT MAX(i_b2.id)  FROM item_balance AS i_b2 GROUP BY i_b2.item_id, i_b2.department_id, i_b2.department_area_id, i_b2.price) ";
        $area =!empty ($area) ? $area= " AND itb.department_area_id in (".implode(',',$area).")" : $area = ' ';



        if (empty($item) && !empty($item_category)){
            $item_category=!empty ($item_category) ? $item_category=implode(',',$item_category) : $item_category = ' ';
            $itemCat = "SELECT
                           item.id as id
                           FROM item
                    LEFT JOIN  item_category ic on item.category_id = ic.id
                    where  ic.id in ({$item_category}) "; // and ic.status=".ItemCategory::STATUS_ACTIVE." and item.status=".Item::STATUS_ACTIVE." ";
             $model1 = Yii::$app->db->createCommand($itemCat)->queryAll();
             $item_id = [];
             foreach ($model1 as $items){
                    array_push($item_id,$items['id']);
             }
             $item =!empty ($item_id) ? $item = " AND itb.item_id in (".implode(',',$item_id).")" : $item = ' ';
        }
        else if(!empty($item) && !empty($item_category)){
            $item =!empty ($item) ? $item = " AND itb.item_id in (".implode(',',$item).")" : $item = ' ';
        }



        if (!empty($dep) && !empty($reg)) {
            $sql .= " AND itb.department_id = {$dep}  {$area} AND itb.reg_date <= '{$reg}' {$item}";
        }

        $models = Yii::$app->db->createCommand($sql)->queryAll();
        if (!empty($models)) {

            $response['status'] = true;
            $response['data'] = $models;
            $response['message'] = 'OK';
            $response['count'] = Yii::$app->db->createCommand($sql)->execute();
        } else {
            $response['count'] = Yii::$app->db->createCommand($sql)->execute();
            $response['message'] = 'Empty';
        }
        return $response;
    }
}
