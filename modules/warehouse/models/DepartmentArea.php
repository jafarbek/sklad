<?php

namespace app\modules\warehouse\models;

use app\modules\structure\models\Department;
use Yii;

/**
 * This is the model class for table "{{%department_area}}".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $code
 * @property int|null $department_id
 * @property int|null $parent_id
 * @property string|null $add_info
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Department $department
 * @property DepartmentArea $parent
 * @property DepartmentArea[] $departmentAreas
 * @property DocumentItem[] $documentItems
 * @property DocumentItem[] $documentItems0
 * @property ItemBalance[] $itemBalances
 */
class DepartmentArea extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%department_area}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['department_id', 'parent_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['department_id', 'parent_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['department_id', 'status', 'name', 'code'], 'required'],
            [['name', 'code', 'add_info'], 'string', 'max' => 255],
            [['code'], 'unique'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentArea::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'department_id' => Yii::t('app', 'Department'),
            'parent_id' => Yii::t('app', 'Parent'),
            'add_info' => Yii::t('app', 'Add Info'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(DepartmentArea::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentAreas()
    {
        return $this->hasMany(DepartmentArea::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentItems()
    {
        return $this->hasMany(DocumentItem::className(), ['from_dep_area' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentItems0()
    {
        return $this->hasMany(DocumentItem::className(), ['to_dep_area' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['department_area_id' => 'id']);
    }
    public static function getDepartmentAreaTree($parent_id = null)
    {
        $department_areas = DepartmentArea::find()->where(['parent_id' => $parent_id])->andWhere(['!=','status', self::STATUS_DELETED])->all();//->andWhere(['status'=> self::STATUS_ACTIVE])
        $department_area_tree = "";
        foreach ($department_areas as $department_area)
        {
            $department_area_tree = $department_area_tree."<ul><li value='{$department_area['id']}' data-parent-id='{$department_area['parent_id']}' data-status='{$department_area['status']}' data-jstree='{\"opened\" : true}'>{$department_area['name']}".self::getDepartmentAreaTree($department_area['id'])."</li></ul>";
        }
        return $department_area_tree;
    }

    public static function Departments()
    {
        return Department::find()->asArray()->all();
    }

    public function getDepartmentList()
    {
        return Department::getHierarchy();
    }


    public static function getHierarchy($parent_id=null,$lvl = 0) {
        $regions = self::find()->where(['parent_id' => $parent_id])->andWhere(['status'=> self::STATUS_ACTIVE])->all();//->andWhere(['status'=> self::STATUS_ACTIVE])
        $regions_tree = [];
        $probel = "";
        for ($i = 0; $i < $lvl; $i++){
            $probel .= "-";
        }
        $lvl++;
        foreach ($regions as $region)
        {
            $regions_tree[$region['id']] = $probel.$region['name'];
            $regions_tree = \yii\helpers\ArrayHelper::merge($regions_tree, self::getHierarchy($region['id'], $lvl));
        }
        return $regions_tree;
    }


}
