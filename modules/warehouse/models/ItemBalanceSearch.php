<?php

namespace app\modules\warehouse\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\warehouse\models\ItemBalance;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ItemBalanceSearch represents the model behind the search form of `app\modules\warehouse\models\ItemBalance`.
 */
class ItemBalanceSearch extends ItemBalance
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'department_id', 'department_area_id', 'document_id', 'document_item_id', 'price_currency', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['lot', 'reg_date', 'price'], 'safe'],
            [['department_id','reg_date','department_area_id'], 'required'],
            [['quantity', 'inventory', 'price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array|\yii\db\DataReader
     * @throws \yii\db\Exception
     */
    public function search($params)
    {
        $lang = \Yii::$app->language;
        $sql = "SELECT 
            itb.id, 
            item.name_{$lang} as item_{$lang},
            dep.name_{$lang} as department_{$lang},
            dep.id as dep_id,
            depa.name AS department_area, 
            itb.inventory AS inventory, 
            itb.reg_date AS reg_date, 
            itb.price price, 
            itb.price_currency price_currency, 
            ref.name_{$lang},
            ref1.token,
            item.stock_limit,
            itb.status
	FROM public.item_balance as itb
	LEFT JOIN item ON item.id = itb.item_id
	LEFT JOIN department as dep ON dep.id = itb.department_id
	LEFT JOIN public.references AS ref1 ON dep.currency_id = ref1.id
	LEFT JOIN department_area as depa ON depa.id = itb.department_area_id
	LEFT JOIN document as doc ON doc.id = itb.document_id
	LEFT JOIN public.references AS ref ON ref.id = item.unit_id
	LEFT JOIN (SELECT * FROM item_balance AS i_b
                WHERE i_b.id in (SELECT MAX(i_b2.id)  FROM item_balance AS i_b2 GROUP BY i_b2.item_id)) AS i_b ON i_b.item_id = item.id
	LEFT JOIN document_item as doci ON doci.id = itb.document_item_id
	WHERE itb.id in (SELECT MAX(i_b2.id)  FROM item_balance AS i_b2 GROUP BY i_b2.item_id, i_b2.department_id, i_b2.department_area_id, i_b2.price)
	  
	%s
	%s
	%s
	%s
	 ";
        $this->load($params);
        $data = $params;
        $item = [];
        if (!empty($data['ItemBalanceSearch']['item_id'])){
        foreach ($data['ItemBalanceSearch']['item_id'] as $it){
            array_push($item,$it);
            }
            $item = implode(',', $item);
        }
        $depatment = $this->department_id ? "AND itb.department_id ={$this->department_id}" : "";
        $department_area = $this->department_area_id ? "AND itb.department_area_id={$this->department_area_id}" : "";
        $date = $this->reg_date ? "AND itb.reg_date <= '{$this->reg_date}'" : "";
        $item_id = $item ? " AND itb.item_id in ({$item}) " : "";
        $sql = sprintf($sql, $depatment, $department_area, $date, $item_id);

        return $sql = \Yii::$app->db->createCommand($sql)->queryAll();

//        $this->load($params);
//        if (!$this->validate()) {
//            $dataProvider = new SqlDataProvider([
//                'sql' => $sql,
////                'pagination' => [
////                    'pageSize' => 20,
////                ],
//            ]);
//
//            return $dataProvider;
//        }
//
//
//
//        $dataProvider = new SqlDataProvider([
//            'sql' => $sql,
////            'pagination' => [
////                'pageSize' => 20,
////            ],
//        ]);
//        return $dataProvider;
    }

//        $id_in = \Yii::$app->db->createCommand("
//            SELECT MAX(i_b1.id) FROM item_balance AS i_b1
//                GROUP BY
//                    i_b1.item_id,
//                    i_b1.department_id,
//                    i_b1.department_area_id,
//                    i_b1.price
//                ")->queryAll();
//        $id_in = ArrayHelper::getColumn($id_in,'max');
//        $params = \Yii::$app->request->post();
//        $query = ItemBalance::find()->where(['in', 'id', $id_in])->andWhere(['>', 'inventory', '0']);
//        // add conditions that should always apply here
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'pagination'=>[
//                'pageSize' => 20,
//            ]
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
////        $query->joinWith('documentItem');
//        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'item_id' => $this->item_id,
//            'quantity' => $this->quantity,
//            'inventory' => $this->inventory,
//            'reg_date' => $this->reg_date,
//            'department_id' => $this->department_id,
//            'department_area_id' => $this->department_area_id,
//            'document_id' => $this->document_id,
//            'document_item_id' => $this->document_item_id,
//            'price' => $this->price,
//            'price_currency' => $this->price_currency,
//            'status' => $this->status,
//            'created_by' => $this->created_by,
//            'updated_by' => $this->updated_by,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//        ]);
//
//        $query->andFilterWhere(['ilike', 'lot', $this->lot]);
////        $query->andFilterWhere(['ilike', 'documentItem.income_price', $this->documentItem->income_price]);
//        return $dataProvider;

}
