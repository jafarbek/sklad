<?php

namespace app\modules\warehouse\models;

use app\modules\manuals\models\References;
use Yii;

/**
 * This is the model class for table "document_item".
 *
 * @property int $id
 * @property int|null $document_id
 * @property int|null $item_id
 * @property string|null $lot
 * @property float|null $document_quantity
 * @property float|null $quantity
 * @property int|null $from_dep_area
 * @property int|null $to_dep_area
 * @property float|null $income_price
 * @property int|null $income_currency
 * @property float|null $price
 * @property int|null $price_currency
 * @property float|null $sell_price
 * @property int|null $sell_currency
 * @property string|null $add_info
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $summa
 *
 * @property DepartmentArea $fromDepArea
 * @property DepartmentArea $toDepArea
 * @property Document $document
 * @property Item $item
 * @property References $incomeCurrency
 * @property References $priceCurrency
 * @property References $sellCurrency
 * @property ItemBalance[] $itemBalances
 */
class DocumentItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document_item';
    }
    public $summa;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document_id', 'item_id', 'from_dep_area', 'to_dep_area', 'income_currency', 'price_currency', 'sell_currency', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['document_id', 'item_id', 'from_dep_area', 'to_dep_area', 'income_currency', 'price_currency', 'sell_currency', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['document_quantity', 'quantity', 'income_price', 'price', 'sell_price'], 'number'],
            [['lot'], 'string', 'max' => 50],
            [['add_info'], 'string', 'max' => 255],
            [['from_dep_area'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentArea::className(), 'targetAttribute' => ['from_dep_area' => 'id']],
            [['to_dep_area'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentArea::className(), 'targetAttribute' => ['to_dep_area' => 'id']],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['income_currency'], 'exist', 'skipOnError' => true, 'targetClass' => References::className(), 'targetAttribute' => ['income_currency' => 'id']],
            [['price_currency'], 'exist', 'skipOnError' => true, 'targetClass' => References::className(), 'targetAttribute' => ['price_currency' => 'id']],
            [['sell_currency'], 'exist', 'skipOnError' => true, 'targetClass' => References::className(), 'targetAttribute' => ['sell_currency' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'document_id' => Yii::t('app', 'Document'),
            'item_id' => Yii::t('app', 'Item'),
            'lot' => Yii::t('app', 'Lot'),
            'document_quantity' => Yii::t('app', 'Document Quantity'),
            'quantity' => Yii::t('app', 'Quantity'),
            'from_dep_area' => Yii::t('app', 'From Dep Area'),
            'to_dep_area' => Yii::t('app', 'To Dep Area'),
            'income_price' => Yii::t('app', 'Income Price'),
            'income_currency' => Yii::t('app', 'Income Currency'),
            'price' => Yii::t('app', 'Price'),
            'price_currency' => Yii::t('app', 'Price Currency'),
            'sell_price' => Yii::t('app', 'Sell Price'),
            'sell_currency' => Yii::t('app', 'Sell Currency'),
            'add_info' => Yii::t('app', 'Add Info'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromDepArea()
    {
        return $this->hasOne(DepartmentArea::className(), ['id' => 'from_dep_area']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToDepArea()
    {
        return $this->hasOne(DepartmentArea::className(), ['id' => 'to_dep_area']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeCurrency()
    {
        return $this->hasOne(References::className(), ['id' => 'income_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCurrency()
    {
        return $this->hasOne(References::className(), ['id' => 'price_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellCurrency()
    {
        return $this->hasOne(References::className(), ['id' => 'sell_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['document_item_id' => 'id']);
    }
}
