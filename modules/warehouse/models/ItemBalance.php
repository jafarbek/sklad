<?php

namespace app\modules\warehouse\models;

use app\modules\admin\models\UserDepartment;
use app\modules\manuals\models\ReferencesType;
use app\modules\structure\models\Department;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%item_balance}}".
 *
 * @property int $id
 * @property int|null $item_id
 * @property string|null $lot
 * @property float|null $quantity
 * @property float|null $inventory
 * @property string|null $reg_date
 * @property int|null $department_id
 * @property int|null $department_area_id
 * @property int|null $document_id
 * @property int|null $document_item_id
 * @property float|null $price
 * @property int|null $price_currency
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Department $department
 * @property DepartmentArea $departmentArea
 * @property Document $document
 * @property DocumentItem $documentItem
 * @property Item $item
 * @property ReferencesType $priceCurrency
 */
class ItemBalance extends \app\models\BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%item_balance}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'department_id', 'department_area_id', 'document_id', 'document_item_id', 'price_currency', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['item_id', 'department_id', 'department_area_id', 'document_id', 'document_item_id', 'price_currency', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['quantity', 'inventory', 'price'], 'number'],
            [['reg_date'], 'safe'],
            [['reg_date','department_id','department_id'], 'required'],
            [['lot'], 'string', 'max' => 50],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['department_area_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentArea::className(), 'targetAttribute' => ['department_area_id' => 'id']],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document_id' => 'id']],
            [['document_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentItem::className(), 'targetAttribute' => ['document_item_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['price_currency'], 'exist', 'skipOnError' => true, 'targetClass' => ReferencesType::className(), 'targetAttribute' => ['price_currency' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item'),
            'lot' => Yii::t('app', 'Lot'),
            'quantity' => Yii::t('app', 'Quantity'),
            'inventory' => Yii::t('app', 'Inventory'),
            'reg_date' => Yii::t('app', 'Registration date'),
            'department_id' => Yii::t('app', 'Department'),
            'department_area_id' => Yii::t('app', 'Department Area'),
            'document_id' => Yii::t('app', 'Document'),
            'document_item_id' => Yii::t('app', 'Document Item'),
            'price' => Yii::t('app', 'Price'),
            'price_currency' => Yii::t('app', 'Price Currency'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentArea()
    {
        return $this->hasOne(DepartmentArea::className(), ['id' => 'department_area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentItem()
    {
        return $this->hasOne(DocumentItem::className(), ['id' => 'document_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCurrency()
    {
        return $this->hasOne(ReferencesType::className(), ['id' => 'price_currency']);
    }

    public static function getItemList()
    {
        $result = Item::find()->asArray()->all();
        $array = [];
        foreach ($result as $item) {
            $array[$item['id']] = $item['name_' . Yii::$app->language];
        }
        return $array;
    }
    public static function getItemCategory(){
        $result=ItemCategory::find()->select(['name_'.Yii::$app->language.' as name','id'])->where(['status'=>ItemCategory::STATUS_ACTIVE])->asArray()->all();
        $array = [];
        foreach ($result as $item) {
            $array[$item['id']] = $item['name'];
        }
        return $array;
    }
    public static function getDepartmentList()
    {
        $lang = Yii::$app->language;
        $result = Yii::$app->db->createCommand("SELECT
            d.name_{$lang},
            d.id
            FROM department AS d
            INNER JOIN (SELECT department_id AS depid FROM user_department 
            WHERE user_id = ".Yii::$app->user->identity->getId()." 
            group by department_id) AS ud ON ud.depid = d.id 
            where d.status = ".Department::STATUS_ACTIVE." order by d.name_{$lang} asc")->queryAll();
        $array = [];
        foreach ($result as $item) {
            $array[$item['id']] = $item['name_' . $lang];
        }
        return $array;
    }

    public static function getDepartmentAreaList($id)
    {
        $result = DepartmentArea::find()->where(['department_id'=>$id, 'status'=>Department::STATUS_ACTIVE])->asArray()->all();
        $array = [];
        foreach ($result as $item) {
            $array[$item['id']] = $item['name'];
        }
        return $array;
    }

    public static function getDocumentList()
    {
        $result = Document::find()->asArray()->all();
        $array = [];
        foreach ($result as $item) {
            $array[$item['id']] = $item['doc_number'];
        }
        return $array;
    }

    public static function getSearchItemBalance($depatment, $department_area, $item, $date)
    {
        $sql = "SELECT 
            itb.id, 
            item.name_uz as item_uz,
            item.name_en as item_en, 
            item.name_ru as item_ru,
            doc.doc_number, 
            dep.name_uz as department_uz,
            dep.name_ru as department_ru,
            dep.name_en as department_en,
            depa.name department_area, 
            itb.lot, 
            itb.quantity, 
            itb.inventory, 
            itb.reg_date, 
            itb.document_item_id, 
            itb.price, 
            itb.price_currency, 
            itb.status,  
            itb.sell_price, 
            itb.sell_currency, 
            itb.production_id
	FROM public.item_balance as itb
	LEFT JOIN item ON item.id = itb.item_id
	LEFT JOIN department as dep ON dep.id = itb.department_id
	LEFT JOIN department_area as depa ON depa.id = itb.department_area_id
	LEFT JOIN document as doc ON doc.id = itb.document_id
	LEFT JOIN document_item as doci ON doci.id = itb.document_item_id
	 ";
        if (($depatment) && ($department_area) && ($date) && ($item)) {
            $item =implode(',',$item);
            $depatment =implode(',',$depatment);
            $department_area =implode(',',$department_area);
            $sql .= "WHERE itb.item_id in ({$item}) and itb.reg_date <= '{$date}' and itb.department_id in ({$depatment}) and  itb.department_area_id in ({$department_area})";
        }else if (!empty($depatment) && !empty($department_area) && !empty($date)) {
            $item =implode(',',$item);
            $depatment =implode(',',$depatment);
            $department_area =implode(',',$department_area);
            $sql .= "WHERE itb.reg_date <= '{$date}' and itb.department_id in ({$depatment}) and  itb.department_area_id in ({$department_area})";
        }

        return Yii::$app->db->createCommand($sql)->queryAll();
    }
}
