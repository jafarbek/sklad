<?php

namespace app\modules\warehouse\models;

use app\modules\warehouse\models\Document;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * DocumentSearch represents the model behind the search form of `app\modules\warehouse\models\Document`.
 */
class DocumentSearch extends Document
{
    /**
     * @var mixed|null
     */

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'contragent_id', 'from_department', 'to_department', 'doc_status', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['document_type', 'doc_number', 'reg_date', 'contragent_responsible', 'from_employee', 'to_employee', 'add_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $params = \Yii::$app->request->post();
        $docType = 1;
        $slug = \Yii::$app->request->get('slug');
        switch ($slug) {
            case Document::DOC_TYPE_INCOMING_LABEL:
                $docType = Document::DOC_TYPE_INCOMING;
                break;
            case Document::DOC_TYPE_OUTGOING_LABEL:
                $docType = Document::DOC_TYPE_OUTGOING;
                break;

            case Document::DOC_TYPE_MOVING_LABEL:
                $docType = Document::DOC_TYPE_MOVING;
                break;
            case Document::DOC_TYPE_SELLING_LABEL:
                $docType = Document::DOC_TYPE_SELLING;
                break;
        }

        $query = Document::find()->where(['document_type'=>$docType])->andWhere(['!=','status', Document::STATUS_DELETED]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reg_date' => $this->reg_date,
            'contragent_id' => $this->contragent_id,
            'from_department' => $this->from_department,
            'to_department' => $this->to_department,
            'doc_status' => $this->doc_status,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'document_type', $this->document_type])
            ->andFilterWhere(['ilike', 'doc_number', $this->doc_number])
            ->andFilterWhere(['ilike', 'contragent_responsible', $this->contragent_responsible])
            ->andFilterWhere(['ilike', 'from_employee', $this->from_employee])
            ->andFilterWhere(['ilike', 'to_employee', $this->to_employee])
            ->andFilterWhere(['ilike', 'add_info', $this->add_info]);
        $query->orderBy(['id' => SORT_DESC]);
        return $dataProvider;
    }
}
