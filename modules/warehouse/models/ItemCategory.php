<?php

namespace app\modules\warehouse\models;

use phpDocumentor\Reflection\Types\Self_;
use Yii;

/**
 * This is the model class for table "item_category".
 *
 * @property int $id
 * @property string|null $name_en
 * @property string|null $name_ru
 * @property string|null $name_uz
 * @property int|null $parent_id
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property ItemCategory $parent
 * @property ItemCategory[] $itemCategories
 */
class ItemCategory extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public $pozitsiya = 0;
    public static function tableName()
    {
        return 'item_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ru', 'name_uz'], 'required'],
            [['parent_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['parent_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['name_en', 'name_ru', 'name_uz'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemCategory::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_uz' => Yii::t('app', 'Name Uz'),
            'parent_id' => Yii::t('app', 'Parent'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ItemCategory::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategories()
    {
        return $this->hasMany(ItemCategory::className(), ['parent_id' => 'id']);
    }


    public static function getTreeItemCategory($parent_id = null, $create = null, $update = null){
        $name = Yii::$app->language;
        $items = ItemCategory::find()->where(['parent_id' => $parent_id])->andWhere(['!=', 'status', '4'])->asArray()->all();
        $tree = "";
        foreach ($items as $item){
            $icon = true;
            if ($create != null){
                $icon = false;
            }
            $tree = $tree."<ul><li value='{$item['id']}'  data-jstree='{ \"opened\" : {$icon}, \"icon\" : \"flaticon-layer text-info\" }'>{$item['name_'.$name]}".self::getTreeItemCategory($item['id'], $create, $update)."</li></ul>";
        }
        return $tree;
    }
}
