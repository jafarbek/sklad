<?php

namespace app\modules\warehouse\models;

use app\models\BaseModel;
use app\modules\manuals\models\Countries;
use app\modules\manuals\models\References;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "item".
 *
 * @property int $id
 * @property string|null $name_en
 * @property string|null $name_ru
 * @property string|null $name_uz
 * @property string|null $short_name
 * @property int|null $category_id
 * @property float|null $size
 * @property float|null $weight
 * @property int|null $unit_id
 * @property string|null $article
 * @property int|null $country_id
 * @property string|null $add_info
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $stock_limit
 *
 * @property Countries $country
 * @property ItemCategory $category
 * @property References $unit
 * @property ItemBarcode[] $itemBarcodes
 */
class Item extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ru', 'name_uz', 'article', 'category_id', 'unit_id', 'country_id', 'status', 'stock_limit', 'short_name'], 'required'],
            [['category_id', 'unit_id', 'country_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['size', 'weight', 'stock_limit'], 'number'],
            [['name_en', 'name_ru', 'name_uz', 'article', 'add_info'], 'string', 'max' => 255],
            [['short_name'], 'string', 'max' => 50],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => References::className(), 'targetAttribute' => ['unit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_uz' => Yii::t('app', 'Name Uz'),
            'short_name' => Yii::t('app', 'Short Name'),
            'category_id' => Yii::t('app', 'Category'),
            'size' => Yii::t('app', 'Size'),
            'weight' => Yii::t('app', 'Weight'),
            'unit_id' => Yii::t('app', 'Unit'),
            'article' => Yii::t('app', 'Article'),
            'country_id' => Yii::t('app', 'Country'),
            'add_info' => Yii::t('app', 'Add Info'),
            'stock_limit' => Yii::t('app', 'Stock Limit'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ItemCategory::className(), ['id' => 'category_id']);
    }

    public static function getItemCategoryList(){
        $result = ItemCategory::find()->asArray()->all();
        $array = [];
        foreach ($result as $item){
            $array[$item['id']] = $item['name_'.Yii::$app->language];
        }
        return $array;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(References::className(), ['id' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBarcodes()
    {
        return $this->hasMany(ItemBarcode::className(), ['item_id' => 'id']);
    }
}
