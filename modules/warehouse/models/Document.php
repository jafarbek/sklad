<?php

namespace app\modules\warehouse\models;

use app\modules\admin\models\UserDepartment;
use app\modules\admin\models\Users;
use app\modules\manuals\models\Contragent;
use app\modules\manuals\models\References;
use app\modules\structure\models\Department;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%document}}".
 *
 * @property int $id
 * @property int|null $document_type
 * @property string|null $doc_number
 * @property string|null $reg_date
 * @property int|null $contragent_id
 * @property string|null $contragent_responsible
 * @property int|null $from_department
 * @property int|null $to_department
 * @property string|null $from_employee
 * @property string|null $to_employee
 * @property string|null $add_info
 * @property int|null $doc_status
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Contragent $contragent
 * @property Department $fromDepartment
 * @property Department $toDepartment
 * @property DocumentItem[] $documentItems
 * @property ItemBalance[] $itemBalances
 * @property string|null $search
 */
class Document extends \app\models\BaseModel
{
    const DOC_TYPE_INCOMING               = 1;
    const DOC_TYPE_OUTGOING               = 2;
    const DOC_TYPE_MOVING                 = 3;
    const DOC_TYPE_SELLING                = 4;
    const DOC_TYPE_FINAL                = 5;



    const DOC_TYPE_INCOMING_LABEL         = 'incoming';
    const DOC_TYPE_OUTGOING_LABEL        = 'outgoing';
    const DOC_TYPE_MOVING_LABEL           = 'moving';
    const DOC_TYPE_SELLING_LABEL          = 'selling';
    const DOC_TYPE_FINAL_LABEL          = 'final';

    public $search;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%document}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document_type', 'contragent_id', 'from_department', 'to_department', 'doc_status', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['document_type', 'contragent_id', 'from_department', 'to_department', 'doc_status', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['reg_date'], 'safe'],
            [['doc_number'], 'string', 'max' => 30],
            [['doc_number'], 'unique'],
            [['contragent_responsible'], 'string', 'max' => 100],
            [['from_employee', 'to_employee'], 'string', 'max' => 70],
            [['add_info'], 'string', 'max' => 255],
            [['contragent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contragent::className(), 'targetAttribute' => ['contragent_id' => 'id']],
//            [['from_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['from_department' => 'id']],
            [['to_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['to_department' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'document_type' => Yii::t('app', 'Document Type'),
            'doc_number' => Yii::t('app', 'Doc Number'),
            'reg_date' => Yii::t('app', 'Reg Date'),
            'contragent_id' => Yii::t('app', 'Contragent'),
            'contragent_responsible' => Yii::t('app', 'Contragent Responsible'),
            'from_department' => Yii::t('app', 'From Department'),
            'to_department' => Yii::t('app', 'To Department'),
            'from_employee' => Yii::t('app', 'From Employee'),
            'to_employee' => Yii::t('app', 'To Employee'),
            'add_info' => Yii::t('app', 'Add Info'),
            'doc_status' => Yii::t('app', 'Doc Status'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContragent()
    {
        return $this->hasOne(Contragent::className(), ['id' => 'contragent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'from_department']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'to_department']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentItems()
    {
        return $this->hasMany(DocumentItem::className(), ['document_id' => 'id']);
    }
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['document_id' => 'id']);
    }

    public function getContgentsList()
    {
        $list =  Contragent::find()->asArray()->all();
        return ArrayHelper::map($list, 'id','name');
    }

    public function getDepartmentListReciving($parent_id = null, $lvl = 0)
    {
        $id = Yii::$app->user->identity->id;
        $regions = Department::find()->where(['parent_id' => $parent_id])->all();//->andWhere(['status'=> self::STATUS_ACTIVE])
        $regions_tree = [];
        $probel = "";
        for ($i = 0; $i < $lvl; $i++){
            $probel .= "&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        $lvl++;
        foreach ($regions as $region)
        {
            $region_name = "";
            if (!empty(UserDepartment::find()->where(['user_id' => $id, 'department_id' => $region['id'], 'is_transfer' => self::IS_TRANSFER_RESIVING])->asArray()->all())) {
                if ($lvl !== 1) {
                    $region_name = $probel . "-" . $region['name_' . Yii::$app->language];
                } else {
                    $region_name = $probel . "<b>" . $region['name_' . Yii::$app->language] . "</b>";
                }
            }
            if ($region_name != "") {
                $regions_tree[$region['id']] = $region_name;
            }
            $regions_tree = \yii\helpers\ArrayHelper::merge($regions_tree, self::getDepartmentListReciving($region['id'], $lvl));
        }
        return $regions_tree;
    }

    public function getDepartmentListOutPuting($parent_id = null, $lvl = 0)
    {
        $id = Yii::$app->user->identity->id;
        $regions = Department::find()->where(['parent_id' => $parent_id])->all();//->andWhere(['status'=> self::STATUS_ACTIVE])
        $regions_tree = [];
        $probel = "";
        for ($i = 0; $i < $lvl; $i++){
            $probel .= "&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        $lvl++;
        foreach ($regions as $region)
        {
            $region_name = "";
            if (!empty(UserDepartment::find()->where(['user_id' => $id, 'department_id' => $region['id'], 'is_transfer' => self::IS_TRANSFER_OUTPUTING])->asArray()->all())) {
                if ($lvl !== 1) {
                    $region_name = $probel . "-" . $region['name_' . Yii::$app->language];
                } else {
                    $region_name = $probel . "<b>" . $region['name_' . Yii::$app->language] . "</b>";
                }
            }
            if ($region_name != "") {
                $regions_tree[$region['id']] = $region_name;
            }
            $regions_tree = \yii\helpers\ArrayHelper::merge($regions_tree, self::getDepartmentListReciving($region['id'], $lvl));
        }
        return $regions_tree;
    }
    public function getDepartmentAreaList()
    {
        return DepartmentArea::getHierarchy();
    }
    public function afterFind()
    {
        parent::afterFind();
        $this->reg_date = $this->reg_date ? date('d.m.Y', strtotime($this->reg_date)) : date('d.m.Y');
    }


    public static function getDocTypeBySlug($key = null)
    {
        $result = [
            self::DOC_TYPE_INCOMING_LABEL => Yii::t('app', 'Incoming'),
            self::DOC_TYPE_OUTGOING_LABEL => Yii::t('app', "Outgoing"),
            self::DOC_TYPE_MOVING_LABEL => Yii::t('app', "Moving"),
            self::DOC_TYPE_SELLING_LABEL => Yii::t('app', "Selling"),
        ];
        if ($key)
            return $result[$key];
        return $result;
    }
    public function getSlugLabel($default_slug=null)
    {
        if($default_slug){
            return self::getDocTypeBySlug($default_slug);
        }
        $slug = Yii::$app->request->get('slug');
        if (!empty($slug)) {
            return self::getDocTypeBySlug($slug);
        }
    }
    public function getItems($all)
    {
        $active = Item::STATUS_ACTIVE;
        $lang = Yii::$app->language;
        if ($all) {
            $sql = "select id, name_{$lang} 
                    from item
                    where status = {$active} ORDER BY updated_at ASC;";
        }
        $items = Yii::$app->db->createCommand($sql)->queryAll();
        return !empty($items ) ? ArrayHelper::map($items ,'id','name_'.Yii::$app->language):null;
    }
    public static function getReferences($idName)
    {
        return ArrayHelper::map(References::find()->where(['references_type_id' => $idName])->all(), 'id', 'name_'.Yii::$app->language);
    }

    public static function getEmployes($id, $type)
    {
        $sql = "SELECT u.id as id,u.fullname as name FROM users AS u
                LEFT JOIN user_department ud on u.id = ud.user_id
                WHERE ud.department_id=%d AND ud.is_transfer=%d GROUP BY u.id;";
        $sql = sprintf($sql,$id,$type);
        return Yii::$app->db->createCommand($sql)->queryAll();
    }

    public static function getAllReferencesCurrency(){
        $dataType = References::find()->where(['references_type_id' => 1])->all();
        if(!empty($dataType)){
            return ArrayHelper::map($dataType,'id','name_'.Yii::$app->language);
        }
        return [];
    }
}
