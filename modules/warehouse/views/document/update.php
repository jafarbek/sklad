<?php

use app\modules\warehouse\models\Document;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\Document */
/* @var $models app\modules\warehouse\models\Document */

$this->title = Yii::t('app', 'Update Document: {name}', [
    'name' => $model->id,
]);
$slug = $this->context->slug;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Documents')." (".Document::getDocTypeBySlug($this->context->slug).")", 'url' => ['index','slug' => $this->context->slug]];
$this->params['breadcrumbs'][] = ['label' => $model->doc_number, 'url' => ['view', 'slug' => $this->context->slug, 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="wh-document-form document-updata  card card-body">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->render("forms/_{$this->context->slug}", [
        'model' => $model,
        'form' => $form,
        'models' => $models,
    ]); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <br>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-success btn-custom-doc']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>