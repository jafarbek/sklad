<?php

use app\modules\warehouse\models\Document;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\modules\warehouse\models\Document */
$slug = Yii::$app->request->get('slug');
$this->title = Yii::t('app', Document::getDocTypeBySlug($slug));
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .twoInOne{
        font-size: 10px;
        border-top: grey 1px solid;
        display: block;
    }
    .new-row {
        display: block;
    }
</style>
    <!--begin::Container-->
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                </div>
                <div class="card-toolbar">
                    <!--begin::Dropdown-->
                    <!--end::Dropdown-->
                    <a href="<?= Url::to(['document/create', 'slug'=>$slug]) ?>" class="btn btn-sm btn-primary">
                        <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                            <!--end::Svg Icon-->
                    </span><?=Yii::t('app', 'Create')?>
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <?php if (Yii::$app->session->hasFlash('danger')): ?>
                <h6 class="alert alert-danger"><?=Yii::$app->session->getFlash('danger'); ?></h6>
                <?php elseif (Yii::$app->session->hasFlash('success')): ?>
                    <h6 class="alert alert-success"><?=Yii::$app->session->getFlash('success'); ?></h6>
                <?php endif; ?>
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <? $form = \yii\bootstrap\ActiveForm::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => '',
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'options' => ['style' => 'width:40px'],
                        ],
                        [
                            'attribute' => 'doc_number',
                            'format' => 'raw',
                            'value' => function($model){
                                return "<span class='new-row'>{$model->doc_number}</span>
                                    <span class='twoInOne'>{$model->reg_date}</span>";
                            }
                        ],
                        [
                            'attribute' => 'from_department',
                            'format' => 'raw',
                            'value' => function($model){
                                $fromDep = $model->fromDepartment['name_'.Yii::$app->language] ?? '';
                                return "<span class='new-row'>{$fromDep}</span><span class='twoInOne'>".$model->from_employee."</span>";
                            }
                        ],
                        'add_info',
                        [
                            'attribute' => 'created_by',
                            'format' => 'raw',
                            'value' => function($model){
                                return "<span class='new-row'>{$model->createdBy->fullname}</span><span class='twoInOne'>".date('Y-m-d H:i:s', $model->created_at)."</span>";
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => Yii::t('app', "Action"),
                            'template' => '{view} {update} {delete}',
                            'options' => ['style' => 'width:100px;'],
                            'buttons' => [
                                'view' => function($url, $model){
                                    return '<a href="'.Url::to(['document/outgoing/view', 'id' => $model->id]).'" class="btn btn-xs btn-outline-info" ><i class="la la-eye ml-1"></i></a>';
                                },
                                'delete' => function($url, $model){
                                    return '<a href="'.Url::to(['document/outgoing/delete', 'id' => $model->id]).'" class="btn btn-xs btn-outline-danger" ><i class="la la-trash ml-1"></i></a>';
                                },

                                'update' => function($url, $model){
                                    return '<a href="'.Url::to(['document/outgoing/update', 'id' => $model->id]).'" class="btn btn-xs btn-outline-primary"><i class="la la-pencil ml-1"></i></a>';
                                }
                            ],
                            'visibleButtons' => [
                                'update' => function($model) {
                                    return ($model->status == 1 || $model->status == 2);
                                },
                                'delete' => function($model) {
                                    return ($model->status == 1 || $model->status == 2);
                                },
                                'view' => function() {
                                    return true;
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <input type="submit" class="offcanvas click-button-ajax">
                <? \yii\bootstrap\ActiveForm::end(); ?>
                <!--end::Search Form-->
                <!--end: Search Form-->
            </div>
        </div>
