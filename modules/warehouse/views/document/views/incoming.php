<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $models app\modules\warehouse\models\Item */

$slug = Yii::$app->request->get('slug');
$this->title = $models->doc_number;
$slug = Yii::$app->request->get('slug');

$this->title = Yii::t('app', '{document_type}  №{number} - {date}', [
    'number' => $models->doc_number,
    'date' => date('d.m.Y', strtotime($models->reg_date)),
    'document_type' => $models->getSlugLabel()
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '{document_type}',
    ['document_type' => $models->getSlugLabel()]), 'url' => ["index", 'slug' => $this->context->slug]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

    <div class="tovar-form">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header mt-5">
                <div class="col-xs-8">
                    <h4><?=Yii::t('app','Document Number:'); ?> <?= Html::encode($this->title) ?></h4>
                </div>
                <div class="no-print col-xs-4 text-right">
                    <div class="dropdown dropdown-inline mr-2">
                        <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="svg-icon svg-icon-md">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24" /><path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" /><path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" /></g></svg></span><?=Yii::t('app', 'Export');?></button>
                        <!--begin::Dropdown Menu-->
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi flex-column navi-hover py-2">
                                <li class="navi-item">
                                    <button href="#" class="navi-link button-print" style="border: none!important; background-color: white;">
                                        <span class="navi-icon"><i class="la la-print"></i></span>
                                        <span class="navi-text">Print</span>
                                    </button>
                                </li>
                                <li class="navi-item">
                                    <a href="<?=\yii\helpers\Url::to(['document/incoming/print', 'id' => $models->id]); ?>" class="navi-link">
                                        <span class="navi-icon"><i class="la la-copy"></i></span>
                                        <span class="navi-text"><?=Yii::t('app', 'Excel'); ?></span>
                                    </a>
                                </li>
                            </ul>
                            <!--end::Navigation-->
                        </div>
                        <!--end::Dropdown Menu-->
                    </div>
                    <a href="<?= \yii\helpers\Url::to(['document/incoming/index']) ?>"
                       class="btn btn-sm btn-outline-primary font-weight-bolder">
                        <i class="la la-backspace"></i><?= Yii::t('app', 'Назад'); ?>
                    </a>
                </div>
                <?php if ($models->status == '1'): ?>
                    <div class="no-print col-sm-12 mt-1 mb-1" style="margin-left: -16px; ">
                        <a class="btn btn-sm btn-success" href="<?=\yii\helpers\Url::to(['document/save-and-finish', 'id' => $models->id, 'slug' => $slug])?>"><?=Yii::t('app', 'Save And Finish'); ?></a>
                        <a class="ml-2 btn btn-sm btn-outline-primary" href="<?=\yii\helpers\Url::to(['document/update', 'id' => $models->id, 'slug' => $slug])?>"><?=Yii::t('app', 'Update'); ?></a>
                        <?php
                        echo Html::a(Yii::t('app', 'Delete'), \yii\helpers\Url::to(['delete', 'id' => $models->id, 'slug'=>$slug]), [
                            'class' => "ml-2 btn btn-sm btn-outline-danger",
                            'data' => [
                                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                                'method' => 'POST',
                            ],
                        ]);
                        ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="card-body">
                <table class="document-table">
                    <thead >
                        <tr>
                            <th><b><?=Yii::t('app','Document'); ?> No:</b> <span><?=$models->doc_number; ?></span></th>
                            <th><b><?=Yii::t('app', 'Reg date:')?></b> <span><?=$models->reg_date; ?></span></th>
                        </tr>
                        <tr>
                            <th><b><?=Yii::t('app', 'Qayerdan:'); ?></b> <span><?=!empty($models->contragent['name']) ? $models->contragent['name'] : NULL; ?></span></th>
                            <th><b><?=Yii::t('app', 'Qayerga:')?></b> <span><?=$models->toDepartment['name_'.Yii::$app->language]; ?></span></th>
                        </tr>
                        <tr>
                            <th><b><?=Yii::t('app', 'Javobgar Shaxs:')?></b> <span><?=$models->contragent_responsible; ?></span></th>
                            <th><b><?=Yii::t('app', 'Javobgar Shaxs:')?></b> <span><?=$models->to_employee; ?></span></th>
                        </tr>
                        <tr>
                            <th colspan="2"><b><?=Yii::t('app', 'Info:')?></b> <span><?=$models->add_info; ?></span></th>
                        </tr>
                    </thead>
                </table>
                <table class="item-table">
                    <thead>
                        <tr class="item-tr">
                            <th>#</th>
                            <th><?=Yii::t('app', 'Item ID'); ?></th>
                            <th><?=Yii::t('app', 'To Department Area'); ?></th>
                            <th><?=Yii::t('app', 'Document Quantity')?></th>
                            <th><?=Yii::t('app', 'Quantity')?></th>
                            <th><?=Yii::t('app', 'Income Price')?></th>
                            <th><?=Yii::t('app', 'Price')?></th>
                        </tr>
                    </thead>
                    <tbody class="item-tbody">
                        <?php $i = 1;?>
                        <?php foreach ($items as $item): ?>
                            <tr class="item-tbody-tr">
                                <td><?=$i++; ?></td>
                                <td><?=$item->item['name_'.Yii::$app->language]; ?></td>
                                <td><?=!empty($item->toDepArea['name']) ? $item->toDepArea['name'] : NULL; ?></td>
                                <td class="doc-quantity"><?=number_format($item->document_quantity,2, '.', ''); ?></td>
                                <td class="quantity"><?=number_format($item->quantity, 2, '.', ''); ?></td>
                                <td class="income-price"><span><?=number_format($item->income_price, 2, '.', ''); ?></span></td>
                                <td class="price"><span><?=number_format($item->price, 2, '.', ''); ?></span></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr class="footer-out">
                        <td colspan="3" class="margin-text"><b><?=Yii::t('app', 'Jami:')?></b></td>
                        <td class="text-right doc-quantity-out"></td>
                        <td class="text-right quantity-out"></td>
                        <td class="text-right income-out"></td>
                        <td class="text-right price-out"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
<?php
$css = <<< CSS
    .item-table {
        margin-top: 20px;
        font-family: Arial;
    }
    .item-table th {
        border: 1px solid gray;
    }
    .document-table th {
        border: 1px solid lightgrey;
        padding-left: 10px;
    }
    .item-tr th {
        padding-left: 5px;
        background-color: #0c5460;
        color: whitesmoke;
        border: 1px solid black;
    }
    .item-tbody-tr td {
        border: 1px solid black;
        padding-left: 5px;
    }
    .footer-out td {
        border: 1px solid black;
        padding-right: 5px;
        color: #0a73bb;
    }
    th span {
        color: gray;
        margin-left: 8px;
    }
    tr {
      height: 30px;
    }
    table {
      width: 100%;
    }
    hr {
        background-color: #0c5460;
        height: 2px;
    }
CSS;
$this->registerCss($css);
$js = <<< JS
    let tr = $('.item-tbody').find('tr');
    var doc_quantity = 0;
    var quantity = 0;
    var income = 0;
    var price_quantity = 0;
    var selling = 0;
    tr.map(function(index, item) {  
        doc_quantity += $(item).find('.doc-quantity').html()*1;
        quantity += $(item).find('.quantity').html()*1;
        income += $(item).find('.income-price').find('span').html()*1;
        price_quantity += $(item).find('.price').find('span').html()*1;
        selling += $(item).find('.sell-price').find('span').html()*1;
    });
     $('.doc-quantity-out').html('<b>'+doc_quantity+'</b>');
     $('.quantity-out').html('<b>'+quantity+'</b>');
     $('.income-out').html('<b>'+income+'</b>');
     $('.price-out').html('<b>'+price_quantity+'</b>');
     $('.sell-price-out').html('<b>'+selling+'</b>');
     
     // $('.button-print').click(function() {
     //     $('.no-print').css('display', 'none');
     //     window.print();
     // //     $('.no-print').css('display', 'block');
     // });
JS;
$this->registerJs($js);