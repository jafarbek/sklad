<?php

use app\modules\structure\models\Vat;
use app\modules\warehouse\models\Document;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\Document */
/* @var $models app\modules\warehouse\models\DocumentItem */
/* @var $form yii\widgets\ActiveForm */
$dep_area = \app\modules\warehouse\models\DepartmentArea::getHierarchy();
$income_currency = $model::getAllReferencesCurrency();
?>
<?= Html::dropDownList('documentItem_to_department_list', '', $dep_area, ['id' => 'documentItem_to_department_list', 'prompt' => 'Malumotlarni...', 'class' => 'offcanvas']) ?>
<?= Html::dropDownList('documentItem_income_currency_list', '', $income_currency, ['id' => 'documentItem_income_currency_list', 'prompt' => 'Malumotlarni...', 'class' => 'offcanvas']) ?>
<?= Html::dropDownList('documentItem_vat', '', [1=>'12'], ['id' => 'documentItem_vat_list', 'prompt' => Yii::t("app","Select ..."), 'class' => 'offcanvas']) ?>
<style>
    .form-control {
        border: solid 1px lightslategray;
    }

    .rmOrderId > div > span {
        width: 130px !important;
    }

    .rm_order {
        width: 100px !important;
    }
</style>
<div class="row justify-content-center mb-2">
    <div class="col-md-6 col-sm-12">
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'doc_number')->textInput(['maxlength' => true, 'class'=> 'form-control']) ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'reg_date')->input('datetime-local', ['id' => 'datePicker']) ?>
            </div>
        </div>
        <input type="hidden" name="Document[document_type]" value="<?= $model::DOC_TYPE_INCOMING ?>"
               class="offcanvas">

        <?= $form->field($model, 'contragent_id')->textInput()->widget(Select2::className(), [
            'data' => $model->getContgentsList(),
            'options' => [
                'disabled' => true,
            ],
        ]) ?>

        <?= $form->field($model, 'contragent_responsible')->textInput(['maxlength' => true, 'disabled' => true,]) ?>
    </div>
    <div class="col-md-6 col-sm-12">
        <?= $form->field($model, 'add_info')->textarea(['rows' => 1, 'disabled' => true]) ?>

        <?php $url = Url::to(['employee-ajax', 'slug' => $this->context->slug]);?>
        <?= $form->field($model, 'to_department')->widget(Select2::classname(), [
            'data' => $model->getDepartmentListReciving(),
            'pluginOptions' => [
                'disabled' => true,
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            ],
        ]); ?>

        <?= $form->field($model, 'to_employee')->widget(Select2::className(), [
            'options' => [
                'disabled' => true,
            ],
        ]) ?>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-sm-12">
        <div class="flex-wrap border-0 pt-6 pb-0 mt-7">
            <div class="col-sm-6 offset-3">
                <?php $url = Url::to(['search-ajax', 'slug' => $this->context->slug]); ?>
                <?php $url_item = Url::to(['search-ajax-item', 'slug' => $this->context->slug]); ?>
                <?= $form->field($model, 'search')->widget(Select2::classname(), [
                    'options' => ['placeholder' => 'Search for a city ...', 'id' => 'js-search-language', 'disabled' => true,],
                ]); ?>
            </div>
        </div>
        <br>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            <table class="table table-bordered table-hover table-incoming">
                <thead>
                <tr class="header-table-tabular">
                    <th>#</th>
                    <th><?= Yii::t('app', 'Item name') ?></th>
                    <th><?= Yii::t('app', 'To Dep Area') ?></th>
                    <th><?= Yii::t('app', 'Document Quantity') ?></th>
                    <th><?= Yii::t('app', 'Quantity') ?></th>
                    <th><?= Yii::t('app', 'Price') ?></th>
                    <th><?= Yii::t('app', 'Currency') ?></th>
                    <th><?= Yii::t('app', 'VAT') ?></th>
                    <th><?= Yii::t('app', 'Sum') ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody class="body-table-tabular" data-number-tbody="<?= !empty($models)? count($models) : 0;?>">
                <?php $i=1;$vat = Vat::find()->where(['department_id' => $model->id, 'status' => $model::STATUS_ACTIVE])->asArray()->one();
                if (!empty($models)) : ?>

                    <?php foreach($models as $key): ?>
                        <tr class="multiple-input-list__item">
                            <td><?=$i;?></td>
                            <td><input type="hidden" name="DocumentItem[<?=$i;?>][item_id]" class="form-control form-control-sm item_id" data-number="<?=$i;?>" value="<?=$key['item_id']?>"><span><?=$key['item_name']?></span></td>
                            <td>
                                <?php
                                echo Select2::widget([
                                    'name' => 'DocumentItem['.$i.'][to_dep_area]',
                                    'value' => $key['to_dep_area'],
                                    'data' => $dep_area,
                                    'options' => ['placeholder' => 'Select status...','class' => 'to_dep_area', 'disabled' => true,],
                                    'pluginOptions' => [
                                        'width' => '130px',
                                    ],
                                ]);
                                ?>
                            </td>
                            <td class="document-quantity"><input disabled type="number"  value="<?=number_format($key['document_quantity'],2)?>" name="DocumentItem[<?=$i;?>][document_quantity]" class="form-control form-control-sm" data-number="<?=$i;?>"></td>
                            <td class="quantity">
                                <input type="number" value="0"  name="DocumentItem[<?=$i;?>][quantity]" class="quantity form-control form-control-sm" data-number="<?=$i;?>">
                                <label class="warning text-center" style="color: red;width: 100%"></label>
                            </td>
                            <td class="income-price"><input disabled type="number" value="<?=number_format($key['income_price'],2, '.', '')?>"  name="DocumentItem[<?=$i;?>][income_price]" class="income-price form-control form-control-sm" data-number="<?=$i;?>"></td>
                            <td class='income_currency'><?=$key['token']?></td>
                            <td><span class="vat"><?= number_format($key['vat'], 2) ?></span> %</td>
                            <td class="summa-td">0</td>
                            <td>
                                <button class="btn btn-sm btn-outline-danger minus-button" disabled>
                                    <i class="la la-trash ml-1"></i>
                                </button>
                            </td>
                        </tr>
                        <?php $i++; endforeach; ?>
                <?php endif; ?>
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td><?= Yii::t('app', 'Jami:') ?></td>
                    <td></td>
                    <td id="footer-document-quantity">0</td>
                    <td id="footer-quantity">0</td>
                    <td id="footer-income-price">0</td>
                    <td></td>
                    <td></td>
                    <td id="footer-summa">0</td>
                    <td></td>
                </tr>
                </tfoot>
            </table>

        </div>
        <!--end::Card-->
    </div>
</div>
<?php
$url = Url::to(['return-ajax', 'slug' => $this->context->slug]);
$dep = $model->to_department;
$warning = Yii::t('app', 'In warehouse are not so many products!');
$warning_status_false = Yii::t('app', 'In warehouse are not this product!');
$js = <<<JS
    
   $('body').delegate('.multiple-input-list__item', 'blur', function(event) {
       let dep_area = $(this).find('.to_dep_area').val();
       let dep = '{$dep}';
       let item_id = $(this).find('.item_id').val();
       let vat = $(this).find('.vat').html()*1/100+1;
       let price = $(this).find('.income-price input').val() * vat;
       let td_this = $(this);
       $.ajax({
           type:'GET',
           url:'{$url}',
           data:{dep:dep,dep_area:dep_area,item_id:item_id,price:price},
           success:function(response){
               if(response.status && response.message[0].inventory * 1 != 0){
                   let item_balance_quantity = response.message[0].inventory * 1;
                   if ($(td_this).find('.quantity input').val()*1 > item_balance_quantity){
                       $(td_this).find('.quantity .warning').html('{$warning}');
                       $(td_this).find('.quantity input').val(item_balance_quantity);
                   } else {
                       $(td_this).find('.quantity .warning').html('');
                       $('.return-submit').prop("disabled", false);
                   }
               } else {
                   $(td_this).find('.quantity input').val(0);
                   $(td_this).find('.quantity .warning').html('{$warning_status_false}');
               }
           }
       });
       let quantity = $(this).find('.quantity').find('input').val() * 1;
       let income_price = $(this).find('.income-price').find('input').val() * 1;
       let summa_ = income_price * quantity * vat;
       let tr_body = $('.table-incoming').find('tbody').find('tr');
       $(this).find('.summa-td').html(summa_.toFixed(2));
       
       let all_quanty = 0;
       let all_doc_quanty = 0;
       let all_income_price = 0;
       let all_sum = 0;
       tr_body.map(function(index, item) {
            all_doc_quanty += $(item).find('.document-quantity input').val() * 1;    
            all_quanty += $(item).find('.quantity input').val() * 1;    
            all_income_price += $(item).find('.income-price input').val() * 1;    
            all_sum += $(item).find('.summa-td').text() * 1
       });
       $('#footer-document-quantity').html(all_doc_quanty);
       $('#footer-quantity').html(all_quanty);
       $('#footer-summa').html(all_sum);
       
   });

var now = new Date();
now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
$('#datePicker').val(now.toISOString().slice(0,16));
JS;
$this->registerJs($js);

?>
