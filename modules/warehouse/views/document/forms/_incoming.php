<?php

use app\modules\structure\models\Vat;
use app\modules\warehouse\models\Document;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\Document */
/* @var $models app\modules\warehouse\models\DocumentItem */
/* @var $form yii\widgets\ActiveForm */
$dep_area = \app\modules\warehouse\models\DepartmentArea::getHierarchy();
$income_currency = $model::getAllReferencesCurrency();
?>
<?= Html::dropDownList('documentItem_to_department_list', '', $dep_area, ['id' => 'documentItem_to_department_list', 'prompt' => 'Malumotlarni...', 'class' => 'offcanvas']) ?>
<?= Html::dropDownList('documentItem_income_currency_list', '', $income_currency, ['id' => 'documentItem_income_currency_list', 'prompt' => 'Malumotlarni...', 'class' => 'offcanvas']) ?>
<?= Html::dropDownList('documentItem_vat', '', [1=>'12'], ['id' => 'documentItem_vat_list', 'prompt' => Yii::t("app","Select ..."), 'class' => 'offcanvas']) ?>
<style>
    .form-control {
        border: solid 1px lightslategray;
    }

    .rmOrderId > div > span {
        width: 130px !important;
    }

    .rm_order {
        width: 100px !important;
    }
</style>
 <div class="row justify-content-center mb-2">
    <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'doc_number')->textInput(['maxlength' => true, 'class'=> 'form-control']) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'reg_date')->input('datetime-local', ['id' => 'datePicker']) ?>
                    </div>
                </div>
                <input type="hidden" name="Document[document_type]" value="<?= $model::DOC_TYPE_INCOMING ?>"
                       class="offcanvas">

                <?= $form->field($model, 'contragent_id')->textInput()->widget(Select2::className(), [
                    'data' => $model->getContgentsList(),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Выбирите поставщика'),
                    ],
                ]) ?>

                <?= $form->field($model, 'contragent_responsible')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6 col-sm-12">
                <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

                <?php $url = Url::to(['employee-ajax', 'slug' => $this->context->slug]);?>
                <?= $form->field($model, 'to_department')->widget(Select2::classname(), [
                    'data' => $model->getDepartmentListReciving(),
                    'options' => ['placeholder' => '', 'id' => 'to_department',],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'pluginEvents' => [
                        "select2:select" => "
                                function() {
                                    $('#js-search-language').prop(\"disabled\", false);
                                    $('tbody').html(''); 
                                    var id = $(this).val();
                                    $.ajax({
                                        type:'GET',
                                        url:'{$url}',
                                        data:{id:id,type:1},
                                        success:function(response){
                                            $('#to_employee').html('');
                                            $('#documentItem_vat_list').html('');
                                            if(response.status){
                                                if(response.vat.vat){
                                                    let optionVat = '<option value=\"\"></option>' + '<option value=\"'+response.vat.vat+'\">'+response.vat.vat+'</option>';
                                                    $('#documentItem_vat_list').html(optionVat);
                                                }
                                                $('#to_employee').append('<option value=\"\"></option>');
                                                let option = response.items.map(function(item,index){
                                                        return ('<option value=\"'+item.name+'\">'+item.name+'</option>');
                                                    });
                    
                                                let newOption = option.join('');
                                                $('#to_employee').html(newOption);
                                            }
                                        }
                                    })
                                }",
                    ],
                    'pluginOptions' => [
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    ]
                ]); ?>

                <?= $form->field($model, 'to_employee')->widget(Select2::className(), [
//                        'data'=> [1],
                    'options' => [
                        'id' => 'to_employee',
                        'placeholder' => Yii::t('app', 'Выбирите отдель'),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-sm-12">
            <div class="flex-wrap border-0 pt-6 pb-0 mt-7">
                    <div class="col-sm-6 offset-3">
                        <?php $url = Url::to(['search-ajax', 'slug' => $this->context->slug]); ?>
                        <?php $url_item = Url::to(['search-ajax-item', 'slug' => $this->context->slug]); ?>
                        <?= $form->field($model, 'search')->widget(Select2::classname(), [
                            'options' => ['placeholder' => 'Search for a city ...', 'id' => 'js-search-language'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) {
                                                let to_department = $("#to_department option:selected").val();
                                                return {
                                                    q: params.term,
                                                    dep: to_department,
                                                }; 
                                             },
                                             ')

                                ],
                                'templateResult' => new JsExpression('function(city) {console.log(city);return city.name; }'),
                                'templateSelection' => new JsExpression('function (city) {name_item = city; }'),
                            ],
                            'pluginEvents' => [
                                "select2:select" => "
                                    function() { 
                                    console.log(name_item);
                                        var id = $(this).val();
                                        $(this).val('');
                                        let nextNum = $('.body-table-tabular').data('number-tbody');
                                        let num = (typeof nextNum !== 'undefined') ? 1*nextNum+1 : 0;
                                           var tbody_tr_ichi = `
                                                <tr class=\"multiple-input-list__item\">
                                                    <td>`+ num +`</td>
                                                    <td><input type=\"hidden\" name=\"DocumentItem[`+num+`][item_id]\" class=\"form-control form-control-sm\" data-number=\"`+num+`\" value=\" `+id+`\"><span style='font-size: small'>`+ name_item.name +`</span></td>
                                                    <td>
                                                            <div class=\"col-md-3 rmOrderId\"> 
                                                                <div class=\"field-documentitem-`+num+`-to_dep_area required\">
                                                                    <select id=\"documentitem-`+num+`-to_dep_area\" class=\"from_to_department form-control\" name=\"DocumentItem[`+num+`][to_dep_area]\" indeks=\"`+num+`\" style=\"width: 150px!important;\"> 
                                                                        `+$('#documentItem_to_department_list').html()+` 
                                                                    </select> 
                                                                    <div class=\"help-block\"></div> 
                                                                </div> 
                                                                <div class=\"rmSpan\"></div> 
                                                            </div> 
                                                    </td>
                                                    
                                                    <td class=\"document-quantity\"><input type=\"text\" name=\"DocumentItem[`+num+`][document_quantity]\" class=\"form-control form-control-sm\" data-number=\"`+num+`\"></td>
                                                    <td class=\"quantity\"><input type=\"text\" name=\"DocumentItem[`+num+`][quantity]\" class=\"quantity form-control form-control-sm\" data-number=\"`+num+`\"></td>
                                                    <td class=\"income-price\"><input type=\"text\" name=\"DocumentItem[`+num+`][income_price]\" class=\"income-price form-control form-control-sm\" data-article=\"`+name_item.article+`\" data-number=\"`+num+`\"></td>>
                                                    <td class='income_currency'><input type=\"hidden\" name=\"DocumentItem[`+num+`][price_currency]\" value=\"`+ name_item.currency_id +`\" class=\"form-control form-control-sm\" data-number=\"`+num+`\">`+ name_item.currency +`</td>
                                                    <td>
                                                      <div class=\"col-md-3 rmOrderId\" style=''> 
                                                                <div class=\"field-documentitem-`+num+`-price_currency form-group required\"> 
                                                                    <select id=\"documentitem-`+num+`-price_currency\" class=\"form-control vat\" name=\"DocumentItem[`+num+`][vat]\" indeks=\"`+num+`\">
                                                                    `+$('#documentItem_vat_list').html()+`
                                                                    </select> 
                                                                    <div class=\"help-block\"></div> 
                                                                </div> 
                                                                <div class=\"rmSpan\"></div> 
                                                            </div> 
                                                    </td>
                                                    <td class=\"summa-td\">0</td>
                                                    <td>
                                                        <button class=\"btn btn-sm btn-outline-danger minus-button\">
                                                            <i class=\"la la-trash ml-1\"></i>
                                                        </button>
                                                    </td>
                                                </tr>    
                                            `;
            
                                        $('.body-table-tabular').data('number-tbody',num);
                                        $('.body-table-tabular').append(tbody_tr_ichi);
//                                        $('.plus-button').click();
                                        $.ajax({
                                        type:'GET',
                                        url:'{$url_item}',
                                        data:{id:id},
                                        success:function(response){
//                                            if(response.status){
//                                            }
                                        }
                                    });
                                    if (jQuery('#documentitem-'+num+'-to_dep_area').data('select2')) { 
                                    jQuery('#documentitem-'+num+'-to_dep_area').select2('destroy'); }
                                    jQuery.when(jQuery('#documentitem-'+num+'-to_dep_area').select2()).done(initS2Loading('documentitem-'+num+'-to_dep_area','s2options_d6851687'));
                                    if (jQuery('#documentitem-'+num+'-price_currency').data('select2')) { 
                                    jQuery('#documentitem-'+num+'-price_currency').select2('destroy'); }
                                    jQuery.when(jQuery('#documentitem-'+num+'-price_currency').select2()).done(initS2Loading('documentitem-'+num+'-price_currency','s2options_d6851687'));
//                                     if (jQuery('#documentitem-`+num+`-vat').data('select2')) { 
//                                    jQuery('#documentitem-`+num+`-vat').select2('destroy'); }
//                                    jQuery.when(jQuery('#documentitem-`+num+`-vat').select2()).done(initS2Loading('documentitem-`+num+`-vat','s2options_d6851687'));
                                    }",
                            ],
                        ]); ?>
                </div>
                </div>
                <div class="card-toolbar">
                <!--begin::Dropdown-->
                    <table class="table table-bordered table-hover table-incoming">
                        <thead>
                        <tr class="header-table-tabular">
                            <th>#</th>
                            <th><?= Yii::t('app', 'Item name') ?></th>
                            <th><?= Yii::t('app', 'To Dep Area') ?></th>
                            <th><?= Yii::t('app', 'Document Quantity') ?></th>
                            <th><?= Yii::t('app', 'Quantity') ?></th>
                            <th><?= Yii::t('app', 'Price') ?></th>
                            <th><?= Yii::t('app', 'Currency') ?></th>
                            <th><?= Yii::t('app', 'VAT') ?></th>
                            <th><?= Yii::t('app', 'Sum') ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody class="body-table-tabular" data-number-tbody="<?= !empty($models)? count($models) : 0;?>">
                        <?php $i=1;$vat = Vat::find()->where(['department_id' => $model->id, 'status' => $model::STATUS_ACTIVE])->asArray()->one();
                        if (!empty($models)) : ?>
                        <?php $this->registerJs("$(function(){
                        $('#js-search-language').prop('disabled', false);
                        });")?>
                            <?php foreach($models as $key): ?>
                                <tr class="multiple-input-list__item">
                                    <td><?=$i;?></td>
                                <td><input type="hidden" name="DocumentItem[<?=$i;?>][item_id]" class="form-control form-control-sm" data-number="<?=$i;?>" value="<?=$key['item_id']?>"><span><?=$key['item_name']?></span></td>
                                    <td>
                                        <?php
                                            echo Select2::widget([
                                                'name' => 'DocumentItem['.$i.'][to_dep_area]',
                                                'hideSearch' => true,
                                                'value' => $key['to_dep_area'],
                                                'data' => $dep_area,
                                                'options' => ['placeholder' => 'Select status...','class' => 'w-100'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'width' => '130px',
                                                ],
                                            ]);
                                        ?>
                                    </td>
                                    <td class="document-quantity"><input type="number"  value="<?=number_format($key['document_quantity'],2)?>" name="DocumentItem[<?=$i;?>][document_quantity]" class="form-control form-control-sm" data-number="<?=$i;?>"></td>
                                    <td class="quantity"><input type="number" value="<?=number_format($key['quantity'],2)?>"  name="DocumentItem[<?=$i;?>][quantity]" class="quantity form-control form-control-sm" data-number="<?=$i;?>"></td>
                                    <td class="income-price"><input type="number" value="<?=number_format($key['income_price'],2, '.', '')?>"  name="DocumentItem[<?=$i;?>][income_price]" class="income-price form-control form-control-sm" data-number="<?=$i;?>"></td>
                                    <td class='income_currency'><?=$key['token']?></td>
                                    <td>
                                        <?php
                                        $vat = Vat::findOne(['department_id' => $model->to_department, 'status' => Document::STATUS_ACTIVE])->vat ?? null;
                                        echo Select2::widget([
                                            'name' => 'DocumentItem['.$i.'][vat]',
                                            'hideSearch' => true,
                                            'data' => [$vat => $vat],
                                            'options' => ['placeholder' => 'Select status...', 'class' => 'vat'],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'width' => '130px',
                                            ],
                                        ]);
                                        ?>
                                    </td>
                                    <td class="summa-td">0</td>
                                    <td>
                                        <button class="btn btn-sm btn-outline-danger minus-button">
                                        <i class="la la-trash ml-1"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php $i++; endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td><?= Yii::t('app', 'Jami:') ?></td>
                            <td></td>
                            <td id="footer-document-quantity">0</td>
                            <td id="footer-quantity">0</td>
                            <td id="footer-income-price">0</td>
                            <td></td>
                            <td></td>
                            <td id="footer-summa">0</td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>

            </div>
    <!--end::Card-->
    </div>
</div>
<?php
$css = <<<CSS
    .multipleTabularInput-tfoot tr td{
        border:1px solid #cccccc;
        background:#fff;
    }
    #footer_item_id{
    text-indent: 10px;
    }
    .table-incoming {
    /*border: 5px solid red;*/
    }
    /*.table-incoming tr {
        height: 10px;
        border: 2px  solid green;
    }*/
CSS;
$this->registerCSS($css);

$url = Url::to(['employee-ajax', 'slug' => $this->context->slug]);
$js = <<<JS
    
    $(document).ready(function(event) {
        
        // $('#js-search-language').prop("disabled", true);
        $('.list-cell__button div').addClass('btn-sm');
       
        let quantity = $(this).find('.quantity').find('input').val() * 1;
       let income_price = $(this).find('.income-price').find('input').val() * 1;
       let vat = $(this).find('.vat').val() / 100 + 1;
       let summa_ = income_price * quantity * vat;
       let tr_body = $('.table-incoming').find('tbody').find('tr');
       $(this).find('.summa-td').html(summa_.toFixed(2));
       
       let all_quanty = 0;
       let all_doc_quanty = 0;
       let all_income_price = 0;
       let all_sum = 0;
       tr_body.map(function(index, item) {
            all_doc_quanty += $(item).find('.document-quantity input').val() * 1;    
            all_quanty += $(item).find('.quantity input').val() * 1;    
            all_income_price += $(item).find('.income-price input').val() * 1;    
            all_sum += $(item).find('.summa-td').text() * 1
       });
       $('#footer-document-quantity').html(all_doc_quanty);
       $('#footer-quantity').html(all_quanty);
       $('#footer-income-price').html(all_income_price);
       $('#footer-summa').html(all_sum);
    });
   $('body').delegate('.list-cell__button','click mousedown',function(event) {
     let tbody = $(this).parents('tbody');
     tbody.map(function(value,index){
        $(index).find('.list-cell__button div').addClass('btn-sm');
     });
   });
   $('body').delegate('.multiple-input-list__item', 'blur', function(event) {
       let quantity = $(this).find('.quantity').find('input').val() * 1;
       let income_price = $(this).find('.income-price').find('input').val() * 1;
       let vat = $(this).find('.vat').val() / 100 + 1;
       let summa_ = income_price * quantity * vat;
       let tr_body = $('.table-incoming').find('tbody').find('tr');
       $(this).find('.summa-td').html(summa_.toFixed(2));
       
       let all_quanty = 0;
       let all_doc_quanty = 0;
       let all_income_price = 0;
       let all_sum = 0;
       tr_body.map(function(index, item) {
            all_doc_quanty += $(item).find('.document-quantity input').val() * 1;    
            all_quanty += $(item).find('.quantity input').val() * 1;    
            all_income_price += $(item).find('.income-price input').val() * 1;    
            all_sum += $(item).find('.summa-td').text() * 1
       });
       $('#footer-document-quantity').html(all_doc_quanty);
       $('#footer-quantity').html(all_quanty);
       $('#footer-income-price').html(all_income_price);
       $('#footer-summa').html(all_sum);
       
   });
   $('.plus-button').click(function(e) {
        e.preventDefault();
        let nextNum = $('.body-table-tabular').data('number-tbody');
        let num = (typeof nextNum !== 'undefined') ? 1*nextNum+1 : 0;
        let trtr = $(this).parent('tr');
       // let tr = '<tr class="add-tr-plus">'+$('.body-one-tr').html()+'</tr>';
       $('.body-table-tabular').append(tbody_tr_ichi);
    });

$('body').delegate('.minus-button', 'click', function(e) {
    e.preventDefault();
    let tr = $(this).parents('tr').remove();
       let tr_body = $('.table-incoming').find('tbody').find('tr');
       let all_doc_quanty = 0;
       let all_quanty = 0;
       let all_income_price = 0;
       let all_sum = 0;
       tr_body.map(function(index, item) {
            all_doc_quanty += $(item).find('.document-quantity input').val() * 1;    
            all_quanty += $(item).find('.quantity input').val() * 1;    
            all_income_price += $(item).find('.income-price input').val() * 1;    
            all_sum += $(item).find('.summa-td').text() * 1
       });
       $('#footer-document-quantity').html(all_doc_quanty);
       $('#footer-quantity').html(all_quanty);
       $('#footer-income-price').html(all_income_price);
       $('#footer-income-price').html(all_income_price);
       $('#footer-summa').html(all_sum);
});
 
var now = new Date();
now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
$('#datePicker').val(now.toISOString().slice(0,16));
JS;
$this->registerJs($js);

?>
