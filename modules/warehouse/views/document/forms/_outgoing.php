<?php

use kartik\select2\Select2;
use unclead\multipleinput\TabularInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\Document */
/* @var $models app\modules\warehouse\models\DocumentItem */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .form-control {
        border: solid 1px #595959;
    }
</style>
    <div class="row justify-content-center">
        <div class="col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <?= $form->field($model, 'doc_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'reg_date')->input('datetime-local', ['id' => 'datePicker']) ?>
                        </div>
                    </div>

                    <input type="hidden" name="Document[document_type]" value="<?=$model::DOC_TYPE_OUTGOING?>" class="offcanvas">

                    <?php $url = Url::to(['employee-ajax','slug' => $this->context->slug]);?>
                    <?= $form->field($model, 'from_department')->widget(Select2::classname(), [
                        'data' => $model->getDepartmentListOutPuting(),
                        'options' => ['placeholder' => '','id'=>'from_department',],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'pluginEvents' => [
                            "select2:select" => "
                                function() { 
                                    $('#js-search-language').prop(\"disabled\", false);
                                    $('tbody').html(''); 
                                    $('#from_employee').html('');
                                    var id = $(this).val();
                                    $.ajax({
                                        type:'GET',
                                        url:'{$url}',
                                        data:{id:id,type:0},
                                        success:function(response){
                                            $('#to_employee').html('');
                                            if(response.status){
                                                $('#from_employee').append('<option></option>');
                                                let option = response.items.map(function(item,index){
                                                        return ('<option value=\"'+item.name+'\">'+item.name+'</option>');
                                                    });
                    
                                                    let newOption = option.join('');
                                                    $('#from_employee').html($('#from_employee').html() + newOption);
                                            }
                                        }
                                    })
                                }",
                        ],
                        'pluginOptions' => [
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        ]
                    ]); ?>

                    <?= $form->field($model, 'from_employee')->widget(Select2::className(), [
//                        'data'=> [1],
                        'options'=>[
                            'id' => 'from_employee',
                            'placeholder'=>Yii::t('app', 'Выбирите отдель'),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]) ?>


        </div>
        <div class="col-md-6">

                    <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

                    <?= $form->field($model, 'contragent_id')->textInput()->widget(Select2::className(), [
                        'data'=> $model->getContgentsList(),
                        'options'=>[
                            'placeholder'=>Yii::t('app', 'Выбирите поставщика'),
                        ],
                    ]) ?>

                    <?= $form->field($model, 'contragent_responsible')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-sm-12">
            <!--begin::Card-->
                <div class="flex-wrap border-0 pt-6 pb-0 mt-7">
                        <div class="col-sm-6 offset-3">
                            <?php $url_item = Url::to(['search-ajax-item-balanse','slug' => $this->context->slug]);?>
                            <?= $form->field($model, 'search')->widget(Select2::classname(), [
                                'options' => ['placeholder' => 'Search for  ...', 'id'=>'js-search-language'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url_item,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) {
                                                let from_department = $("#from_department option:selected").val();
                                                return {
                                                    q: params.term,
                                                    fd: from_department
                                                }; 
                                             },
                                             ')
                                    ],
                                    'templateResult' => new JsExpression('function(city) {
                                    if (city.price == null) {
                                        city.price = 0;
                                    }
                                    if (city.currency == null) {
                                        city.currency = "";
                                    }
                                    console.log(city);
                                    return city.item_name+"/"+city.dep_name+"/"+(city.price*100/(city.doc_item_price/city.income_price*100))+" "+city.currency+"/"+(city.doc_item_price/city.income_price*100)+"%"; }'),
                                    'templateSelection' => new JsExpression('function (city) {city.price = city.price*100/(city.doc_item_price/city.income_price*100); name_item = city;}'),
                                ],
                                'pluginEvents' => [
                                    "select2:select" => "
                                    function() { 
                                        var id = $(this).val();
                                        $(this).val('');
                                        let nextNum = $('.body-table-tabular').data('number-tbody');
                                        let num = (typeof nextNum !== 'undefined') ? 1*nextNum+1 : 0;
                                        vat  = name_item.doc_item_price/name_item.income_price*100;
                                        if (vat == '0') {
                                            name_item.price = 0;
                                        }    
                                        console.log(name_item);
                                           var tbody_tr_ichi = `
                                                <tr class=\"multiple-input-list__item\">
                                                    <td>`+ num +`</td>
//                                                  <td><input type=\"hidden\" name=\"DocumentItem[`+num+`][item_id]\" class=\"form-control form-control-sm\" data-number=\"`+num+`\" value=\" `+name_item.item_id+`\"><span>`+ name_item.item_name +`</span></td>
                                                    <td><input type=\"hidden\" name=\"DocumentItem[`+num+`][from_dep_area]\" class=\"form-control form-control-sm\" data-number=\"`+num+`\" value=\" `+name_item.dep_id+`\"><span>`+ name_item.dep_name +`</span></td>
                                                    <td>`+ (name_item.quantity*1).toFixed(2) +`</td>
                                                    <td class=\"quantity\"><input type=\"number\" name=\"DocumentItem[`+num+`][quantity]\"  data-article=\"`+name_item.department_area+name_item.article+`\" data-quantity=\"`+name_item.quantity+`\" data-number=\"`+num+`\"  class=\"quantity form-control form-control-sm\" value=\"\"></td>
                                                    <td class=\"income-price\"><input type=\"hidden\" name=\"DocumentItem[`+num+`][income_price]\" class=\"income-price form-control form-control-sm\" data-number=\"`+num+`\" value=\"`+ name_item.income_price +`\"><span>`+ (name_item.income_price*1).toFixed(2) +`</span></td>
                                                    <td ><input type=\"hidden\" name=\"DocumentItem[`+num+`][price_currency]\" class=\"form-control form-control-sm\" data-number=\"`+num+`\" value=\"`+ name_item.currency_id +`\"><span>`+ name_item.currency +`</span></td>
                                                    <td class=\"vat\"><input type=\"hidden\" name=\"DocumentItem[`+num+`][vat]\" class=\"form-control form-control-sm\" data-number=\"`+num+`\" value=\"`+ vat +`\"><span>`+ vat +`</span></td>
                                                    <td class=\"summa-td\">0</td>
                                                    <td>
                                                        <button class=\"btn btn-sm btn-outline-danger minus-button\">
                                                            <i class=\"la la-trash ml-1\"></i>
                                                        </button>
                                                    </td>
                                                </tr>    
                                            `;
                                        $('.body-table-tabular').data('number-tbody',num);
                                        $('.body-table-tabular').append(tbody_tr_ichi);
                                        $('.multiple-input-list__item').blur();
                                        $.ajax({
                                        type:'GET',
                                        url:'{$url_item}',
                                        data:{id:id},
                                        success:function(response){
                                        }
                                    });
                                    if (jQuery('#documentitem-'+num+'-price_currency').data('select2')) { 
                                    jQuery('#documentitem-'+num+'-price_currency').select2('destroy'); }
                                    jQuery.when(jQuery('#documentitem-'+num+'-price_currency').select2(select2_b1d16fb8)).done(initS2Loading('documentitem-'+num+'-price_currency','s2options_d6851687'));
                                    }",
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Dropdown-->
                        <table class="table table-bordered table-hover table-incoming">
                            <thead>
                            <tr class="header-table-tabular">
                                <th>#</th>
                                <th><?=Yii::t('app', 'Item name')?></th>
                                <th><?=Yii::t('app', 'From Dep Area')?></th>
                                <th colspan="2"><?=Yii::t('app', 'Quantity')?></th>
                                <th><?=Yii::t('app', 'Income Price')?></th>
                                <th><?=Yii::t('app', 'Currency')?></th>
                                <th><?=Yii::t('app', 'Vat')?></th>
                                <th><?=Yii::t('app', 'Sum')?></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody class="body-table-tabular" data-number-tbody="<?= !empty($models)? count($models) : 0;?>">
                            <?php $i=1;
                            if (!empty($models)) : ?>
                                <?php foreach($models as $key): ?>
                                    <tr class="multiple-input-list__item">
                                        <td><?=$i;?></td>
                                        <td><input type="hidden" name="DocumentItem[<?=$i;?>][item_id]" class="form-control form-control-sm" data-number="<?=$i;?>" value="<?=$key['item_id'];?>"><span><?=$key['item_name'];?></span></td>
                                        <td><input type="hidden" name="DocumentItem[<?=$i;?>][from_dep_area]" class="form-control form-control-sm" data-number="<?=$i;?>" value="<?=$key['from_dep_area']?>"><span><?=\app\modules\warehouse\models\DepartmentArea::findOne(['id' => $key['from_dep_area']])->name?></span></td>
                                        <td><?=number_format($key['inventory'],2)?></td>
                                        <td class="quantity"><input data-quantity="<?=$key['inventory']?>" data-article="<?=$key['department_name'].$key['article'] ?? ''?>"  type="number" name="DocumentItem[<?=$i;?>][quantity]" class="quantity form-control form-control-sm" data-number="<?=$i;?>" value="<?=number_format($key['quantity'],2)?>"></td>
                                        <td class="income-price"><input type="hidden" name="DocumentItem[<?=$i;?>][income_price]" class="income-price form-control form-control-sm" data-number="<?=$i;?>" value="<?=$key['income_price']?>"><span><?=number_format($key['income_price'],2)?></span></td>
                                        <td ><input type="hidden" name="DocumentItem[<?=$i;?>][price_currency]" class="form-control form-control-sm" data-number="<?=$i;?>" value="<?=$key['price_currency']?>"><span><?=$key['token']?></span></td>
                                        <td class="vat"><input type="hidden" name="DocumentItem[<?=$i;?>][vat]" class="form-control form-control-sm" data-number="<?=$i;?>" value="<?=!empty($key['vat']) ? $key['vat'] : 0 ?>"><span><?=!empty($key['vat']) ? number_format($key['vat'], 2) : 0 ?></span></td>
                                        <td class="summa-td">0</td>
                                        <td>
                                            <button class="btn btn-sm btn-outline-danger minus-button">
                                                <i class="la la-trash ml-1"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $i++; endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td></td>
                                <td><?=Yii::t('app', 'Jami:')?></td>
                                <td></td>
                                <td colspan="2" id="footer-quantity">0</td>
                                <td id="footer-income-price">0</td>
                                <td></td>
                                <td></td>
                                <td id="footer-summa">0</td>
                                <td></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
            </div>
    </div>

<?php
$js = <<<JS
$('body').delegate('.quantity input', 'keyup', function(e) {
    let quanty = 0;
    let all_quanty = $(this).attr('data-quantity')*1;
    let item_this = $(this);
    $('.quantity input').map(function(index, item) {
        if ($(this).attr('data-article') == $(item_this).attr('data-article')){
            quanty += $(item).val()*1;
        }
    });
    if (all_quanty < quanty){
        $(this).val(all_quanty-(quanty - $(this).val()*1));
    }
});
    $(document).ready(function(event) {
          $('.list-cell__button div').addClass('btn-sm');
          let quantity = $(this).find('.quantity').find('input').val() * 1;
           let income_price = $(this).find('.income-price').find('input').val() * 1;
           let vat = $(this).find('.vat span').html() / 100 + 1;
           let summa_ = income_price * quantity * vat;
           let tr_body = $('.table-incoming').find('tbody').find('tr');
           $(this).find('.summa-td').html(summa_.toFixed(2));
           
           let all_doc_quanty = 0;
           let all_quanty = 0;
           let all_income_price = 0;
           let all_sum = 0;
           tr_body.map(function(index, item) {
                all_doc_quanty += $(item).find('.document-quantity input').val() * 1;    
                all_quanty += $(item).find('.quantity input').val() * 1;    
                all_income_price += $(item).find('.income-price input').val() * 1;    
                all_sum += $(item).find('.summa-td').text() * 1
           });
           $('#footer-document-quantity').html(all_doc_quanty);
           $('#footer-quantity').html(all_quanty);
           $('#footer-income-price').html(all_income_price);
           $('#footer-summa').html(all_sum);
           
           
           
           $('.sql').click(function(e) {
             e.preventDefault();
             $.ajax({
                type:"GET",
                url:'http://example.loc/warehouse/document/outgoing/unversial-ajax',
                data:{select:"id,name_uz,status",from:"public.item",condition:""},
                success: function(response){
                    console.log(response);
                }
             })
           });
    });
   $('body').delegate('.list-cell__button','click mousedown',function(event) {
     let tbody = $(this).parents('tbody');
     tbody.map(function(value,index){
        $(index).find('.list-cell__button div').addClass('btn-sm');
     });
   });
   function Hisoblash(){
       $('body').delegate('.multiple-input-list__item', 'blur', function(event) {
       let quantity = $(this).find('.quantity').find('input').val() * 1;
           let income_price = $(this).find('.income-price').find('input').val() * 1;
           let vat = $(this).find('.vat span').html() / 100 + 1;
           let summa_ = income_price * quantity * vat;
           let tr_body = $('.table-incoming').find('tbody').find('tr');
           $(this).find('.summa-td').html(summa_.toFixed(2));
           
           let all_doc_quanty = 0;
           let all_quanty = 0;
           let all_income_price = 0;
           let all_sum = 0;
           tr_body.map(function(index, item) {
                all_doc_quanty += $(item).find('.document-quantity input').val() * 1;    
                all_quanty += $(item).find('.quantity input').val() * 1;    
                all_income_price += $(item).find('.income-price input').val() * 1;    
                all_sum += $(item).find('.summa-td').text() * 1
           });
           $('#footer-document-quantity').html(all_doc_quanty);
           $('#footer-quantity').html(all_quanty);
           $('#footer-income-price').html(all_income_price);
           $('#footer-summa').html(all_sum);
       
   });
   }
   Hisoblash();
   $('.plus-button').click(function(e) {
        e.preventDefault();
        let nextNum = $('.body-table-tabular').data('number-tbody');
        let num = (typeof nextNum !== 'undefined') ? 1*nextNum+1 : 0;
        let trtr = $(this).parent('tr');
       // let tr = '<tr class="add-tr-plus">'+$('.body-one-tr').html()+'</tr>';
       $('.body-table-tabular').append(tbody_tr_ichi);
    });

$('body').delegate('.minus-button', 'click', function(e) {
    e.preventDefault();
    let tr = $(this).parents('tr').remove();
       let tr_body = $('.table-incoming').find('tbody').find('tr');
       let all_quanty = 0;
       let all_income_price = 0;
       let all_sum = 0;
       tr_body.map(function(index, item) {
            all_quanty += $(item).find('.quantity input').val() * 1;    
            all_income_price += $(item).find('.income-price input').val() * 1;    
            all_sum += $(item).find('.summa-td').text() * 1
       });
       $('#footer-quantity').html(all_quanty);
       $('#footer-income-price').html(all_income_price);
       $('#footer-summa').html(all_sum);
});
 
var now = new Date();
now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
$('#datePicker').val(now.toISOString().slice(0,16));
//$('.bread_on').click(function(e) {
//    // e.preventDefault();
//    let url = $(this).children('a').attr('href');
//    if (url.length ==25){
//        $(this).children('a').attr('href','url_index');
//    }
//    
//})
JS;
$this->registerJs($js);
?>