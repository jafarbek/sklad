<?php

use app\modules\warehouse\models\Document;
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\Document */
/* @var $models app\modules\warehouse\models\DocumentItem */
$slug = $this->context->slug;
$this->title = Yii::t('app', 'Create Document');

$this->params['breadcrumbs'][] =
    ['label' => Yii::t('app', 'Document')." (".Document::getDocTypeBySlug($this->context->slug).")",
        'url' => ['index','slug' => $this->context->slug]];
$this->params['breadcrumbs'][] = $this->title;


?>
    <div class="wh-document-form card card-body">

        <?php $form = ActiveForm::begin(['options' => ['id' => 'form-nazorat']]); ?>
        <?= $this->render("forms/_return_{$this->context->slug}", [
            'model' => $model,
            'form' => $form,
            'models' => $models,
        ]); ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'return-submit btn btn-sm btn-outline-success btn-custom-doc mt-4']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
<?php $this->registerJs("$(function(){
    $('#js-search-language').prop('disabled', true);
});")?><?php
