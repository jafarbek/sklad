<?php
/* @var $this yii\web\View */
/* @var $models app\modules\warehouse\models\Item */
$this->title = Yii::t('app', 'Document Number {number}', [
    'number' => $models->doc_number,
]);
$document_quantity = 0;
$quantity = 0;
$income_price = 0;
$all_price = 0;
$sell_price = 0;
header("Content-Type: application/xls");   // excel fayllar bilan ishlash uchun ulash
header("Content-Disposition: attachment; filename={$this->title}.xls"); // ekrandagi ma'lumotlarni excelga yozish
?>
<table border="1" width="100%">
    <thead>
    <tr>
        <td colspan="8"><?=Yii::t('app','Document'); ?></td>
    </tr>
    <tr>
        <th colspan="4"><?=Yii::t('app','Document'); ?> No: <span><?=$models->doc_number; ?></span></th>
        <th colspan="4"><?=Yii::t('app', 'Reg date:')?> <span><?=$models->reg_date; ?></span></th>
    </tr>
    <tr>
        <th colspan="4"><b><?=Yii::t('app', 'Qayerdan:'); ?></b> <span><?= !empty($models->fromDepartment['name_'.Yii::$app->language]) ? $models->fromDepartment['name_'.Yii::$app->language] : " "; ?></span></th>
        <th colspan="4"><b><?=Yii::t('app', 'Qayerga:')?></b> <span><?= !empty($models->contragent['name']) ? $models->contragent['name'] : NULL; ?></span></th>

    </tr>
    <tr>
        <th colspan="4"><b><?=Yii::t('app', 'Javobgar Shaxs:')?></b> <span><?=$models->to_employee; ?></span></th>
        <th colspan="4"><b><?=Yii::t('app', 'Javobgar Shaxs:')?></b> <span><?=$models->contragent_responsible; ?></span></th>

    </tr>
    <tr>
        <th colspan="4"><?=Yii::t('app', 'Info:')?> <span><?=$models->add_info; ?></span></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="8"><?=Yii::t('app', 'Item ID'); ?></td>
    </tr>
    <tr>
        <th width="30px">#</th>
        <th><?=Yii::t('app', 'Item ID'); ?></th>
        <th><?=Yii::t('app', 'From Department Area'); ?></th>
        <th><?=Yii::t('app', 'Document Quantity')?></th>
        <th><?=Yii::t('app', 'Quantity')?></th>
        <th><?=Yii::t('app', 'Income Price')?></th>
        <th><?=Yii::t('app', 'Price')?></th>
        <th><?=Yii::t('app', 'Selling Price')?></th>
    </tr>
    <?php $i = 1;?>
    <?php foreach ($items as $item): ?>
        <tr>
            <td><?=$i++; ?></td>
            <td><?=$item->item['name_'.Yii::$app->language]; ?></td>
            <td><?= !empty($item->fromDepArea->name) ? $item->fromDepArea->name : ' '?></td>
            <td>
                <?php
                echo number_format($item->document_quantity,2, '.', '');
                $document_quantity += $item->document_quantity*1;
                ?>
            </td>
            <td><?php
                echo number_format($item->quantity, 2, '.', '');
                $quantity += $item->quantity*1;
                ?>
            </td>
            <td>
                <?php
                echo number_format($item->income_price, 2, '.', '');
                $income_price += $item->income_price*1;
                ?>
            </td>
            <td>
                <?php
                echo number_format($item->price, 2, '.', '');
                $all_price += $item->price*1;
                ?>
            </td>
            <td><?php
                    echo number_format($item->sell_price, 2, '.', '');
                    $sell_price += $item->sell_price;
                ?>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="3"><?=Yii::t('app', 'Jami:')?></th>
        <th><?=$document_quantity; ?></th>
        <th><?=$quantity; ?></th>
        <th><?=$income_price; ?></th>
        <th><?=$all_price; ?></th>
        <th><?=$sell_price; ?></th>
    </tr>
    </tfoot>
</table>