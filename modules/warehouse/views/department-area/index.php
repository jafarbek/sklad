<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\warehouse\models\DepartmentAreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $department_area_tree yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Department Areas');
$this->params['breadcrumbs'][] = $this->title;
?>
        <!--begin::Todo Docs-->
        <div class="d-flex flex-row">
            <!--begin::Aside-->
            <div class="flex-row-auto w-250px w-xxl-275px" id="kt_todo_aside">
                <!--begin::Card-->
                <div class="card card-custom card-stretch">
                    <!--begin::Body-->
                    <div class="card-body px-5">
                        <!--begin:Nav-->
                        <button disabled class="btn btn-sm-button btn-sm-left btn-outline-info add-tree save-parent" id="kt_demo_panel_toggle"
                                href="<?= Url::to(['department-area/create']) ?>"><i class="la la-plus ml-1"></i></button>
                        <button class="btn btn-sm-button btn-sm-left btn-outline-success add-tree click_view" id="kt_quick_cart_toggle"
                                href="<?= Url::to(['department-area/create']) ?>"><i class="la la-tree ml-1"></i></button>
                        <button disabled class="btn btn-sm-button btn-sm-left btn-outline-danger delete-tree"
                                href="<?= Url::to(['department-area/delete-ajax']) ?>"><i class="la la-trash ml-1"></i>
                        </button>
                        <button disabled class="btn btn-sm-button btn-sm-left btn-outline-primary update-parent" id="" href="<?=Url::to(['department-area/update']); ?>"><i class="la la-pen ml-1"></i></button>

                        <br><br>
                        <div id="kt_tree_1" class="tree-demo">
                            <?= $department_area_tree ?>
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Aside-->
            <!--begin::List-->
            <div class="flex-row-fluid d-flex flex-column ml-4">
                <div class="d-flex flex-column flex-grow-1">
                    <div class="card card-custom">
                        <div class="card-header">
                            <div class="card-title">
<!--                                <span class="card-icon">-->
<!--                                    <i class="flaticon2-favourite text-primary"></i>-->
<!--                                </span>-->
                                <div class="input-icon mr-2">
                                    <input type="text" class="form-control"
                                           placeholder="<?= Yii::t('app', 'Search'); ?>"
                                           id="kt_datatable_search_query"/>
                                    <span><i class="flaticon2-search-1 text-muted"></i></span>
                                </div>
                            </div>
                            <div class="card-toolbar">
                                <!--begin::Dropdown-->
                                <div class="dropdown dropdown-inline mr-2">
                                    <button type="button"
                                            class="btn btn-sm btn-light-primary font-weight-bolder dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="svg-icon svg-icon-md">
                                          <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                                          <svg xmlns="http://www.w3.org/2000/svg"
                                               xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                               height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none"
                                               fill-rule="evenodd">
                                              <rect x="0" y="0" width="24" height="24"/>
                                              <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"
                                                    fill="#000000" opacity="0.3"/>
                                              <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"
                                                    fill="#000000"/>
                                            </g>
                                          </svg>
                                            <!--end::Svg Icon-->
                                        </span><?= Yii::t('app', 'Export'); ?>
                                    </button>
                                    <!--begin::Dropdown Menu-->
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                        <!--begin::Navigation-->
                                        <ul class="navi flex-column navi-hover py-2">
                                            <li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">
                                                <?= Yii::t('app', 'Choose an option:') ?>
                                            </li>
                                            <li class="navi-item">
                                                <a href="#" class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="la la-print"></i>
                                                    </span>
                                                    <span class="navi-text">Print</span>
                                                </a>
                                            </li>
                                            <li class="navi-item">
                                                <a href="#" class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="la la-copy"></i>
                                                    </span>
                                                    <span class="navi-text">Copy</span>
                                                </a>
                                            </li>
                                            <li class="navi-item">
                                                <a href="#" class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="la la-file-excel-o"></i>
                                                    </span>
                                                    <span class="navi-text">Excel</span>
                                                </a>
                                            </li>
                                            <li class="navi-item">
                                                <a href="#" class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="la la-file-text-o"></i>
                                                    </span>
                                                    <span class="navi-text">CSV</span>
                                                </a>
                                            </li>
                                            <li class="navi-item">
                                                <a href="#" class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="la la-file-pdf-o"></i>
                                                    </span>
                                                    <span class="navi-text">PDF</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <!--end::Navigation-->
                                    </div>
                                    <!--end::Dropdown Menu-->
                                </div>
                                <!--end::Button-->
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <table class="table table-bordered table-hover table-checkable my-department-aria"  id="kt_datatable"
                                   style="margin-top: 13px !important"><!--id="kt_datatable"-->
                                <thead>
                                <tr>
                                    <th title="Field #1">ID</th>
                                    <th title="Field #2"><?= Yii::t('app', 'Name') ?></th>
                                    <th title="Field #3"><?= Yii::t('app', 'Code') ?></th>
                                    <th title="Field #4"><?= Yii::t('app', 'Department') ?></th>
                                    <!-- <th title="Field #1">--><?//= Yii::t('app', 'Parent') ?><!--</th>-->
                                    <th title="Field #5"><?= Yii::t('app', 'Add Info') ?></th>
                                    <th title="Field #6"><?= Yii::t('app', 'Status') ?></th>
                                    <th title="Field #7"><?= Yii::t('app', 'Action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <? if (!empty($models)): ?>
                                    <?php $til = Yii::$app->language; $i=1; ?>
                                    <?php foreach ($models as $key): ?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td><?= $key['name']; ?></td>
                                            <td><?= $key['code']; ?></td>
                                            <td><?= $key['dname_'.$til]; ?></td>
                                            <td><?= $key['add_info']; ?></td>
                                            <td><?= $key['status']; ?></td>
                                            <td>
                                                <button href="<?= \yii\helpers\Url::to(['department-area/update', 'id' => $key['id']]); ?>"
                                                        class="btn btn-icon btn-xs btn-outline-primary update-one"><i
                                                            class="la la-pencil-square-o"></i></button>

                                                <?= Html::a('<i class="la la-trash-restore-alt"></i>', \yii\helpers\Url::to(['department-area/remove', 'id' => $key['id']]), [
                                                    'class' => "btn btn-icon btn-xs btn-outline-danger",
                                                    'data' => [
                                                        'method' => 'post',
                                                    ],
                                                ]);
                                                ?>
                                            </td>
                                        </tr>
                                    <?php $i++; endforeach; ?>
                                <? endif; ?>
                                </tbody>
                            </table>
                            <!--end: Datatable-->
                        </div>
                    </div>

                </div>
            </div>
            <!-- end::List-->
        </div>
        <!--end::Todo Docs-->
<button id="kt_quick_panel_toggle" class="offcanvas"></button>
<?php
$massage = Yii::t('app','Delete Yes');
$massage1 = Yii::t('app','Delete Not ');
$js = <<< JS
    $('body').delegate('.jstree-anchor', 'click', function() {
        $('.save-parent').attr('disabled', false);
        $('.delete-tree').attr('disabled', false);
        $('.update-parent').attr('disabled', false);    
        $('#reset-parent').attr('disabled', false);
        $('.jstree-icon').css({'color':'#0c5460'});
        $('.jstree-anchor').css({'background-color':'white','color':'black'});
        $(this).css({'background-color':'#0c5460','color':'white'});
        $(this).find('.jstree-icon').css({'color':'white'});
    });
 $('body').delegate('#kt_demo_panel_toggle','click',function(e) {
     let url = $(this).attr('href');
     $('.right-modal-all').load(url);
     $('.save-parent').attr('disabled','true');
 })
  $('body').delegate('#kt_quick_cart_toggle', 'click', function(e) {
       e.preventDefault();
           // let url = $(this).attr('href');
           var value = $('#kt_tree_1 li');
           value.each(function(index, item) {
               if ($(item).attr('aria-selected') == 'true') {
                   $(item).attr('aria-selected', 'false');
               }
           });
           let url = $(this).attr('href');
           $('.create-modal').load(url);
   });
 $('body').delegate('.delete-tree', 'click', function(e) {
   e.preventDefault();
   let url=$(this).attr('href');
   var value = $('#kt_tree_1 li');
   var id;
   value.each(function(index, item) {
       if ($(item).attr('aria-selected') == 'true') {
           id = $(item).val()*1;
           f = $(item).attr('data-parent-id')*1;
       }
   });
       let bool = confirm("{$massage}");
    if( bool){
        $.ajax({
            url:url,
            data:{id:id},
            type:'GET',
            success: function(response){
                if(response){
                    var value = $('#kt_tree_1 li');
                    value.each(function(index, item) {
                        if ($(item).attr('aria-selected') == 'true') {
                            $(item).remove();
                        }
                    });
                } else{
                    alert("{$massage1}");
                }      
            }
        });
    }else {
        alert("{$massage1}");
    }
    $('.delete-parent').attr('disabled', true);
    $('.save-parent').attr('disabled', true);
    $('.update-parent').attr('disabled', true);
});
 $('body').delegate('.update-parent', 'click', function() {
    let url=$(this).attr('href');
    var value = $('#kt_tree_1 li');
    var id;
    value.each(function(index, item) {
        if ($(item).attr('aria-selected') == 'true') {
            id = $(item).val();
        }
    });
    url = url+'?id='+id;
    $('#kt_quick_panel_toggle').click();
    $('.update-modal').load(url);
    
   
});
  $('body').delegate('.update-one', 'click', function() {
    let url=$(this).attr('href');
    $('#kt_quick_panel_toggle').click();
    $('.update-modal').load(url);
   
});
 
  $('#department_area_create_button').click(function(e) {
    $('.save-parent').click();
  });

  $('#violators_tbl').dataTable({
    "aoColumnDefs": [
      { "sClass": "my-department-aria", "aTargets": [ 0 ] }
    ]
  } );

JS;
$this->registerJs($js)
?>
