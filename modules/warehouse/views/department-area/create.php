<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\DepartmentArea */

$this->title = Yii::t('app', 'Create Department Area');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Department Areas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


