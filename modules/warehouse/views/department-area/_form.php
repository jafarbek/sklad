<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\DepartmentArea */
/* @var $form yii\widgets\ActiveForm */
?>
    <?php $form = ActiveForm::begin([
            'id' => $model->formName(),
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'department_id')->widget(Select2::classname(), [
        'data' => $model->getDepartmentList(),
        'options' => ['placeholder' => 'Select a state ...'],//,'multiple'=>true
        'pluginOptions' => [
            'allowClear' => true,
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        ]
    ]);
    ?>

    <?= $form->field($model, 'parent_id')->label(false)->hiddenInput() ?>

    <?= $form->field($model, 'add_info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([
            $model::STATUS_ACTIVE => Yii::t('app','Active'),
            $model::STATUS_INACTIVE => Yii::t('app','Inactive')
    ]) ?>

    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary']) ?>

    <?php ActiveForm::end(); ?>

<?php
$js = <<< JS
$(document).ready(function(e) {
        $('.modal-title').html('{$this->title}');
        $('.modal-title1').html('{$this->title}');
});
$('body').delegate('input', 'keyup', function(e) {
    var value = $('#kt_tree_1 li');
    var id;
    value.each(function(index, item) {
        if ($(item).attr('aria-selected') == 'true') {
            id = $(item).val();
            $('#departmentarea-parent_id').val(id);
            $(item).find('.jstree-icon').css({'color':'rgb(12, 84, 96)'});
            $(item).find('.jstree-anchor').css({'background-color':'white','color':'black'});
            $(item).find('a').css({'color':'black'});
        }
    });
});

JS;
$this->registerJs($js)
?>
