<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\warehouse\models\ItemCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $tree */

$this->title = Yii::t('app', 'Item Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<button id="kt_demo_panel_toggle" class="offcanvas"></button>
        <div class="flex-row">
            <!--begin::Aside-->
            <div class="flex-row-auto" id="kt_todo_aside">
                <!--begin::Card-->
                <div class="card card-custom card-stretch">
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin:Nav-->
                        <button disabled class="btn btn-sm-button btn-outline-info create-parent" href="<?= Url::to(['item-category/create']) ?>"><i class="la la-plus ml-1"></i></button>

                        <button class="btn btn-sm-button btn-outline-success add-parent" href="<?= Url::to(['item-category/create']) ?>"><i class="la la-tree ml-1"></i></button>

                        <button disabled class="btn btn-sm-button btn-outline-danger delete-parent" href="<?= Url::to(['item-category/delete-ajax']); ?>"><i class="la la-trash ml-1"></i>
                        </button>
                        <button disabled class="btn btn-sm-button btn-outline-primary update-parent" href="<?= Url::to(['item-category/update']); ?>"><i class="la la-pen ml-1"></i></button>


                        <button disabled class="btn btn-sm-button btn-outline-warning reset-parent"><i class="la la-undo-alt ml-1"></i></button>


                        <br><br>
                        <div id="kt_tree_1" class="tree-demo">
                            <?= $tree ?>
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Aside-->
        </div>
<?php
$js = <<< JS
function colorAll(){
     $('.create-parent').attr('disabled', true);
    $('.reset-parent').attr('disabled', true);
    $('.update-parent').attr('disabled', true);
    $('.delete-parent').attr('disabled', true);
    $('.jstree-icon').css({'color':'#0c5460'});
    $('.jstree-anchor').css({'background-color':'white','color':'black'});
}


// elementlar
$('body').delegate('.jstree-anchor', 'click', function() {
    $('.create-parent').attr('disabled', false);
    $('.reset-parent').attr('disabled', false);
    $('.update-parent').attr('disabled', false);
    $('.delete-parent').attr('disabled', false);
    $('.jstree-icon').css({'color':'#0c5460'});
    $('.jstree-anchor').css({'background-color':'white','color':'black'});
   $(this).css({'background-color':'#0c5460','color':'white'});
   $(this).find('.jstree-icon').css({'color':'white'});
});

// update
$('body').delegate('.update-parent', 'click', function() {
    let url=$(this).attr('href');
    var value = $('#kt_tree_1 li');
    var id;
    value.each(function(index, item) {
        if ($(item).attr('aria-selected') == 'true') {
            id = $(item).val();
        }
    });
    url = url+'?id='+id;
    $('#kt_demo_panel_toggle').click();
    $('.right-modal-all').load(url);
    colorAll();
});

$('body').delegate('.reset-parent', 'click', function() {
    colorAll();
    $(this).attr('disabled', true);
});


// create +
$('body').delegate('.create-parent', 'click', function(e) {
    let url=$(this).attr('href');
    var value = $('#kt_tree_1 li');
    var id = '';
    value.each(function(index, item) {
        if ($(item).attr('aria-selected') == 'true') {
            id = $(item).val();
        }
    });
    url = url+'?id='+id;
      $('#kt_demo_panel_toggle').click();
    $('.right-modal-all').load(url);
    colorAll();
});

// daraxt
$('body').delegate('.add-parent', 'click', function(e) {
     let url=$(this).attr('href');
    var value = $('#kt_tree_1 li');
    var id = '0';
    url = url+'?id='+id;
      $('#kt_demo_panel_toggle').click();
    $('.right-modal-all').load(url);
    colorAll();
});

// delete
$('body').delegate('.delete-parent', 'click', function(e) {
    e.preventDefault();
    let url=$(this).attr('href');
    var value = $('#kt_tree_1 li');
    var id;
    value.each(function(index, item) {
        if ($(item).attr('aria-selected') == 'true') {
            id = $(item).val()*1;
        }
    });
    if(id > 0){
        $.ajax({
            url:url,
            data:{id:id},
            type:'GET',
            success: function(response){
                console.log(response);
                if(response){
                    alert("Ma'lumot o'chirildi");
                    var value = $('#kt_tree_1 li');
                    value.each(function(index, item) {
                        if ($(item).attr('aria-selected') == 'true') {
                            $(item).remove();
                        }
                    });
                }  else {
                    alert("Ma'lumot o'chirilmadi");
                }
            }
        });
    }
    colorAll();
});

JS;
$this->registerJs($js)
?>
