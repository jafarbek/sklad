<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\ItemCategory */
/* @var $form yii\bootstrap\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'id' => $model->formName(),
    'method' => 'post',
    'enableAjaxValidation' => true,
    'validationUrl' => \yii\helpers\Url::toRoute('item-category/validate'),
]); ?>

<?= $form->field($model, 'parent_id')->hiddenInput(['maxlength' => true, 'id' =>
    'parent-id-hidden'])->label(false); ?>

<?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>


<?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'status')->dropDownList([
    '1' => 'Active',
        '2' => 'Inactive',
]) ?>

        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary display-block']) ?>

<?php ActiveForm::end(); ?>

<?php
$js = <<< JS
$(document).ready(function(e) {
        $('.modal-title').html('{$this->title}');
        if ($('#parent-id-hidden').val() == '0') {
            $('#parent-id-hidden').val('');
        }
});
JS;
$this->registerJs($js)
?>
