<?php

use app\modules\admin\models\Users;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\ItemBalance */

$this->title = $model->lot;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Item Balances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="item-balance-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'item_id',
                'value' => function($model){
                    return $model->item['name_'.Yii::$app->language];
                },
            ],
            'lot',
            'quantity',
            'inventory',
            'reg_date',
            [
                'attribute' => 'department_id',
                'value' => function($model){
                    return $model->department['name_'.Yii::$app->language];
                },
            ],
            [
                'attribute' => 'department_area_id',
                'value' => function($model){
                    return $model->departmentArea['name'];
                },
            ],
            [
                'attribute' => 'document_id',
                'value' => function($model){
                    return $model->document['doc_number'];
                },
            ],
            //'document_item_id',
            'price',
            'price_currency',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model){
                    if ($model->status == '1'){
                        return '<span class="badge badge-info">Active</span>';
                    } else {
                        return '<span class="badge badge-info">Inactive</span>';
                    }
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    return (Users::findOne($model->created_by))
                        ? Users::findOne($model->created_by)->fullname . "<br><small>
                    <b style='font-size: small; color: green'>" . date('d.m.Y H:i', $model->created_at) . "</b></small>"
                        : $model->created_by;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'updated_by',
                'value' => function ($model) {
                    return (Users::findOne($model->updated_by))
                        ? Users::findOne($model->updated_by)->fullname . "<br><small>
                    <b style='font-size: small; color: red'>". date('d.m.Y H:i', $model->updated_at) . "</b></small>"
                        : $model->updated_by;
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
<?php
$js = <<<JS
  $('.modal-title').html('$this->title');
JS;
$this->registerJs($js);

?>