<?php

use app\modules\warehouse\models\ItemBalance;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\ItembalanceSearch */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="card">

        <div class="itembalance-search">

            <?php
            //             Pjax::begin([
            //                     'id' =>'search_item_balances'
            //             ]);
            $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1
                ],
                'id' => $model->formName(),


            ]); ?>

            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">

                            <?php echo $form->field($model, 'reg_date')->widget(DateTimePicker::classname(), [
                                'options' => ['placeholder' => Yii::t('app', 'Enter date ')],
                                'size' => DateTimePicker::SIZE_SMALL,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => "yyyy-mm-dd hh:ii:ss",
                                    'todayHighlight' => true,
                                    'todayBtn' => true,
                                    'startDate' => '2020-07-12 00:00:00',
                                    'todayClear' => true

                                ]
                            ]); ?>

                            <?php
                            if ($list) {
                                echo $form->field($model, 'department_area_id')
                                    ->widget(Select2::classname(), [
                                        'data' => $list,
                                        'size' => Select2::SMALL,
                                        'options' => [
                                            'placeholder' => Yii::t('app', 'Select'),
//                                    'id' => 'department_area_id',

                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ]);
                            } else {
                                echo $form->field($model, 'department_area_id')
                                    ->widget(Select2::classname(), [
//
                                        'size' => Select2::SMALL,
                                        'options' => [
                                            'placeholder' => Yii::t('app', 'Select'),
//                                    'id' => 'department_area_id',

                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ]);
                            }
                            ?>


                        </div>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'department_id')
                                                    ->widget(Select2::classname(), [
                                                    'data' => ItemBalance::getDepartmentList(),
                                                    'size' => Select2::SMALL,
                                                    'options' => [
                                                        'placeholder' => Yii::t('app', 'Select'),

                                                    ],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                    ],
                                                ]) ?>
                        <?php echo $form->field($model, 'item_id')->widget(Select2::classname(), [
                            'data' => ItemBalance::getItemList(),
                            'size' => Select2::SMALL,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select'),
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]) ?>
                        <p class="text-right">
                            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-outline-primary search_btn']) ?>
                            <?= Html::a(Yii::t('app', 'Cancel search'), ['item-balance/index'], [
                                'class' => 'btn btn-sm btn-outline-danger',
                            ]); ?>
                        </p>
                    </div>
                </div>

            </div>
            <?php ActiveForm::end();
            //            Pjax::end();
            ?>
        </div>
    </div>
<?php
$url = \yii\helpers\Url::to(['item-balance/department-area']);
$js = <<<JS
  
    
    
    $('#itembalancesearch-department_id').on('change', function() {
             let id= $(this).val();
             $.ajax({
                    url:'$url',
                    data:{q:id},
                    type:'GET',
                                success: function(response){ 
                                    if(response.status){                             
                                        $('#itembalancesearch-department_area_id').html('');
                                        
                                        let option = response.data.map(function(item,index){
                                                return ('<option value=\"'+item.id+'\">'+item.name+'</option>');
                                        });                
                                        let newOption = option.join('');
                                        $('#itembalancesearch-department_area_id').html('<option>'+" "+'</option>'+newOption);
                                    }else{ 
                                        let option = '<option></option>'
                                         let newOption = option.join('');
                                        $('#itembalancesearch-department_area_id').html().empty(); 
                                        $('#itembalancesearch-department_area_id').html().val('');  
                                         
                                    }              
                                }
             });
        });
JS;

$this->registerJS($js);
?>