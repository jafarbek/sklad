<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\Item */

$this->title = $model['name_'.Yii::$app->language];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name_en',
            'name_ru',
            'name_uz',
            'short_name',
            'category_id',
            'size',
            'weight',
            'unit_id',
            'article',
            'country_id',
            'add_info',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->status == $model::STATUS_ACTIVE) {
                        return "<span class='badge badge-info'>Active</span>";
                    } else if ($model->status == $model::STATUS_INACTIVE) {
                        return "<span class='badge badge-warning'>Inactive</span>";
                    }
                }
            ],
            'stock_limit',
        ],
    ]) ?>
<p>
    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-sm btn-outline-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>

<?php
$js = <<< JS
$(document).ready(function(e) {
        $('.modal-title').html('{$this->title}');
});
JS;
$this->registerJs($js)
?>