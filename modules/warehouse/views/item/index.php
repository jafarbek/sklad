<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\warehouse\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Items');
$this->params['breadcrumbs'][] = $this->title;
?>

<button id="#kt_demo_panel_toggle" class="offcanvas"></button>
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="col-lg-9 col-xl-8">
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            <!--end::Dropdown-->
            <!--begin::Button-->
            <a href="<?=\yii\helpers\Url::to(['item/create'])?>" class="btn btn-sm btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                         height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <circle fill="#000000" cx="9" cy="15" r="6"/>
                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                  fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span><?=Yii::t('app', 'Create')?></a>
            <!--end::Button-->
        </div>
    </div>
    <div class="separator separator-solid"></div>
    <div class="card-body">
        <? $form = \yii\bootstrap\ActiveForm::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary' => '',
            'columns' => [

                ['class' => 'yii\grid\SerialColumn'],

                'name_'.Yii::$app->language,
                [
                    'attribute' => 'category_id',
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'width: 140px'],
                    'value' => function($model){
                        return $model->category['name_'.Yii::$app->language];
                    },
                    'filter' => \app\modules\warehouse\models\Item::getItemCategoryList(),
                ],
                'size',
                'weight',
                [
                    'attribute' => 'unit_id',
                    'value' => function($model){
                        return $model->unit->token;
                    }
                ],
                'article',
                [
                    'attribute' => 'country_id',
                    'value' => function($model){
                        return $model->country->code;
                    }
                ],
                'stock_limit',
                [
                    'attribute' => 'status',
                    'headerOptions' => ['style' => 'width: 94px'],
                    'value' =>function($model){
                        $class = $model->status == 0 ? 'badge badge-warning' : 'badge badge-success';
                        $name = $model->status == 0 ? 'Inactive' : 'Active';
                        return "<span class='$class'>$name</span>";
                    },
                    'format' => 'raw',
                    'filter' => [
                        0 => Yii::t('app', 'Inactive'),
                        1 => Yii::t('app', 'Active')
                    ],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => Yii::t('app', "Action"),
                    'template' => '{view} {update} {delete}',
                    'options' => ['style' => 'width:90px;'],
                    'buttons' => [
                        'view' => function($url){
                            return '<a href="'.$url.'" class="btn btn-xs btn-outline-info view-modal-show" data-toggle="modal" data-target="#exampleModalCustomScrollable"><i class="la la-eye ml-1"></i></a>';
                        },
                        'delete' => function($url){
                            return '<a href="'.$url.'" class="btn btn-xs btn-outline-danger" ><i class="la la-trash ml-1"></i></a>';
                        },
                        'update' => function($url){
                            return '<a href="'.$url.'" class="btn btn-xs btn-outline-primary"><i class="la la-pencil ml-1"></i></a>';
                        }
                    ]
                ],
            ],
        ]); ?>
        <input type="submit" class="offcanvas click-button-ajax">
        <? \yii\bootstrap\ActiveForm::end(); ?>
    </div>
</div>