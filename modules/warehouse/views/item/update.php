<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\Item */
/* @var $item_category */
/* @var $unit */
/* @var $country */

$this->title = Yii::t('app', 'Update Item: {name}', [
    'name' => $model['name_'.Yii::$app->language],
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

    <?= $this->render('_form', [
        'model' => $model,
        'item_category' => $item_category,
        'unit' => $unit,
        'country' => $country
    ]) ?>
