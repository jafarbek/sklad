<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\warehouse\models\Item */
/* @var $form yii\widgets\ActiveForm */
/* @var $country */
/* @var $item_category */
/* @var $unit */
?>


    <div class="tovar-form">

        <?php $form = ActiveForm::begin([
            'class' => 'form'
        ]); ?>

        <div class="card card-custom gutter-b example example-compact">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'size')->textInput() ?>
                    </div>

                    <div class="col-lg-6">
                        <?= $form->field($model, 'weight')->textInput() ?>
                    </div>

                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'unit_id')->widget(Select2::classname(), [
                            'data' => $unit,
                            'options' => ['placeholder' => Yii::t('app', 'Select a state ...')],//,'multiple'=>true
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>

                    </div>

                    <div class="col-lg-6">
                        <?= $form->field($model, 'stock_limit')->textInput() ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'country_id')->widget(Select2::classname(), [
                            'data' => $country,
                            'options' => ['placeholder' => Yii::t('app', 'Select a state ...')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'add_info')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'status')->dropDownList([
                                '1' => 'Active',
                                '0' => 'Inactive'
                        ]) ?>
                    </div>
                    <div class="col-lg-12">
                        <label><?= Yii::t('app', 'Category Id') ?></label>
                        <div id="kt_tree_1" class="tree-demo"
                             style="border: 1px solid lightgrey; border-radius: 5px; padding: 10px 20px">
                            <?= $item_category; ?>
                        </div>
                        <p class="text-danger output-text"></p>
                    </div>
                </div>
                <div class="form-group row row-cols-lg-1">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary button-click-save']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="offcanvas">
        <?= $form->field($model, 'category_id')->label(false)->hiddenInput(['id' => 'hidden-parent-input']); ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
<?php
$each = Yii::t('app', '«Category» cannot be blank.');
$js = <<<JS

    $('body').delegate('.button-click-save', 'click', function() {
        $('.field-hidden-parent-input').find('.help-block').html('');
        $('.output-text').text('$each');
        let value = $('#kt_tree_1 li');
        value.each(function(index, item) {
            if ($(item).attr('aria-selected') == 'true') {
                $('.output-text').text('');
            }
         });
        let hidden = $('#hidden-parent-input').val();
        if (hidden*1 > 0) {
            $('.output-text').text('');
        }
    });

    let hidden = $('#hidden-parent-input').val();
    if (hidden*1 > 0) {
        $('.output-text').text('');
    }
    let value = $('#kt_tree_1 li');
         value.each(function(index, item) {
            if ($(item).val() == hidden) {
                text = $(item).text();
                let color =  $(item).find('a');
                color.each(function(index, item) {
                   $(item).css({'background-color':'#0c5460','color':'white'});
                   return false;
                });
            }
     });
    
    $('body').delegate('.jstree-anchor', 'click', function() {
        $('.jstree-icon').css({'color':'#0c5460'});
        $('.jstree-anchor').css({'background-color':'white','color':'black'});
        $(this).css({'background-color':'#0c5460','color':'white'});
        $(this).find('.jstree-icon').css({'color':'white'});
        let value = $('#kt_tree_1 li');
         let category_id;
         var text;
         value.each(function(index, item) {
            if ($(item).attr('aria-selected') == 'true') {
                category_id = $(item).val();
                $('#hidden-parent-input').val(category_id);
            }
         });
    });
    
  
JS;
$this->registerJs($js);
?>