<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\structure\models\DepBankAccount */
/* @var $form yii\widgets\ActiveForm */
/* @var $regions_tree */
?>
    <div class="department-form">
        <?php $form = ActiveForm::begin(); ?>
        <input type="hidden" name="department_id" id="parent_id">

        <?= $form->field($model, 'bank_account')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'bank')->widget(\kartik\select2\Select2::classname(), [
            'data' => $model::getBankItems(),
            'options' => ['placeholder' => 'Select a state ...'],//,'multiple'=>true
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'account_type')->widget(\kartik\select2\Select2::classname(), [
            'data' => $model::getAccountTypeItems(),
            'options' => ['placeholder' => 'Select a state ...'],//,'multiple'=>true
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'status')->dropDownList([
            $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
            $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
        ]); ?>

        <div class="offcanvas-footer">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary  btn-shadow font-weight-bolder text-uppercase save-and-finish']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

<?php
$url = \yii\helpers\Url::to(['department/create-address-ajax']);
$js = <<< JS
$('body').delegate('input', 'keyup', function(e) {
    var value = $('#kt_tree_1 li');
    var id;
    value.each(function(index, item) {
        if ($(item).attr('aria-selected') == 'true') {
            id = $(item).val();
            $('#parent_id').val(id);
        }
    });
});
JS;
$this->registerJs($js)
?>