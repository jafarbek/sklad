<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\structure\models\Department */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="department-form">

    <?php $form = ActiveForm::begin(); ?>
    <input type="hidden" name="parent_id" id="parent_id">

    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $model::getCurrencyItems(),
        'options' => ['placeholder' => 'Select a state ...'],//,'multiple'=>true
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'department_type_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $model::getDepartmentTypeItems(),
        'options' => ['placeholder' => 'Select a state ...'],//,'multiple'=>true
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'okonx')->textInput(['maxlength' => true]) ?>

    <div class="offcanvas-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary  btn-shadow font-weight-bolder text-uppercase']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<< JS
$('body').delegate('input', 'keyup', function(e) {
    var value = $('#kt_tree_1 li');
    var id;
    value.each(function(index, item) {
        if ($(item).attr('aria-selected') == 'true') {
            id = $(item).val();
            $('#parent_id').val(id);
        }
    });
});
JS;
$this->registerJs($js)
?>