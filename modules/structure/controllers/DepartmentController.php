<?php

namespace app\modules\structure\controllers;

use app\controllers\BaseController;
use app\modules\manuals\models\License;
use app\modules\structure\models\DepAddress;
use app\modules\structure\models\DepBankAccount;
use app\modules\structure\models\Vat;
use Yii;
use app\modules\structure\models\Department;
use app\modules\structure\models\DepartmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Request;
use yii\web\Response;

/**
 * DepartmentController implements the CRUD actions for Department model.
 */
class DepartmentController extends BaseController
{
    /**
     * {@inheritdoc}
     */

    /**
     * Lists all Department models.
     * @return mixed
     */
    public function actionIndex($deb = null)
    {
        $tree = Department::getTreeViewHtmlForm();
        if ($deb == null){
            $id = Department::findOne(['parent_id' => null]);
        } else {
            $id = Department::findOne(['id' => $deb]);
        }
        if (!empty($id)){
            $depAddress = DepAddress::getAddressItems($id->id);
            $debBankAcount = DepBankAccount::getBankAccountItems($id->id);
            $depVat = Vat::getVatItems($id->id);
            $license = License::getLicenseItems($id->id);
        } else {
            $depAddress = [];
            $debBankAcount = [];
            $depVat = [];
            $license = [];
        }
        return $this->render('index', [
            'tree' => $tree,
            'depAddress' => $depAddress,
            'debBankAcount' => $debBankAcount,
            'depVat' => $depVat,
            'license' => $license,
            'deb' => $deb,
        ]);
    }

    /**
     * Displays a single Department model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('views/view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewAddress($id)
    {
        return $this->renderAjax('views/view-address', [
            'model' => $this->findModelAddress($id),
        ]);
    }
    /**
     * Creates a new Department model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Department();
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->parent_id = Yii::$app->request->post('parent_id');
            $model->status = 1;
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }
        $form = '_form';
        return $this->renderAjax('create', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    public function actionCreateAddress()
    {
        $model = new DepAddress();
        $form = '_form-address';
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->department_id = Yii::$app->request->post('department_id');
            DepAddress::updateAll(['status' => 0], ['department_id' => $model->department_id]);
            $model->status = 1;
            if ($model->save()) {
                return $this->redirect(['index', 'deb' => $model->department_id]);
            }
        }
        return $this->renderAjax('create', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    public function actionCreateAddressAjax(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->get('items');
        $response= [];
        $model = new DepAddress();
        array_shift($data);
        foreach ($data as $item) {
            $model[$item['name']] = $item['value'];
        }
        $update_item = DepAddress::findOne(['status' => 1]);
        $model->status = 1;
        if ($model->save()){
            $update_item->status = 0;
            $update_item->save();
            $response['cheack'] = true;
            $response['body'] = "
            <tr>
                <td>{$model->physical_location}</td>
                <td>{$model->legal_location}</td>
                <td><p class='btn btn-sm btn-success status-address'>Active</p></td>
                <td>{$model->tel}</td>
                <td>{$model->email}</td>
                <td>
                    <button href=\"/structure/department/update-address?id={$model->id}\" class=\"btn btn-icon btn-xs btn-outline-primary update-address\"><i class=\"la la-pencil-square-o\"></i></button>
                    <button href=\"/structure/department/view-address?id={$model->id}\" class=\"btn btn-icon btn-xs btn-outline-info view-address\" data-toggle=\"modal\" data-target=\"#exampleModalCustomScrollable\"><i class=\"la la-eye\"></i></button>

                </td>
                
            </tr>
            ";
            return $response;
        }
        $response['cheack'] = false;
        return $response;

    }

    public function actionCreateBankAccount()
    {
        $model = new DepBankAccount();
        $form = '_form-bank-account';
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->department_id = Yii::$app->request->post('department_id');
            if ($model->save()) {
                return $this->redirect(['index', 'deb' => $model->department_id]);
            }
        }
        return $this->renderAjax('create', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    public function actionCreateVat()
    {
        $model = new Vat();
        $form = '_form-vat';
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->department_id = Yii::$app->request->post('department_id');
            Vat::updateAll(['status' => 0], ['department_id' => $model->department_id]);
            $model->status = 1;
            if ($model->save()) {
                return $this->redirect(['index', 'deb' => $model->department_id]);
            }
        }
        return $this->renderAjax('create', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    /**
     * Updates an existing Department model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'deb' => $model->id]);
        }
        $form = '_form';
        return $this->renderAjax('update', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    public function actionUpdateAddress($id)
    {
        $model = $this->findModelAddress($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'deb' => $model->department_id]);
        }
        $form = '_form-address';
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update', [
                'model' => $model,
                'form' => $form,
            ]);
        }
        return $this->render('update', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    public function actionUpdateAddressAjax($id)
    {
        $model = $this->findModelAddress($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        $form = '_form-address';
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update', [
                'model' => $model,
                'form' => $form,
            ]);
        }
        return $this->render('update', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    public function actionUpdateBankAccount($id)
    {
        $model = $this->findModelBankAccount($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'deb' => $model->department_id]);
        }
        $form = '_form-bank-account';
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update', [
                'model' => $model,
                'form' => $form,
            ]);
        }
        return $this->render('update', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    public function actionUpdateVat($id)
    {
        $model = $this->findModelVat($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'deb' => $model->department_id]);
        }
        $form = '_form-vat';
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update', [
                'model' => $model,
                'form' => $form,
            ]);
        }
        return $this->render('update', [
            'model' => $model,
            'form' => $form,
        ]);
    }
    /**
     * Deletes an existing Department model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Department model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Department the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Department::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelAddress($id)
    {
        if (($model = DepAddress::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelBankAccount($id)
    {
        if (($model = DepBankAccount::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelVat($id)
    {
        if (($model = Vat::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        if ($this->findModel($id)->delete()){
            return true;
        }
        return false;

    }

    public function actionGetItemsAjax(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        $response = [];
        if (!empty($id)){
            $address = DepAddress::getAddressItems($id);
            $bank = DepBankAccount::getBankAccountItems($id);
            $vat = Vat::getVatItems($id);
            $license = License::getLicenseItems($id);
            $response['address'] = $address;
            $response['bank'] = $bank;
            $response['vat'] = $vat;
            $response['license'] = $license;
            return $response;
        }
        return false;
    }
}
