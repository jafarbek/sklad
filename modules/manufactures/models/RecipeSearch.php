<?php

namespace app\modules\manufactures\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\manufactures\models\Recipe;

/**
 * RecipeSearch represents the model behind the search form of `app\modules\manufactures\models\Recipe`.
 */
class RecipeSearch extends Recipe
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'item_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['wasting_percent', 'quantity'], 'number'],
            [['instruction'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recipe::find()->where(['!=' ,'status', self::STATUS_DELETED]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'item_id' => $this->item_id,
            'wasting_percent' => $this->wasting_percent,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'instruction', $this->instruction]);

        return $dataProvider;
    }
}
