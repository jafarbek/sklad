<?php

namespace app\modules\manufactures\models;

use Yii;

/**
 * This is the model class for table "production_item".
 *
 * @property int $id
 * @property int|null $production_id
 * @property int|null $recipe_ingredient_id
 * @property float|null $quantity
 * @property float|null $wasting_percent
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Production $production
 * @property RecipeIngredient $recipeIngredient
 */
class ProductionItem extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'production_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['production_id', 'recipe_ingredient_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['production_id', 'recipe_ingredient_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['quantity', 'wasting_percent'], 'number'],
            [['production_id'], 'exist', 'skipOnError' => true, 'targetClass' => Production::className(), 'targetAttribute' => ['production_id' => 'id']],
            [['recipe_ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => RecipeIngredient::className(), 'targetAttribute' => ['recipe_ingredient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'production_id' => Yii::t('app', 'Production ID'),
            'recipe_ingredient_id' => Yii::t('app', 'Recipe Ingredient ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'wasting_percent' => Yii::t('app', 'Wasting Percent'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Production]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduction()
    {
        return $this->hasOne(Production::className(), ['id' => 'production_id']);
    }

    /**
     * Gets query for [[RecipeIngredient]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecipeIngredient()
    {
        return $this->hasOne(RecipeIngredient::className(), ['id' => 'recipe_ingredient_id']);
    }
}
