<?php

namespace app\modules\manufactures\models;

use app\modules\warehouse\models\Item;
use Yii;

/**
 * This is the model class for table "recipe_ingredient".
 *
 * @property int $id
 * @property int|null $recipe_id
 * @property int|null $item_id
 * @property float|null $wasting_percent
 * @property float|null $quantity
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property ProductionItem[] $productionItems
 * @property Item $item
 * @property Recipe $recipe
 */
class RecipeIngredient extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recipe_ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['recipe_id', 'item_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['recipe_id', 'item_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['wasting_percent', 'quantity'], 'number'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['recipe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipe::className(), 'targetAttribute' => ['recipe_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'recipe_id' => Yii::t('app', 'Recipe ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'wasting_percent' => Yii::t('app', 'Wasting Percent'),
            'quantity' => Yii::t('app', 'Quantity'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[ProductionItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductionItems()
    {
        return $this->hasMany(ProductionItem::className(), ['recipe_ingredient_id' => 'id']);
    }

    /**
     * Gets query for [[Item]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * Gets query for [[Recipe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecipe()
    {
        return $this->hasOne(Recipe::className(), ['id' => 'recipe_id']);
    }
}
