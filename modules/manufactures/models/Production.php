<?php

namespace app\modules\manufactures\models;

use app\modules\admin\models\UserDepartment;
use app\modules\admin\models\Users;
use app\modules\structure\models\Department;
use app\modules\warehouse\models\DepartmentArea;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "production".
 *
 * @property int $id
 * @property int|null $recipe_id
 * @property string|null $reg_date
 * @property float|null $quantity
 * @property float|null $wasted_quantity
 * @property int|null $department_id
 * @property int|null $department_area_id
 * @property int|null $responsible
 * @property float|null $price
 * @property float|null $additional_price_percent
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Department $department
 * @property DepartmentArea $departmentArea
 * @property Recipe $recipe
 * @property Users $responsible0
 * @property ProductionItem[] $productionItems
 */
class Production extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'production';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['recipe_id', 'department_id', 'department_area_id', 'responsible', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['recipe_id', 'department_id', 'department_area_id', 'responsible', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['reg_date'], 'safe'],
            [['quantity', 'wasted_quantity', 'price', 'additional_price_percent'], 'number'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['department_area_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentArea::className(), 'targetAttribute' => ['department_area_id' => 'id']],
            [['recipe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipe::className(), 'targetAttribute' => ['recipe_id' => 'id']],
            [['responsible'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['responsible' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'recipe_id' => Yii::t('app', 'Recipe ID'),
            'reg_date' => Yii::t('app', 'Reg Date'),
            'quantity' => Yii::t('app', 'Quantity'),
            'wasted_quantity' => Yii::t('app', 'Wasted Quantity'),
            'department_id' => Yii::t('app', 'Department ID'),
            'department_area_id' => Yii::t('app', 'Department Area ID'),
            'responsible' => Yii::t('app', 'Responsible'),
            'price' => Yii::t('app', 'Price'),
            'additional_price_percent' => Yii::t('app', 'Additional Price Percent'),
            'status' => Yii::t('app', 'Finalize'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Department]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * Gets query for [[DepartmentArea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentArea()
    {
        return $this->hasOne(DepartmentArea::className(), ['id' => 'department_area_id']);
    }

    /**
     * Gets query for [[Recipe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecipe()
    {
        return $this->hasOne(Recipe::className(), ['id' => 'recipe_id']);
    }

    /**
     * Gets query for [[Responsible0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible0()
    {
        return $this->hasOne(Users::className(), ['id' => 'responsible']);
    }

    /**
     * Gets query for [[ProductionItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductionItems()
    {
        return $this->hasMany(ProductionItem::className(), ['production_id' => 'id']);
    }
    public function getRecipesList()
    {
        $lang = Yii::$app->language;
        $s = self::STATUS_SAVED;
        $sql = "SELECT recipe.id, CONCAT(i.name_{$lang} , '/', i.article) AS name,  
                        r.name_{$lang} AS unit
                FROM recipe 
                LEFT JOIN item AS i on recipe.item_id = i.id
                LEFT JOIN public.references AS r ON i.unit_id = r.id
                WHERE recipe.status={$s};";
        $query = Yii::$app->db->createCommand($sql);
        if($query){$result = $query->queryAll();}
        else{
            $result = $query->execute();
            Yii::info('' .$result );
        }
        return ArrayHelper::map($result,'id' , 'name');
    }
    public function getDepartmentListOutPuting($parent_id = null, $lvl = 0)
    {
        $id = Yii::$app->user->identity->id;
        $regions = Department::find()->where(['parent_id' => $parent_id])->all();//->andWhere(['status'=> self::STATUS_ACTIVE])
        $regions_tree = [];
        $probel = "";
        for ($i = 0; $i < $lvl; $i++){
            $probel .= "&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        $lvl++;
        foreach ($regions as $region)
        {
            $region_name = "";
            if (!empty(UserDepartment::find()->where(['user_id' => $id, 'department_id' => $region['id'], 'is_transfer' => self::IS_TRANSFER_OUTPUTING])->asArray()->all())) {
                if ($lvl !== 1) {
                    $region_name = $probel . "-" . $region['name_' . Yii::$app->language];
                } else {
                    $region_name = $probel . "<b>" . $region['name_' . Yii::$app->language] . "</b>";
                }
            }
            if ($region_name != "") {
                $regions_tree[$region['id']] = $region_name;
            }
            $regions_tree = \yii\helpers\ArrayHelper::merge($regions_tree, self::getDepartmentListOutPuting($region['id'], $lvl));
        }
        return $regions_tree;
    }

    /*
     * Responseble
     * producionni
     * Departmentga qo'yildi*/
    public static function getResponsebles($id,$type)
    {
        $sql = "SELECT u.id as id,u.fullname as name FROM users AS u
                LEFT JOIN user_department ud on u.id = ud.user_id
                WHERE ud.department_id=%d AND ud.is_transfer=%d GROUP BY u.id;";
        $sql = sprintf($sql,$id,$type);
        $query = Yii::$app->db->createCommand($sql);
        if($query){$result = $query->queryAll();}
        else{$result = $query->execute();}
        return $result;
    }
    public static function getRecipeList($id,$dep,$dep_area)
    {
        $lang = Yii::$app->language;
        $sql = "SELECT rp.id, CONCAT_WS('|',i.name_{$lang},i.article) AS item_name,
                rp.quantity , rp.wasting_percent,r.name_{$lang} AS unit_name , 
                CASE WHEN ib.price IS NULL THEN 0
                ELSE ib.price END AS price
                FROM recipe_ingredient AS rp
                LEFT JOIN item AS i on rp.item_id = i.id
                LEFT JOIN public.references AS r ON i.unit_id = r.id
                LEFT JOIN public.item_balance AS ib ON ib.item_id = rp.item_id AND  ib.department_id = %d
                AND ib.department_area_id = %d 
                WHERE rp.recipe_id = %d;";
        $sql = sprintf($sql,$dep,$dep_area,$id);
        $query = Yii::$app->db->createCommand($sql);
        if($query){$result = $query->queryAll();}
        else{$result = $query->execute();}
        return $result;
    }
    public static function getUnit($id)
    {
        $lang = Yii::$app->language;
        $sql = "SELECT re.name_%s AS name FROM recipe AS r
                LEFT JOIN item AS i on r.item_id = i.id
                LEFT JOIN public.references AS re ON i.unit_id = re.id
                WHERE r.id = %d;";
        $sql = sprintf($sql,$lang,$id);
        $query = Yii::$app->db->createCommand($sql);
        if($query){$result = $query->queryAll();}
        else{$result = $query->execute();}
        return $result;
    }

    public static function  getProductModel($id)
    {
        $lang = Yii::$app->language;
        $sql = "SELECT
            pi.id,pi.reg_date,
           pi.id AS production_id,
           CONCAT(item.name_{$lang} , '/', item.article) AS item_name,
           r_t.id as recipe_id,
           dep.name_{$lang} AS dep_name,
           dep_area.name AS dep_area_name,
           r.name_{$lang} AS unit,
           pi.quantity,
           pi.wasted_quantity,
           pi.additional_price_percent,
           pi.price,
           pi.status        
        FROM production AS pi
            LEFT JOIN recipe AS r_t ON pi.recipe_id = r_t.id
            LEFT JOIN item ON item.id = r_t.item_id
            LEFT JOIN public.references AS r ON item.unit_id = r.id
            LEFT JOIN department AS dep ON dep.id = pi.department_id
            LEFT JOIN department_area AS dep_area ON dep_area.id = pi.department_area_id
        WHERE pi.id = %d;";
        $sql = sprintf($sql,$id);
        $query = Yii::$app->db->createCommand($sql);
        if($query){$result = $query->queryAll();}
        else{$result = $query->execute();}
        return $result[0];
    }
    public static function  getProductItemModels($id)
    {
        $lang = Yii::$app->language;
        $sql = "SELECT
                   pi.id AS production_itam_id,
                   CONCAT(item.name_uz , '/', item.article) AS name,
                   r_t.id as recipe_id,
                   r.name_uz AS unit,
                   pi.quantity,
                   pi.wasting_percent
                   FROM production_item AS pi
                LEFT JOIN recipe_ingredient AS r_t ON pi.recipe_ingredient_id= r_t.id
                LEFT JOIN item ON item.id = r_t.item_id
                LEFT JOIN public.references AS r ON item.unit_id = r.id
            WHERE pi.production_id = %d;";
        $sql = sprintf($sql,$id);
        $query = Yii::$app->db->createCommand($sql);
        if($query){$result = $query->queryAll();}
        else{$result = $query->execute();}
        return $result;
    }

}
