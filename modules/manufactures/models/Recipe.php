<?php

namespace app\modules\manufactures\models;

use app\modules\warehouse\models\Item;
use Yii;

/**
 * This is the model class for table "recipe".
 *
 * @property int $id
 * @property int|null $item_id
 * @property float|null $wasting_percent
 * @property float|null $quantity
 * @property string|null $instruction
 * @property string|null $search
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property float|null $price
 *
 * @property Production[] $productions
 * @property Item $item
 * @property RecipeIngredient[] $recipeIngredients
 */
class Recipe extends \app\models\BaseModel
{
    public $search;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recipe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['item_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['wasting_percent', 'quantity', 'price'], 'number'],
            [['instruction'], 'string'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'wasting_percent' => Yii::t('app', 'Wasting Percent'),
            'quantity' => Yii::t('app', 'Quantity'),
            'instruction' => Yii::t('app', 'Instruction'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    /**
     * Gets query for [[Productions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductions()
    {
        return $this->hasMany(Production::className(), ['recipe_id' => 'id']);
    }

    /**
     * Gets query for [[Item]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * Gets query for [[RecipeIngredients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecipeIngredients()
    {
        return $this->hasMany(RecipeIngredient::className(), ['recipe_id' => 'id']);
    }
}
