<?php

namespace app\modules\manufactures\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\manufactures\models\Production;

/**
 * ProductionSearch represents the model behind the search form of `app\modules\manufactures\models\Production`.
 */
class ProductionSearch extends Production
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'recipe_id', 'department_id', 'department_area_id', 'responsible', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['reg_date'], 'safe'],
            [['quantity', 'wasted_quantity', 'price', 'additional_price_percent'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Production::find()->where(['!=','status', self::STATUS_DELETED]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'recipe_id' => $this->recipe_id,
            'reg_date' => $this->reg_date,
            'quantity' => $this->quantity,
            'wasted_quantity' => $this->wasted_quantity,
            'department_id' => $this->department_id,
            'department_area_id' => $this->department_area_id,
            'responsible' => $this->responsible,
            'price' => $this->price,
            'additional_price_percent' => $this->additional_price_percent,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
