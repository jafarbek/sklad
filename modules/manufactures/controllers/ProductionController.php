<?php

namespace app\modules\manufactures\controllers;

use app\modules\manufactures\models\ProductionItem;
use app\modules\manufactures\models\Recipe;
use app\modules\manufactures\models\RecipeIngredient;
use app\modules\warehouse\models\ItemBalance;
use Yii;
use app\modules\manufactures\models\Production;
use app\modules\manufactures\models\ProductionSearch;
use app\controllers\BaseController;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ProductionController implements the CRUD actions for Production model.
 */
class ProductionController extends BaseController
{

    /**
     * Lists all Production models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Production model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = Production::getProductModel($id);
        $models = Production::getProductItemModels($id);
        return $this->render('view', [
            'model' => $model,
            'models' => $models
        ]);
    }

    /**
     * Creates a new Production model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Production();
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $model->status = $model::STATUS_ACTIVE;
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                if ($model->load($data) && $model->save()) {
                    $producId = $model->id;
                    $saved = true;
                    if (!empty($data['ProductionItem'])) {
                        $saved = false;
                        foreach ($data['ProductionItem'] as $product) {
                            $modelProItem = new ProductionItem();
                            $modelProItem->setAttributes([
                                'production_id' => $producId,
                                'recipe_ingredient_id' => $product['recipe_production_id'],
                                'quantity' => $product['quantity'],
                                'wasting_percent' => $product['wasting_percent'],
                                'status' => $model::STATUS_ACTIVE
                            ]);
                            if ($modelProItem->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', "Saved SuccessFully");
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                }
            } catch (Exception $e) {
                Yii::info('Error Document ' . $e->getMessage(), 'save');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Production model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $lang = Yii::$app->language;
        $model = $this->findModel($id);
        $responseble = Production::getResponsebles($model->department_id,0);
        $responseble = ArrayHelper::map($responseble,'id','name');
        $sql = "SELECT
                   pi.id AS production_id,
                   ri.id as item_id,
                   CONCAT(item.name_{$lang} , '/', item.article) AS name,
                   r.name_{$lang} AS unit,
                   pi.quantity,
                   pi.wasting_percent,
                   CASE WHEN ib.price IS NULL THEN 0
                   ELSE ib.price END AS price
                FROM production_item AS pi
                LEFT JOIN recipe_ingredient as ri ON pi.recipe_ingredient_id = ri.id
                LEFT JOIN item ON item.id = ri.item_id
                LEFT JOIN public.references AS r ON item.unit_id = r.id
                LEFT JOIN public.item_balance AS ib ON ib.item_id = ri.item_id 
                AND  ib.department_id = {$model->department_id}
                AND ib.department_area_id = {$model->department_area_id}
                WHERE pi.production_id = {$id}/*
                AND ib.id in (SELECT MAX(ib2.id) as id FROM item_balance as ib2 
                GROUP BY ib2.item_id ,
                ib2.price,ib2.department_area_id);*/";
        $models = Yii::$app->db->createCommand($sql)->queryAll();
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $model->status = $model::STATUS_ACTIVE;
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                if ($model->load($data) && $model->save()) {
                    $producId = $model->id;
                    $saved = true;
                    if (!empty($data['ProductionItem'])) {
                        $saved = false;
                        ProductionItem::deleteAll(['production_id' => $id]);
                        foreach ($data['ProductionItem'] as $product) {
                            $modelProItem = new ProductionItem();
                            $modelProItem->setAttributes([
                                'production_id' => $producId,
                                'recipe_ingredient_id' => $product['recipe_production_id'],
                                'quantity' => $product['quantity'],
                                'wasting_percent' => $product['wasting_percent'],
                                'status' => $model::STATUS_ACTIVE
                            ]);
                            if ($modelProItem->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', "Saved SuccessFully");
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                }
            } catch (Exception $e) {
                Yii::info('Error Document ' . $e->getMessage(), 'save');
            }

        }
        return $this->render('update', [
            'model' => $model,
            'models' => $models,
            'responseble' => $responseble,
        ]);
    }


    /**
     * Deletes an existing Production model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (!empty($model))
        {
            $model->status = $model::STATUS_DELETED;
            $sql = "UPDATE production_item
                    SET status = %d WHERE production_id = %d;";
            $sql = sprintf($sql,$model::STATUS_DELETED,$model->id);
            $result = Yii::$app->db->createCommand($sql)->queryAll();
            if ($model->save() && $result) {
                return $this->redirect(['index']);
            }else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
    }

    public function actionSaveAndFinish($id){

        $production = Production::findOne($id);
        if (!empty($production)) {
            $production_item = ProductionItem::find()->where(['production_id' => $production['id']])->asArray()->all();
            $dep = $production->department_id;
            $dep_area = $production->department_area_id;
            $reg_date=$production->reg_date;
            $recipe_id = Recipe::findOne($production['recipe_id'])->item_id;
            $productionId = $id;$summ=0;
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                if (!empty($production_item)) {
                    $saved = true;
                    foreach ($production_item as $item) {
                        $saved = false;
                        $item_id = RecipeIngredient::findOne($item['recipe_ingredient_id'])->item_id;
                        $sql = "SELECT ib.* FROM item_balance AS ib
                                    INNER JOIN (SELECT MAX(id) AS id FROM item_balance
                                    GROUP BY item_id, price,department_area_id,department_id
                                    ORDER BY id DESC) AS ib2 ON ib2.id = ib.id
                                    LEFT JOIN (SELECT MIN(ib.id) AS id, sum(inventory) AS summa FROM item_balance AS ib
                                        INNER JOIN (SELECT MAX(id) AS id FROM item_balance
                                        GROUP BY item_id, price,department_area_id,department_id
                                        ORDER BY id DESC) AS ib2 ON ib2.id = ib.id
                                        GROUP BY item_id, department_area_id,department_id
                                    ) AS ib3 ON ib3.id = ib.id
                                WHERE (ib.item_id=%d) AND (ib.department_id=%d) AND (ib.department_area_id=%d) AND ib.inventory > 0 ORDER BY ib.id ASC ;";
                        $sql = sprintf($sql,$item_id,$dep,$dep_area);
                        $balance = Yii::$app->db->createCommand($sql)->queryAll();
                        if (!empty($balance))
                        {   $quantity=$item['quantity'];
                            foreach ($balance as $bal)
                            {
                                if(($bal['inventory'] > 0 && $quantity > 0) && ($quantity >= $bal['inventory'])){
                                    $item_balance = new ItemBalance();
                                    $quantity1=$quantity-$bal['inventory'];
                                    $summ += !empty($bal['price']) ? ($quantity-$quantity1)*$bal['price'] : ($quantity-$quantity1)*1;
                                    $item_balance->setAttributes([
                                       'item_id' => $bal['item_id'],
                                        'lot' => $bal['lot'],
                                        'quantity' => ($quantity-$quantity1)*(-1),
                                        'inventory' =>0,
                                        'reg_date' => $reg_date,
                                        'department_id' => $dep,
                                        'department_area_id' => $dep_area,
                                        'document_id' => $bal['document_id'],
                                        'document_item_id' => $bal['document_item_id'],
                                        'price' => $bal['price'],
                                        'price_currency' => $bal['price_currency'],
                                        'status' => $item_balance::STATUS_ACTIVE,
                                        'sell_price' => $bal['sell_price'],
                                        'sell_currency' => $bal['sell_currency'],
                                        'production_id' => $bal['production_id'],
                                    ]);
                                    $quantity = $quantity1;
                                    if ($item_balance->save())
                                    {
                                        $saved = true;
                                    }else{
                                        $saved = false;
                                        break;
                                    }
                                } elseif (($bal['inventory'] > 0 && $quantity > 0) && ($quantity <= $bal['inventory'])){
                                    $item_balance = new ItemBalance();
                                    $quantity1 = $bal['inventory'] - $quantity;
                                    $summ += $quantity*$bal['price'];
                                    $item_balance->setAttributes([
                                        'item_id' => $bal['item_id'],
                                        'lot' => $bal['lot'],
                                        'quantity' => $quantity*(-1),
                                        'inventory' => $quantity1,
                                        'reg_date' => $reg_date,
                                        'department_id' => $dep,
                                        'department_area_id' => $dep_area,
                                        'document_id' => $bal['document_id'],
                                        'document_item_id' => $bal['document_item_id'],
                                        'price' => $bal['price'],
                                        'price_currency' => $bal['price_currency'],
                                        'status' => $item_balance::STATUS_ACTIVE,
                                        'sell_price' => $bal['sell_price'],
                                        'sell_currency' => $bal['sell_currency'],
                                        'production_id' => $bal['production_id'],
                                    ]);
                                    if ($item_balance->save())
                                    {
                                        $saved = true;
                                    }else{
                                        $saved = false;
                                        break;
                                    }
                                }

                            }

                        }
                    }
                    if($saved)
                    {
                        $saved = false;
                        $sql1 = "SELECT ib.* FROM item_balance AS ib
                                    INNER JOIN (SELECT MAX(id) AS id FROM item_balance
                                    GROUP BY item_id, price,department_area_id,department_id
                                    ORDER BY id DESC) AS ib2 ON ib2.id = ib.id
                                    LEFT JOIN (SELECT MIN(ib.id) AS id, sum(inventory) AS summa FROM item_balance AS ib
                                        INNER JOIN (SELECT MAX(id) AS id FROM item_balance
                                        GROUP BY item_id, price,department_area_id,department_id
                                        ORDER BY id DESC) AS ib2 ON ib2.id = ib.id
                                        GROUP BY item_id, department_area_id,department_id
                                    ) AS ib3 ON ib3.id = ib.id
                                WHERE (ib.item_id=%d) AND (ib.department_id=%d) AND (ib.department_area_id=%d) AND (ib.production_id=%d) ORDER BY ib.id DESC ;";
                        $sql1 = sprintf($sql1,$recipe_id,$dep,$dep_area,$productionId);
                        $proItem = Yii::$app->db->createCommand($sql1)->queryAll();
                        if (!empty($proItem) && ($proItem[0]['price'] == $summ/($production->quantity-$production->wasted_quantity)))
                        {
                            $model = new ItemBalance();
                            $model->setAttributes([
                                'item_id' => $recipe_id,
                                'quantity' => $production->quantity-$production->wasted_quantity,
                                'inventory' => $proItem[0]['inventory']+($production->quantity-$production->wasted_quantity),
                                'reg_date' => $reg_date,
                                'department_id' => $dep,
                                'department_area_id' => $dep_area,
                                'price' => $proItem[0]['price'],
                                'price_currency' => null,
                                'status' => $model::STATUS_ACTIVE,
                                'sell_price' => null,
                                'sell_currency' => null,
                            ]);
                            $model->production_id = $id;
                            $production['status'] = $model::STATUS_SAVED;
                            if ($model->save() && $production->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                            }
                        }else {
                            $model = new ItemBalance();
                            $model->setAttributes([
                                'item_id' => $recipe_id,
                                'quantity' => $production->quantity-$production->wasted_quantity,
                                'inventory' => $production->quantity - $production->wasted_quantity,
                                'reg_date' => $reg_date,
                                'department_id' => $dep,
                                'department_area_id' => $dep_area,
                                'price' => $summ/($production->quantity-$production->wasted_quantity),
                                'price_currency' => null,
                                'status' => $model::STATUS_ACTIVE,
                                'sell_price' => null,
                                'sell_currency' => null,
                            ]);
                            $model->production_id = $id;
                            $production['status'] = $model::STATUS_SAVED;
                            if ($model->save() && $production->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                            }
                        }

                    }
                    if ($saved) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app',"Saved SuccessFully"));
                        return $this->redirect(['index']);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', Yii::t('app','The product is not enough'));
                        return $this->redirect(['view', 'id' => $id]);
                    }

                }
            }catch (Exception $e){
                Yii::info('Error Production' . $e->getMessage(), 'save');
                echo "<pre>";
                    print_r($e->getMessage());
                echo "</pre>";exit();
            }

        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Production model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Production the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Production::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionResponsibleAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        $type = Yii::$app->request->get('type');
        $response = [];
        $response['status'] = false;
        $response['message'] = "Bunday malumot yoq";
        if (isset($id) && isset($type)) {
            $responseble = Production::getResponsebles($id, $type);
            $response['status'] = true;
            $response['message'] = "";

            $response['responsible'] = $responseble;
        }
        return $response;
    }

    public function actionRecipeAjax($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        $dep = Yii::$app->request->get('dep');
        $dep_area = Yii::$app->request->get('dep_area');
        $type = Yii::$app->request->get('type');
        $response = [];
        $response['status'] = false;
        $response['message'] = "Bunday malumot yoq";
        if (isset($id)) {
            $responseble = Production::getRecipeList($id, $dep, $dep_area);
            $unit = Production::getUnit($id);
            $response['status'] = true;
            $response['message'] = "";

            $response['responsible'] = $responseble;
            $response['unit'] = $unit;
        }
        return $response;
    }
}
