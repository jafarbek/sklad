<?php

namespace app\modules\manufactures\controllers;

use app\controllers\BaseController;
use yii\web\Controller;

/**
 * Default controller for the `manufactures` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
