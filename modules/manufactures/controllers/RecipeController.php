<?php

namespace app\modules\manufactures\controllers;

use app\modules\manufactures\models\RecipeIngredient;
use Yii;
use app\modules\manufactures\models\Recipe;
use app\modules\manufactures\models\RecipeSearch;
use app\controllers\BaseController;
use yii\db\Exception;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecipeController implements the CRUD actions for Recipe model.
 */
class RecipeController extends BaseController
{

    /**
     * Lists all Recipe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecipeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recipe model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (!empty($model))
        {
            $lang = Yii::$app->language;
            $sql = "SELECT 
                    r_i.item_id,
                    CONCAT(item.name_%s , '/', item.article) AS name,
                    r_i.wasting_percent,
                    r_i.quantity,
                    r.name_%s AS unit
                FROM public.recipe_ingredient AS r_i 
                LEFT JOIN public.item ON item.id = r_i.item_id
                LEFT JOIN public.references AS r ON r.id = item.unit_id
                WHERE r_i.recipe_id = %d;";
            $sql = sprintf($sql,$lang,$lang,$id);
            $models = Yii::$app->db->createCommand($sql)->queryAll();

        }
        return $this->render('view', [
            'model' => $model,
            'models' => $models,
        ]);
    }

    /**
     * Creates a new Recipe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recipe();

        if (Yii::$app->request->isPost)
        {
            $data = Yii::$app->request->post();
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $model->status = $model::STATUS_ACTIVE;
                if ($model->load($data) && $model->save())
                {
                    $docId = $model->id;
                    $saved = true;
                    if (!empty($data['DocumentItem']))
                    {
                        $saved = false;
                        foreach ($data['DocumentItem'] as $docItem)
                        {
                            $modelDocItem = new RecipeIngredient();
                            $modelDocItem->recipe_id = $docId;
                            $modelDocItem->item_id = $docItem['item_id'];
                            $modelDocItem->wasting_percent = $docItem['wasting_percent'];
                            $modelDocItem->quantity = $docItem['quantity'] ?? null;
                            $modelDocItem->status = $model::STATUS_ACTIVE;

                            if ($modelDocItem->save())
                            {
                                $saved = true;
                            }else{
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if($saved)
                    {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success','Saved successfully');
                        return $this->redirect(['view','id' =>$model->id]);
                    }
                    else
                    {
                        Yii::$app->session->setFlash('danger','Information not saved, please cheack it!');
                        $transaction->rollBack();
                    }
                }
            }
            catch(Exception $e){
                Yii::info('Error Document '.$e->getMessage(),'save');
            }
        }
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Recipe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->status == $model::STATUS_DELETED || $model->status == $model::STATUS_SAVED){
            return $this->redirect(['index']);
        }
        if (!empty($model))
        {
            $lang = Yii::$app->language;
            $sql = "SELECT 
                    r_i.item_id,
                    CONCAT(item.name_%s , '/', item.article) AS name,
                    r_i.wasting_percent,
                    r_i.quantity,
                    r.name_%s AS unit
                FROM public.recipe_ingredient AS r_i 
                LEFT JOIN public.item ON item.id = r_i.item_id
                LEFT JOIN public.references AS r ON r.id = item.unit_id
                WHERE r_i.recipe_id = %d;";
            $sql = sprintf($sql,$lang,$lang,$id);
            $models = Yii::$app->db->createCommand($sql)->queryAll();

        }

        if (Yii::$app->request->isPost)
        {
            $data = Yii::$app->request->post();
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $model->status = $model::STATUS_ACTIVE;
                if ($model->load($data) && $model->save())
                {
                    $docId = $model->id;
                    $saved = true;
                    if (!empty($data['DocumentItem']))
                    {
                        $saved = false;

                        RecipeIngredient::deleteAll(['recipe_id' => $id]);
                        foreach ($data['DocumentItem'] as $docItem)
                        {
                            $modelDocItem = new RecipeIngredient();
                            $modelDocItem->recipe_id = $docId;
                            $modelDocItem->item_id = $docItem['item_id'];
                            $modelDocItem->wasting_percent = $docItem['wasting_percent'];
                            $modelDocItem->quantity = $docItem['quantity'] ?? null;
                            $modelDocItem->status = $model::STATUS_ACTIVE;

                            if ($modelDocItem->save())
                            {
                                $saved = true;
                            }else{
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if($saved)
                    {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success','Saved successfully');
                        return $this->redirect(['view','id' =>$model->id]);
                    }
                    else
                    {
                        Yii::$app->session->setFlash('danger','Information not saved, please cheack it!');
                        $transaction->rollBack();
                    }
                }
            }
            catch(Exception $e){
                Yii::info('Error Document '.$e->getMessage(),'save');
            }
        }

        return $this->render('update', [
            'model' => $model,
            'models' => $models,
        ]);
    }

    /**
     * Deletes an existing Recipe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->status == $model::STATUS_DELETED || $model->status == $model::STATUS_SAVED){
            return $this->redirect(['index','slug' => $this->slug]);
        }
        $model->status = $model::STATUS_DELETED;
        if ($model->save())
        {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Recipe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recipe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recipe::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    public function actionItemAjax($q = null) {
        $out = ['results' => ['id' => '', 'text' => '']];
        if ($q) {
            $lang = Yii::$app->language;
            $sql = "SELECT 
                        item.id,
                        CONCAT(item.name_".$lang." , '/', item.article) AS name,  
                        r.name_".$lang." AS unit
                    FROM item
                    LEFT JOIN public.references AS r ON r.id = item.unit_id
                    where lower(item.name_en) like lower('%{$q}%') 
                         or lower(item.name_ru) like lower('%{$q}%') 
                        or lower(item.name_uz) like lower('%{$q}%') 
                        or lower(item.short_name) like lower('%{$q}%') 
                        or lower(item.article) like lower('%{$q}%')";
            $data = Yii::$app->db->createCommand($sql)->queryAll();
            $out['results'] = array_values($data);
        }
        return json_encode($out);
    }

    public function actionSaveAndFinish($id)
    {
        $model = $this->findModel($id);
        if ($model->status == $model::STATUS_DELETED || $model->status == $model::STATUS_SAVED){
            return $this->redirect(['index','slug' => $this->slug]);
        }
        $model->status = $model::STATUS_SAVED;
        if ($model->save())
        {
            return $this->redirect(['index']);
        }
    }
}
