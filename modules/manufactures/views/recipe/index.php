<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\manufactures\models\RecipeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Recipes');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .twoInOne{
        font-size: 10px;
        border-top: grey 1px solid;
        display: block;
    }
    .new-row {
        display: block;
    }
</style>
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
        </div>
        <div class="card-toolbar">
            <a href="<?= Url::to(['recipe/create']) ?>" class="btn btn-sm btn-primary">
                        <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                            <!--end::Svg Icon-->
                    </span><?=Yii::t('app', 'Create')?>
            </a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <?php if (Yii::$app->session->hasFlash('danger')): ?>
            <h6 class="alert alert-danger"><?=Yii::$app->session->getFlash('danger'); ?></h6>
        <?php elseif (Yii::$app->session->hasFlash('success')): ?>
            <h6 class="alert alert-success"><?=Yii::$app->session->getFlash('success'); ?></h6>
        <?php endif; ?>

        <?php Pjax::begin(); ?>
        <? $form = \yii\bootstrap\ActiveForm::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary' => '',
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'options' => ['style' => 'width:40px'],
                ],
                [
                    'attribute' => 'item_id',
                    'value' => function($model){
                        $item = \app\modules\warehouse\models\Item::findOne(['id' => $model->item_id]);
                        return $item['name_'.Yii::$app->language].'/'.$item->article;
                    }
                ],
                [
                    'attribute' => 'wasting_percent',
                    'value' => function($model){
                        return $model->wasting_percent.' %';
                    }
                ],
                [
                    'attribute' => 'quantity',
                    'value' => function($model){
                        $unit = \app\modules\manuals\models\References::findOne(['id' => \app\modules\warehouse\models\Item::findOne(['id' => $model->item_id])->unit_id]);
                        return $model->quantity.' '.$unit['name_'.Yii::$app->language];
                    }
                ],
                [
                    'attribute' => 'created_by',
                    'format' => 'raw',
                    'value' => function($model){
                        $user = \app\modules\admin\models\Users::findOne(['id' => $model->created_by]);
                        return "<span class='new-row'>{$user->fullname}</span><span class='twoInOne'>".date('Y-m-d H:i:s', $model->created_at)."</span>";
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => Yii::t('app', "Action"),
                    'template' => '{view} {update} {delete}',
                    'options' => ['style' => 'width:100px;'],
                    'buttons' => [
                        'view' => function($url, $model){
                            return '<a href="'.Url::to(['recipe/view', 'id' => $model->id]).'" class="btn btn-xs btn-outline-info" ><i class="la la-eye ml-1"></i></a>';
                        },
                        'delete' => function($url, $model){
                            return '<a href="'.Url::to(['recipe/delete', 'id' => $model->id]).'" class="btn btn-xs btn-outline-danger" ><i class="la la-trash ml-1"></i></a>';
                        },

                        'update' => function($url, $model){
                            return '<a href="'.Url::to(['recipe/update', 'id' => $model->id]).'" class="btn btn-xs btn-outline-primary"><i class="la la-pencil ml-1"></i></a>';
                        }
                    ],
                    'visibleButtons' => [
                        'update' => function($model) {
                            return ($model->status == 1 || $model->status == 2);
                        },
                        'delete' => function($model) {
                            return ($model->status == 1 || $model->status == 2);
                        },
                        'view' => function($model) {
                            return true;
                        },
                    ],
                ],
            ],
        ]); ?>
        <input type="submit" class="offcanvas click-button-ajax">
        <? \yii\bootstrap\ActiveForm::end(); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
<button id="kt_quick_panel_toggle" class="offcanvas">


