<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\manufactures\models\Recipe */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recipes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

    <div class="tovar-form">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header mt-5">
                <div class="col-xs-8">
                    <?php if ($model->status == '1'): ?>
                        <div class="col-sm-12 mb-4 mt-4">
                            <a class=" btn btn-sm btn-success" href="<?=\yii\helpers\Url::to(['recipe/save-and-finish', 'id' => $model->id])?>"><?=Yii::t('app', 'Save And Finish'); ?></a>
                            <a class="ml-2 btn btn-sm btn-outline-primary" href="<?=\yii\helpers\Url::to(['recipe/update', 'id' => $model->id])?>"><?=Yii::t('app', 'Update'); ?></a>
                            <?php
                            echo Html::a(Yii::t('app', 'Delete'), \yii\helpers\Url::to(['delete', 'id' => $model->id]), [
                                'class' => "ml-2 btn btn-sm btn-outline-danger",
                                'data' => [
                                    'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                                    'method' => 'POST',
                                ],
                            ]);
                            ?>
                        </div>
                    <?php endif; ?>
                 </div>
                <div class="col-xs-4 text-right">
                    <a href="<?= \yii\helpers\Url::to(['recipe/index']) ?>"
                       class="btn btn-sm btn-outline-primary font-weight-bolder">
                        <i class="la la-backspace"></i><?= Yii::t('app', 'Назад'); ?>
                    </a>
                </div>

            </div>
            <div class="card-body">
                <table class="document-table">
                    <thead >
                    <tr>
                        <?php
                            $item = \app\modules\warehouse\models\Item::findOne(['id' => $model->item_id]);
                        ?>
                        <th><b><?=Yii::t('app','Recipe'); ?> :</b> <span><?=$item['name_'.Yii::$app->language].'/'.$item->article; ?></span></th>
                        <th><b><?=Yii::t('app', 'Wasting percent')?> :</b> <span><?=$model->wasting_percent; ?> %</span></th>
                        <th><b><?=Yii::t('app', 'Quantity')?> :</b> <span><?=number_format($model->quantity, 2).' '.\app\modules\manuals\models\References::findOne(['id' => $item->unit_id])->token; ?></span></th>
                    </tr>
                    </thead>
                </table>
                <table class="item-table">
                    <thead>
                    <tr class="header-table-tabular">
                        <th style="width: 30px" class="text-center">#</th>
                        <th class=""><?= Yii::t('app', 'Ingredient') ?></th>
                        <th class=""><?= Yii::t('app', 'Wastage Percent') ?></th>
                        <th class=""><?= Yii::t('app', 'Final Quantity') ?></th>
                    </tr>
                    </thead>
                    <tbody  class="item-tbody">
                    <?php $i = 1; if (!empty($models)) : ?>
                        <?php foreach($models as $key): ?>
                            <tr class="item-tbody-tr">
                                <td class="text-center"><?=$i?></td>
                                <td><?=$key['name']?></td>
                                <td><?=$key['wasting_percent']?> %</td>
                                <td><?=$key['quantity']?> <?=$key['unit']?></td>
                            </tr>
                            <?php $i++; endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
<?php
$css = <<< CSS
    .item-table {
        margin-top: 20px;
        font-family: Arial;
    }
    .item-table th {
        border: 1px solid gray;
    }
    .document-table th {
        border: 1px solid slategrey;
        padding-left: 10px;
    }
    .item-tr th {
        padding-left: 5px;
        background-color: #0c5460;
        color: whitesmoke;
    }
    .item-tbody-tr td {
        border: 1px solid black;
        padding-left: 5px;
    }
    .footer-out td {
        border: 1px solid slategrey;
        padding-right: 5px;
        color: #0a73bb;
    }
    th span {
        color: gray;
        margin-left: 8px;
    }
    tr {
      height: 30px;
    }
    table {
      width: 100%;
    }
    hr {
        background-color: #0c5460;
        height: 2px;
    }
CSS;
$this->registerCss($css);