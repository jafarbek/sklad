<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\manufactures\models\Recipe */

$this->title = Yii::t('app', 'Create Recipe');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recipes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

