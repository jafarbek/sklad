<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\select2\Select2;
use \yii\helpers\Url;
use \yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\modules\manufactures\models\Recipe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recipe-form">

    <?php $form = ActiveForm::begin([
        'class' => 'form'
    ]); ?>

    <div class="card card-custom gutter-b example example-compact">
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-4">
                    <?php $url = Url::to(['item-ajax']); ?>
                    <?= $form->field($model, 'item_id')->widget(Select2::classname(), [
                        'options' => ['placeholder' => 'Search for a city ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) {
                                    return {
                                        q: params.term,
                                    }; 
                                 },
                                 ')

                            ],
                            'templateResult' => new JsExpression('function(city) {return city.name; }'),
                            'templateSelection' => new JsExpression('function (city) {$(".quantity-name").html(city.unit); return city.name;  }'),
                        ],

                        'pluginEvents' => [
                            "select2:select" => "
                                    function() { 
                                        $('#js-search-language').prop(\"disabled\", false); 
                                        $('.quantity-recipe').prop(\"disabled\", false); 
                                        $('.wasting-recipe').prop(\"disabled\", false); 
                                    }"],
                    ]); ?>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label><?=Yii::t('app', 'Quantity')?></label>
                        <div class="input-group input-group-sm">
                            <input disabled type="text" name="Recipe[quantity]" value="<?=$model->quantity ?? ''?>" class="quantity-recipe form-control" aria-describedby="basic-addon2" />
                            <div class="input-group-append">
                                <span class="input-group-text quantity-name  text-dark">
                                    none
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label><?=Yii::t('app', 'Wasting Percent')?></label>
                        <div class="input-group input-group-sm">
                            <input disabled type="text" name="Recipe[wasting_percent]" value="<?=$model->wasting_percent ?? ''?>" class="wasting-recipe form-control" aria-describedby="basic-addon2" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-percent font-size-h6 text-dark"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="row justify-content-center">
                <div class="col-sm-12">
                    <div class="flex-wrap border-0 pt-6 pb-0 mt-7">
                        <div class="col-sm-8 offset-2">
                            <?= $form->field($model, 'search')->widget(Select2::classname(), [
                                'options' => ['placeholder' => 'Search for  ...', 'id'=>'js-search-language'],
                                'pluginOptions' => [
                                    'disabled' => 'true',
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) {
                                                return {
                                                    q: params.term
                                                }; 
                                             },
                                             ')
                                    ],
                                    'templateResult' => new JsExpression('function(city) {return city.name; }'),
                                    'templateSelection' => new JsExpression('function (city) {name_item = city}'),
                                ],
                                'pluginEvents' => [
                                    "select2:select" => "
                                    function() { 
                                        var id = $(this).val();
                                        $(this).val('');
                                        let nextNum = $('.body-table-tabular').data('number-tbody');
                                        let num = (typeof nextNum !== 'undefined') ? 1*nextNum+1 : 0;
                                        vat  = name_item.doc_item_price/name_item.income_price*100;
                                        if (vat == '0') {
                                            name_item.price = 0;
                                        }
                                        console.log(name_item);
                                        var bool_cheack = 0;
                                        $('.item').map(function(index, item){
                                            if ($(this).val() == name_item.id) {
                                                bool_cheack = 1;
                                                return false;
                                            }
                                        });
                                           var tbody_tr_ichi = `
                                                <tr class=\"multiple-input-list__item\">
                                                    <td class='text-center number-count'>`+num+`</td>
                                                    <td><input type=\"hidden\" name=\"DocumentItem[`+num+`][item_id]\" class=\"item form-control form-control-sm\" data-number=\"`+num+`\" value=\"`+name_item.id+`\"><span>`+ name_item.name +`</span></td>
                                                    <td>
                                                        <div class=\"form-group\">
                                                            <div class=\"input-group input-group-sm\">
                                                                <input type=\"text\" name=\"DocumentItem[`+num+`][wasting_percent]\"  class=\"form-control\" value=\"0\" aria-describedby=\"basic-addon2\" />
                                                                <div class=\"input-group-append\">
                                                                    <span class=\"input-group-text\">
                                                                        <i class=\"la la-percent\"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
													</td>
                                                    <td>
                                                        <div class=\"form-group\">
                                                            <div class=\"input-group input-group-sm\">
                                                                <input type=\"text\" name=\"DocumentItem[`+num+`][quantity]\" class=\"form-control\" value=\"1\" aria-describedby=\"basic-addon2\" />
                                                                <div class=\"input-group-append\">
                                                                    <span class=\"input-group-text\">`+ name_item.unit +`</span>
                                                                </div>
                                                            </div>
                                                        </div>
    												</td>
                                                    <td>
                                                        <button class=\"btn btn-sm btn-outline-danger minus-button\">
                                                            <i class=\"la la-trash ml-1\"></i>
                                                        </button>
                                                    </td>
                                                </tr>    
                                            `;
                                        if (jQuery('#documentitem-'+num+'-to_dep_area').data('select2')) { 
                                        jQuery('#documentitem-'+num+'-to_dep_area').select2('destroy'); }
                                        jQuery.when(jQuery('#documentitem-'+num+'-to_dep_area').select2()).done(initS2Loading('documentitem-'+num+'-to_dep_area','s2options_d6851687'));
                                        if (bool_cheack == 0) {
                                            $('.body-table-tabular').data('number-tbody',num);
                                            $('.body-table-tabular').append(tbody_tr_ichi);
                                        }
                                        $('.multiple-input-list__item').blur();
                                        
                                    }",
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <br><br>
                    <div class="card-toolbar">
                        <!--begin::Dropdown-->
                        <table class="table table-bordered table-hover table-incoming">
                            <thead style="background-color: #0d3349; color: white;">
                            <tr class="header-table-tabular">
                                <th style="width: 30px" class="text-center">#</th>
                                <th class="text-center"><?= Yii::t('app', 'Ingredient') ?></th>
                                <th class="text-center"><?= Yii::t('app', 'Wastage Percent') ?></th>
                                <th class="text-center"><?= Yii::t('app', 'Final Quantity') ?></th>
                                <th class="text-center"></th>
                            </tr>
                            </thead>
                            <tbody class="body-table-tabular" data-number-tbody="<?= !empty($models)? count($models) : 0;?>">
                            <?php $i = 1; if (!empty($models)) : ?>
                                <?php $this->registerJs("$(function(){
                                        $('#js-search-language').prop('disabled', false); 
                                        $('.quantity-recipe').prop('disabled', false); 
                                        $('.wasting-recipe').prop('disabled', false); 
                                        });")?>
                            
                                <?php foreach($models as $key): ?>
                                <tr class="multiple-input-list__item">
                                    <td class="text-center number-count"><?=$i?></td>
                                    <td><input type="hidden" name="DocumentItem[<?=$i;?>][item_id]" class="item form-control form-control-sm" data-number="<?=$i;?>" value="<?=$key['item_id']?>"><span><?=$key['name']?></span></td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group input-group-sm">
                                                <input type="text" name="DocumentItem[<?=$i;?>][wasting_percent]"  class="form-control" value="<?=$key['wasting_percent']?>" aria-describedby="basic-addon2" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-percent"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group input-group-sm">
                                                <input type="text" name="DocumentItem[<?=$i;?>][quantity]" class="form-control" value="<?=$key['quantity']?>" aria-describedby="basic-addon2" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><?=$key['unit']?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-outline-danger minus-button">
                                        <i class="la la-trash ml-1"></i>
                                        </button>
                                    </td>
                                </tr>
                                <?php $i++; endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>

                    </div>
                    <!--end::Card-->
                </div>
            </div>
            <br><br>
            <div class="separator separator-solid"></div>
            <br><br>

            <div class="form-group row">
                <div class="gutter-b example example-compact col-12">
                    <label for="">Recipe Instructions</label>
                    <div id="kt-ckeditor-1-toolbar"></div>
                    <div id="kt-ckeditor-1">
                        <?php if (!empty($model->instruction)): ?>
                            <?=$model->instruction?>
                        <?php else :?>
                            <h1>text ...</h1>
                        <?php endif; ?>
                    </div>
                </div>
                <?= $form->field($model, 'instruction')->textarea(['rows' => 6, 'col' => 60, 'id' => 'instruction', 'class' => 'offcanvas'])->label(false) ?>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-outline-primary']) ?>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
<?php
$js = <<< JS
$('body').delegate('#kt-ckeditor-1', 'keyup', function(e) {
    $('#instruction').val($(this).html());
})
$('body').delegate('#kt-ckeditor-1-toolbar', 'click', function(e) {
    $('#instruction').val($('#kt-ckeditor-1').html());
})
$('body').delegate('.minus-button', 'click', function(e) {
    e.preventDefault();
    let tr = $(this).parents('tr').remove();
});
JS;
$this->registerJs($js);


$this->registerJsFile('@web/asset/plugins/custom/ckeditor/ckeditor-document.bundle.js?v=7.0.3', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/asset/js/pages/crud/forms/editors/ckeditor-document.js?v=7.0.3', ['depends' => [\yii\web\JqueryAsset::className()]]);


?>