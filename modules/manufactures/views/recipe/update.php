<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\manufactures\models\Recipe */
/* @var $models app\modules\manufactures\models\RecipeIngredient */

$this->title = Yii::t('app', 'Update Recipe: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recipes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>


    <?= $this->render('_form', [
        'model' => $model,
        'models' => $models,
    ]) ?>

