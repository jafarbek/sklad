<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\manufactures\models\Production */
/* @var $models app\modules\manufactures\models\ProductionItem */
/* @var $responseble */

$this->title = Yii::t('app', 'Update Production: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Productions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
    <?= $this->render('_form', [
        'model' => $model,
        'models' => $models,
        'responseble' => $responseble,
    ]) ?>
<?php
$js = <<<JS
$('#department_area_id').prop("disabled", false); 
$('#recipe').prop("disabled", false); 
JS;
$this->registerJs($js);