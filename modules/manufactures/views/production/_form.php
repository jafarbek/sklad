<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\manufactures\models\Production */
/* @var $form yii\widgets\ActiveForm */
/* @var $summa */
/* @var $dep_area */
$dep_area = \app\modules\warehouse\models\DepartmentArea::getHierarchy();
$summa = 0;
?>
<?php $form = ActiveForm::begin(); ?>
<div class="wh-document-form card card-body">
    <div class="row justify-content-center mb-2">
        <div class="col-md-6 col-sm-12 col-lg-12">
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'reg_date')->input('datetime-local', ['id' => 'datePicker']) ?>
                </div>

                <div class="col-3">
                    <?php $url = Url::to(['responsible-ajax']);?>
                    <?= $form->field($model, 'department_id')->widget(Select2::classname(), [
                        'data' => $model->getDepartmentListOutPuting(),
                        'options' => ['placeholder' => '','id'=>'department_id',],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'pluginEvents' => [
                            "select2:select" => "
                                function() { 
                                    var id = $(this).val();
                                    $('#department_area_id').prop(\"disabled\", false); 
                                    $.ajax({
                                        type:'GET',
                                        url:'{$url}',
                                        data:{id:id,type:0},
                                        success:function(response){
                                            $('#responsible').html('');
                                            if(response.status){
                                                $('#responseble').append('<option></option>');
                                                let option = response.responsible.map(function(item,index){
                                                        return ('<option value=\"'+item.id+'\">'+item.name+'</option>');
                                                    });
                                                    let newOption = option.join('');
                                                    $('#responsible').html($('#responsible').html() + newOption);
                                            }
                                        }
                                    })
                                }",
                        ],
                        'pluginOptions' => [
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        ]
                    ]); ?>
                </div>

                <div class="col-3">
                    <?= $form->field($model, 'department_area_id')->widget(Select2::classname(), [
                        'data' => $dep_area,
                        'disabled' => true,
                        'options' => ['placeholder' => '','id'=>'department_area_id'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'prompt' =>Yii::t('app', 'Select'),
                        ],
                        'pluginEvents' => [
                            "select2:select" => "
                                function() { 
                                    var id = $(this).val();
                                        $('#recipe').prop(\"disabled\", false); 
                                    
                                }",
                        ],
                    ]); ?>
                </div>
                <div class="col-3">
                    <?php $url = Url::to(['recipe-ajax']);?>

                    <?= $form->field($model, 'recipe_id')->widget(Select2::classname(), [
                        'data' => $model->getRecipesList(),
                        'disabled' => true,
                        'options' => ['placeholder' => '','id'=>'recipe',],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'pluginEvents' => [
                            "select2:select" => "
                                function() { 
                                    var id = $(this).val();
                                    let dep_area = $('#department_area_id').val();
                                    let dep = $('#department_id').val();
                                    $.ajax({
                                        type:'GET',
                                        url:'{$url}',
                                        data:{id:id,dep:dep,dep_area:dep_area},
                                        success:function(response){
                                            if(response.status){
                                                $('.unit-quantity').html(response.unit[0].name);
                                                let num = 0;
                                                let tbody = 0;
                                                let all_summa_price = 0;
                                                $('#table-production tbody').html('');
                                                (response.responsible).map(function(item,index,array){
                                                    all_summa_price += ((item.price*item.quantity).toFixed(2))*1;
                                                    tbody  += `
                                                    <tr class=\"multiple-input-list__item\">
                                                        <td><input type=\"hidden\" name=\"ProductionItem[`+num+`][recipe_production_id]\" class=\"form-control form-control-sm\" data-number=\"`+num+`\" value=\"`+item.id+`\"><span>`+ item.item_name +`</span></td>
                                                        <td class=\"quantity\">
                                                            <div class=\"form-group\">
                                                                <div class=\"input-group input-group-sm\">
                                                                    <input type=\"text\" name=\"ProductionItem[`+num+`][quantity]\" class=\"form-control\" value=\"`+item.quantity+`\" aria-describedby=\"basic-addon2\" />
                                                                    <div class=\"input-group-append\" style='width: 80px;' >
                                                                        <span class=\"input-group-text w-100 text-dark \" style='display:block;'>`+ item.unit_name +`</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class=\"wasting-percent\">
                                                            <div class=\"form-group\">
                                                                <div class=\"input-group input-group-sm\">
                                                                    <input type=\"text\" name=\"ProductionItem[`+num+`][wasting_percent]\" class=\"form-control\" value=\"`+item.wasting_percent+`\" aria-describedby=\"basic-addon2\" />
                                                                    <div class=\"input-group-append\">
                                                                        <span class=\"input-group-text\">
                                                                            <i class=\"la la-percent text-dark font-size-h3\"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class='final-quantity'>
                                                            <span class=\"input-group-text\">
                                                                `+ (item.quantity*1-item.quantity/100*item.wasting_percent).toFixed(2)+`&nbsp;
                                                            </span>
                                                            <span>`+item.unit_name+`</span>
                                                        </td>
                                                    </tr>    
                                                    `;
                                                    num +=1;
                                                });
                                                $('#total_ingredient_price').html(all_summa_price);
                                                $('#table-production').append(tbody);
                                            }
                                        }
                                    })
                                }",
                        ],
                        'pluginOptions' => [
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        ]
                    ]); ?>
                </div>

                <div class="col-3">
                    <label for="quantity"><?= Yii::t('app','Quantity') ?></label>
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <input type="number" name="Production[quantity]" id="quantity" class="form-control" value="<?=$model->quantity ?? ''?>" aria-describedby="basic-addon2" />
                            <div class="input-group-append">
                        <span class="input-group-text unit-quantity" >

                        </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'responsible')->widget(Select2::className(), [
                        'data'=> $responseble ?? NULL,
                        'options'=>[
                            'id' => 'responsible',
                            'placeholder'=>Yii::t('app', 'Responsible'),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]) ?>
                </div>
            </div>

        </div>
    </div>
</div>
<hr style="border: 2px solid springgreen">
<div class="wh-document-form card card-body">
    <div class="row justify-content-center mb-2">
        <div class="col-md-6 col-sm-12 col-lg-12">
            <div class="row">
                <h5 class="ml-4">Ingredients</h5>
                <div class="col-lg-12 border-5">
                        <table class="table table-striped table-bordered table-th-green text-center" id="table-production">
                            <thead>
                            <tr class="header-table-tabular" style="background-color: #0d3349; color: white;">
                                <th class="text-center"><?= Yii::t('app', 'Ingredient') ?></th>
                                <th class="text-center"><?= Yii::t('app', 'Input Quantity') ?></th>
                                <th class="text-center"><?= Yii::t('app', 'Wastage Percent') ?></th>
                                <th class="text-center"><?= Yii::t('app', 'Final Quantity') ?></th>
<!--                                <th class="text-center">--><?////= Yii::t('app', 'Price') ?><!--</th>-->
                            </tr>
                            </thead>
                            <tbody class="body-table-tabular" data-number-tbody="0">
                                <?php if(!empty($models)) : ?>
                                <?php $num = 0;?>
                                    <?php foreach ($models as $key): ?>
                                    <tr class="multiple-input-list__item">
                                        <td><input type="hidden" name="ProductionItem[<?=$num;?>][recipe_production_id]" class="form-control form-control-sm" data-number="<?=$num;?>" value="<?=$key['item_id']?>"><span><?=$key['name']?>></span></td>
                                        <td class="quantity">
                                            <div class="form-group">
                                                <div class="input-group input-group-sm">
                                                <input type="text" name="ProductionItem[<?=$num;?>][quantity]" class="form-control" value="<?=$key['quantity']?>" aria-describedby="basic-addon2" />
                                                <div class="input-group-append" style='width: 80px;' >
                                                    <span class="input-group-text w-100 text-dark " style='display:block;'><?=!empty($key['unit']) ? $key['unit'] : NULL?></span>
                                                </div>
                                            </div>
                                        </div>
                                        </td>
                                        <td class="wasting-percent">
                                            <div class="form-group">
                                                <div class="input-group input-group-sm">
                                                <input type="text" name="ProductionItem[<?=$num;?>][wasting_percent]" class="form-control" value="<?=$key['wasting_percent']?>" aria-describedby="basic-addon2" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-percent text-dark font-size-h3"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                        </td>
                                        <td class='final-quantity'>
                                            <span class="input-group-text" style="padding: 0.35rem 1rem;"><?=number_format($key['quantity']*(1-$key['wasting_percent']/100),2);?>
                                            </span>
                                            <span><?=$key['unit'] ?? NULL?></span>
                                        </td>
<!--                                        <td class="price">-->
<!--                                            <span class="input-group-text" data-price="--><?//=$key['price']?><!--">-->
<!--                                              --><?//=$key['quantity'] ?? NULL; $summa += $key['quantity'];?>
<!--                                            </span>-->
<!--                                        </td>-->
                                    </tr>
                                    <?php $num++; endforeach; ?>
                                <?php endif;?>
                            </tbody>
                        </table>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'wasted_quantity')->input('number') ?>
                </div>
                <div class="col-3">
                    <label for="additional_price_percent"><?= Yii::t('app','Additional Price Percent') ?></label>
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                        <input type="number" name="Production[additional_price_percent]" id="additional_price_percent" class="form-control" value="<?=$model->additional_price_percent ?? ''?>" aria-describedby="basic-addon2" />
                        <div class="input-group-append">
                        <span class="input-group-text">
                            <i class="la la-percent text-dark font-size-h3"></i>
                        </span>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-10" style="margin-left: 90%;">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success pull-right']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<?php ActiveForm::end(); ?>
<?php
$js = <<<JS
$('body').delegate('.multiple-input-list__item','keyup',function(e) {
  let quantity = $(this).find('.quantity').find('input').val()*1;
  let wasting_percent = $(this).find('.wasting-percent').find('input').val()*1;
  let price = $(this).find('.price').children('span').data('price')*1;
  let tr_price = (quantity*price).toFixed(2);
  let tr_wasting_percent = (quantity*1-quantity/100*wasting_percent).toFixed(2);
  let tr_table = $(this).parents('table').find('tbody');
  $(this).find('.final-quantity span:first-child').html(tr_wasting_percent);
  $(this).find('.price').children('span').html(tr_price);
  let tr_price_summa = 0;
  tr_table.map(function(index,item) {
        console.log($(item).find('.price'));
    tr_price_summa += $(item).find('.price').children('span').html()*1;
  });
  $('#total_ingredient_price').html(tr_price_summa.toFixed(2));
  console.log(tr_price_summa);
})

$('#production-wasted_quantity').keyup(function(e) {
  let quan = $("#quantity").val()*1;
  let wquan = $(this).val()*1;
  if (wquan > quan){
      $('#production-wasted_quantity').val(quan);
  }
  if( wquan < 0 ){
      $('#production-wasted_quantity').val(0.00);
  }
});
var now = new Date();
now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
$('#datePicker').val(now.toISOString().slice(0,16));
JS;
$this->registerJs($js);

?>
