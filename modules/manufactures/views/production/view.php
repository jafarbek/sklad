<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\manufactures\model\Production */
/* @var $models app\modules\manufactures\model\ProductionItem */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Productions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
//echo "<pre>";
//    print_r($models);
//echo "</pre>";exit();
?>

    <div class="tovar-form">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header mt-5">
                <div class="col-xs-8">
                    <h4><?=Yii::t('app','Document Number:'); ?> <?= Html::encode($this->title) ?></h4>
                </div>
                <div class="col-xs-4 text-right">
                    <a href="<?= \yii\helpers\Url::to(['production/index']) ?>"
                       class="btn btn-sm btn-outline-primary font-weight-bolder">
                        <i class="la la-backspace"></i><?= Yii::t('app', 'Назад'); ?>
                    </a>
                </div>
                <?php if ($model['status'] == '1'): ?>
                    <div class="col-sm-12 mb-4 mt-4">
                        <a class=" btn btn-sm btn-success" href="<?=\yii\helpers\Url::to(['production/save-and-finish', 'id' => $model['id']])?>"><?=Yii::t('app', 'Save And Finish'); ?></a>
                        <a class="ml-2 btn btn-sm btn-outline-primary" href="<?=\yii\helpers\Url::to(['production/update', 'id' => $model['id']])?>"><?=Yii::t('app', 'Update'); ?></a>
                        <?php
                        echo Html::a(Yii::t('app', 'Delete'), \yii\helpers\Url::to(['delete', 'id' => $model['id']]), [
                            'class' => "ml-2 btn btn-sm btn-outline-danger",
                            'data' => [
                                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                                'method' => 'POST',
                            ],
                        ]);
                        ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="card-body">
                <table class="production-table">
                    <thead >
                    <tr>
                        <th><b><?=Yii::t('app', 'Recipe:'); ?></b> <span><?=!empty($model['item_name']) ? $model['item_name'] : NULL;?></span></th>
                        <th><b><?=Yii::t('app', 'Reg date:')?></b> <span><?=$model['reg_date']; ?></span></th>
                    </tr>
                    <tr>
                        <th><b><?=Yii::t('app', 'Department:')?></b> <span><?=$model['dep_name']; ?></span></th>
                        <th><b><?=Yii::t('app', 'Department Area:')?></b><span><?=!empty($model['dep_area_name']) ? $model['dep_area_name']: NULL ;?></span></th>
                    </tr>
                    <tr>
                        <th><b><?=Yii::t('app', 'Quantity:'); ?></b> <span><?=!empty($model['quantity']) ? $model['quantity'] : NULL ;?></span><span><?=!empty($model['unit']) ? $model['unit'] : NULL ;?></span></th>
                        <th><b><?=Yii::t('app', 'Wasted Quantity:')?></b> <span><?=!empty($model['wasted_quantity']) ? $model['wasted_quantity'] : NULL ;?></span></th>
                    </tr>
                    </thead>
                </table>
                <table class="models-table">
                    <thead>
                    <tr class="models-tr">
                        <th>#</th>
                        <th><?=Yii::t('app', 'Item'); ?></th>
                        <th><?=Yii::t('app', 'To Department Area'); ?></th>
                        <th><?=Yii::t('app', 'Quantity')?></th>
                        <th><?=Yii::t('app', 'Wasting percent')?></th>
                        <th><?=Yii::t('app', 'Jami:')?></th>
                    </tr>
                    </thead>
                    <tbody class="models-tbody">
                    <?php $i = 1;?>
                    <?php foreach ($models as $key) : ?>
                        <tr class="item-tbody-tr">
                            <td><?=$i++; ?></td>
                            <td><span><?=$key['name'] ?? NULL;?></span></td>
                            <td><span><?=$model['dep_area_name'] ?? NULL;?></span></td>
                            <td class="quantity"><span><?=$key['quantity'] ?? NULL;?></span>&nbsp;&nbsp;&nbsp;<span><?=$key['unit'] ?? NULL;?></span></td>
                            <td><span><?=$key['wasting_percent'] ?? NULL;?></span></td>
                            <td><span><?=($key['quantity'] * (1-$key['wasting_percent']/100)) ?? NULL;?></span></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
<?php
$css = <<< CSS
    .models-table {
        margin-top: 20px;
        font-family: Arial;
    }
    .models-table th {
        border: 1px solid gray;
    }
    .production-table th {
        border: 1px solid slategrey;
        padding-left: 10px;
    }
    .models-tr th {
        padding-left: 5px;
        background-color: #0c5460;
        color: whitesmoke;
    }
    .models-tbody-tr td {
        border: 1px solid black;
        padding-left: 5px;
    }
    .footer-out td {
        border: 1px solid slategrey;
        padding-right: 5px;
        color: #0a73bb;
    }
    th span {
        color: gray;
        margin-left: 8px;
    }
    tr {
      height: 30px;
    }
    table {
      width: 100%;
    }
    hr {
        background-color: #0c5460;
        height: 2px;
    }
CSS;
$this->registerCss($css);