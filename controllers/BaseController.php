<?php

namespace app\controllers;

use app\models\SystemStructura;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * summary
 */
class BaseController extends Controller

{
    public $info;

    public function __construct($id, $module, $config = [])
    {
        $this->info = SystemStructura::find()->asArray()->orderBy(['id' => SORT_DESC])->limit(1)->one();
        parent::__construct($id, $module, $config);
        return $this->info;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (Yii::$app->authManager->getPermission(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
            if (!Yii::$app->user->can(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
                throw new ForbiddenHttpException(Yii::t('app', 'Access denied'));
            }
        }

        return parent::beforeAction($action);
    }
}

?>