<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%item_balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%item}}`
 * - `{{%department}}`
 * - `{{%department_area}}`
 * - `{{%document}}`
 * - `{{%document_item}}`
 * - `{{%references_type}}`
 */
class m200708_195332_create_item_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item_balance}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'lot' => $this->string(50),
            'quantity' => $this->decimal(20,6),
            'inventory' => $this->decimal(20,6),
            'reg_date' => $this->datetime(),
            'department_id' => $this->integer(),
            'department_area_id' => $this->integer(),
            'document_id' => $this->integer(),
            'document_item_id' => $this->integer(),
            'price' => $this->decimal(20,4),
            'price_currency' => $this->integer(),
            'status' => $this->smallInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `item_id`
        $this->createIndex(
            '{{%idx-item_balance-item_id}}',
            '{{%item_balance}}',
            'item_id'
        );

        // add foreign key for table `{{%item}}`
        $this->addForeignKey(
            '{{%fk-item_balance-item_id}}',
            '{{%item_balance}}',
            'item_id',
            '{{%item}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-item_balance-department_id}}',
            '{{%item_balance}}',
            'department_id'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-item_balance-department_id}}',
            '{{%item_balance}}',
            'department_id',
            '{{%department}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `department_area_id`
        $this->createIndex(
            '{{%idx-item_balance-department_area_id}}',
            '{{%item_balance}}',
            'department_area_id'
        );

        // add foreign key for table `{{%department_area}}`
        $this->addForeignKey(
            '{{%fk-item_balance-department_area_id}}',
            '{{%item_balance}}',
            'department_area_id',
            '{{%department_area}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `document_id`
        $this->createIndex(
            '{{%idx-item_balance-document_id}}',
            '{{%item_balance}}',
            'document_id'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-item_balance-document_id}}',
            '{{%item_balance}}',
            'document_id',
            '{{%document}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `document_item_id`
        $this->createIndex(
            '{{%idx-item_balance-document_item_id}}',
            '{{%item_balance}}',
            'document_item_id'
        );

        // add foreign key for table `{{%document_item}}`
        $this->addForeignKey(
            '{{%fk-item_balance-document_item_id}}',
            '{{%item_balance}}',
            'document_item_id',
            '{{%document_item}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `price_currency`
        $this->createIndex(
            '{{%idx-item_balance-price_currency}}',
            '{{%item_balance}}',
            'price_currency'
        );

        // add foreign key for table `{{%references_type}}`
        $this->addForeignKey(
            '{{%fk-item_balance-price_currency}}',
            '{{%item_balance}}',
            'price_currency',
            '{{%references_type}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%item}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-item_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            '{{%idx-item_balance-item_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-department_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-item_balance-department_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%department_area}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-department_area_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `department_area_id`
        $this->dropIndex(
            '{{%idx-item_balance-department_area_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-document_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `document_id`
        $this->dropIndex(
            '{{%idx-item_balance-document_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%document_item}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-document_item_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `document_item_id`
        $this->dropIndex(
            '{{%idx-item_balance-document_item_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%references_type}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-price_currency}}',
            '{{%item_balance}}'
        );

        // drops index for column `price_currency`
        $this->dropIndex(
            '{{%idx-item_balance-price_currency}}',
            '{{%item_balance}}'
        );

        $this->dropTable('{{%item_balance}}');
    }
}
