<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%recipe}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%item}}`
 */
class m200724_144901_create_recipe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%recipe}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'wasting_percent' => $this->decimal(10,2),
            'quantity' => $this->decimal(20,3),
            'instruction' => $this->text(),
            'status' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `item_id`
        $this->createIndex(
            '{{%idx-recipe-item_id}}',
            '{{%recipe}}',
            'item_id'
        );

        // add foreign key for table `{{%item}}`
        $this->addForeignKey(
            '{{%fk-recipe-item_id}}',
            '{{%recipe}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%item}}`
        $this->dropForeignKey(
            '{{%fk-recipe-item_id}}',
            '{{%recipe}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            '{{%idx-recipe-item_id}}',
            '{{%recipe}}'
        );

        $this->dropTable('{{%recipe}}');
    }
}
