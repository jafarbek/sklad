<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%department}}`
 * - `{{%contragent}}`
 * - `{{%document}}`
 * - `{{%references}}`
 * - `{{%users}}`
 */
class m200727_101626_create_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'department_id' => $this->integer()->notNull(),
            'contragent_id' => $this->integer()->notNull(),
            'reg_date' => $this->dateTime(),
            'status' => $this->smallInteger(),
            'type' => $this->smallInteger(),
            'total_amount' => $this->decimal(20,4),
            'document_id' => $this->integer()->notNull(),
            'payment_status' => $this->smallInteger(),
            'expenses_category_id' => $this->integer()->notNull(),
            'expenses_for' => $this->integer()->notNull(),
            'add_info' => $this->text(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-transaction-department_id}}',
            '{{%transaction}}',
            'department_id'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-transaction-department_id}}',
            '{{%transaction}}',
            'department_id',
            '{{%department}}',
            'id',
            'CASCADE'
        );

        // creates index for column `contragent_id`
        $this->createIndex(
            '{{%idx-transaction-contragent_id}}',
            '{{%transaction}}',
            'contragent_id'
        );

        // add foreign key for table `{{%contragent}}`
        $this->addForeignKey(
            '{{%fk-transaction-contragent_id}}',
            '{{%transaction}}',
            'contragent_id',
            '{{%contragent}}',
            'id',
            'CASCADE'
        );

        // creates index for column `document_id`
        $this->createIndex(
            '{{%idx-transaction-document_id}}',
            '{{%transaction}}',
            'document_id'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-transaction-document_id}}',
            '{{%transaction}}',
            'document_id',
            '{{%document}}',
            'id',
            'CASCADE'
        );

        // creates index for column `expenses_category_id`
        $this->createIndex(
            '{{%idx-transaction-expenses_category_id}}',
            '{{%transaction}}',
            'expenses_category_id'
        );

        // add foreign key for table `{{%references}}`
        $this->addForeignKey(
            '{{%fk-transaction-expenses_category_id}}',
            '{{%transaction}}',
            'expenses_category_id',
            '{{%references}}',
            'id',
            'CASCADE'
        );

        // creates index for column `expenses_for`
        $this->createIndex(
            '{{%idx-transaction-expenses_for}}',
            '{{%transaction}}',
            'expenses_for'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-transaction-expenses_for}}',
            '{{%transaction}}',
            'expenses_for',
            '{{%users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-transaction-department_id}}',
            '{{%transaction}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-transaction-department_id}}',
            '{{%transaction}}'
        );

        // drops foreign key for table `{{%contragent}}`
        $this->dropForeignKey(
            '{{%fk-transaction-contragent_id}}',
            '{{%transaction}}'
        );

        // drops index for column `contragent_id`
        $this->dropIndex(
            '{{%idx-transaction-contragent_id}}',
            '{{%transaction}}'
        );

        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-transaction-document_id}}',
            '{{%transaction}}'
        );

        // drops index for column `document_id`
        $this->dropIndex(
            '{{%idx-transaction-document_id}}',
            '{{%transaction}}'
        );

        // drops foreign key for table `{{%references}}`
        $this->dropForeignKey(
            '{{%fk-transaction-expenses_category_id}}',
            '{{%transaction}}'
        );

        // drops index for column `expenses_category_id`
        $this->dropIndex(
            '{{%idx-transaction-expenses_category_id}}',
            '{{%transaction}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-transaction-expenses_for}}',
            '{{%transaction}}'
        );

        // drops index for column `expenses_for`
        $this->dropIndex(
            '{{%idx-transaction-expenses_for}}',
            '{{%transaction}}'
        );

        $this->dropTable('{{%transaction}}');
    }
}
