<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%production}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%recipe}}`
 * - `{{%department}}`
 * - `{{%department_area}}`
 * - `{{%users}}`
 */
class m200724_161607_create_production_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%production}}', [
            'id' => $this->primaryKey(),
            'recipe_id' => $this->integer(),
            'reg_date' => $this->date(),
            'quantity' => $this->decimal(20,3),
            'wasted_quantity' => $this->decimal(20,3),
            'department_id' => $this->integer(),
            'department_area_id' => $this->integer(),
            'responsible' => $this->integer(),
            'price' => $this->decimal(20, 3),
            'additional_price_percent' => $this->decimal(10, 2),
            'status' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `recipe_id`
        $this->createIndex(
            '{{%idx-production-recipe_id}}',
            '{{%production}}',
            'recipe_id'
        );

        // add foreign key for table `{{%recipe}}`
        $this->addForeignKey(
            '{{%fk-production-recipe_id}}',
            '{{%production}}',
            'recipe_id',
            '{{%recipe}}',
            'id',
            'CASCADE'
        );

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-production-department_id}}',
            '{{%production}}',
            'department_id'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-production-department_id}}',
            '{{%production}}',
            'department_id',
            '{{%department}}',
            'id',
            'CASCADE'
        );

        // creates index for column `department_area_id`
        $this->createIndex(
            '{{%idx-production-department_area_id}}',
            '{{%production}}',
            'department_area_id'
        );

        // add foreign key for table `{{%department_area}}`
        $this->addForeignKey(
            '{{%fk-production-department_area_id}}',
            '{{%production}}',
            'department_area_id',
            '{{%department_area}}',
            'id',
            'CASCADE'
        );

        // creates index for column `responsible`
        $this->createIndex(
            '{{%idx-production-responsible}}',
            '{{%production}}',
            'responsible'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-production-responsible}}',
            '{{%production}}',
            'responsible',
            '{{%users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%recipe}}`
        $this->dropForeignKey(
            '{{%fk-production-recipe_id}}',
            '{{%production}}'
        );

        // drops index for column `recipe_id`
        $this->dropIndex(
            '{{%idx-production-recipe_id}}',
            '{{%production}}'
        );

        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-production-department_id}}',
            '{{%production}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-production-department_id}}',
            '{{%production}}'
        );

        // drops foreign key for table `{{%department_area}}`
        $this->dropForeignKey(
            '{{%fk-production-department_area_id}}',
            '{{%production}}'
        );

        // drops index for column `department_area_id`
        $this->dropIndex(
            '{{%idx-production-department_area_id}}',
            '{{%production}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-production-responsible}}',
            '{{%production}}'
        );

        // drops index for column `responsible`
        $this->dropIndex(
            '{{%idx-production-responsible}}',
            '{{%production}}'
        );

        $this->dropTable('{{%production}}');
    }
}
