<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%production_item}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%production}}`
 * - `{{%rcipe_ingredient}}`
 */
class m200724_161614_create_production_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%production_item}}', [
            'id' => $this->primaryKey(),
            'production_id' => $this->integer(),
            'recipe_ingredient_id' => $this->integer(),
            'quantity' => $this->decimal(20, 3),
            'wasting_percent' => $this->decimal(12, 2),
            'status' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `production_id`
        $this->createIndex(
            '{{%idx-production_item-production_id}}',
            '{{%production_item}}',
            'production_id'
        );

        // add foreign key for table `{{%production}}`
        $this->addForeignKey(
            '{{%fk-production_item-production_id}}',
            '{{%production_item}}',
            'production_id',
            '{{%production}}',
            'id',
            'CASCADE'
        );

        // creates index for column `recipe_ingredient_id`
        $this->createIndex(
            '{{%idx-production_item-recipe_ingredient_id}}',
            '{{%production_item}}',
            'recipe_ingredient_id'
        );

        // add foreign key for table `{{%recipe_ingredient}}`
        $this->addForeignKey(
            '{{%fk-production_item-recipe_ingredient_id}}',
            '{{%production_item}}',
            'recipe_ingredient_id',
            '{{%recipe_ingredient}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%production}}`
        $this->dropForeignKey(
            '{{%fk-production_item-production_id}}',
            '{{%production_item}}'
        );

        // drops index for column `production_id`
        $this->dropIndex(
            '{{%idx-production_item-production_id}}',
            '{{%production_item}}'
        );

        // drops foreign key for table `{{%rcipe_ingredient}}`
        $this->dropForeignKey(
            '{{%fk-production_item-recipe_ingredient_id}}',
            '{{%production_item}}'
        );

        // drops index for column `recipe_ingredient_id`
        $this->dropIndex(
            '{{%idx-production_item-recipe_ingredient_id}}',
            '{{%production_item}}'
        );

        $this->dropTable('{{%production_item}}');
    }
}
