<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%recipe_ingredient}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%recipe}}`
 * - `{{%item}}`
 */
class m200724_145345_create_recipe_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%recipe_ingredient}}', [
            'id' => $this->primaryKey(),
            'recipe_id' => $this->integer(),
            'item_id' => $this->integer(),
            'wasting_percent' => $this->decimal(10, 2),
            'quantity' => $this->decimal(20,3),
            'status' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `recipe_id`
        $this->createIndex(
            '{{%idx-recipe_ingredient-recipe_id}}',
            '{{%recipe_ingredient}}',
            'recipe_id'
        );

        // add foreign key for table `{{%recipe}}`
        $this->addForeignKey(
            '{{%fk-recipe_ingredient-recipe_id}}',
            '{{%recipe_ingredient}}',
            'recipe_id',
            '{{%recipe}}',
            'id',
            'CASCADE'
        );

        // creates index for column `item_id`
        $this->createIndex(
            '{{%idx-recipe_ingredient-item_id}}',
            '{{%recipe_ingredient}}',
            'item_id'
        );

        // add foreign key for table `{{%item}}`
        $this->addForeignKey(
            '{{%fk-recipe_ingredient-item_id}}',
            '{{%recipe_ingredient}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%recipe}}`
        $this->dropForeignKey(
            '{{%fk-recipe_ingredient-recipe_id}}',
            '{{%recipe_ingredient}}'
        );

        // drops index for column `recipe_id`
        $this->dropIndex(
            '{{%idx-recipe_ingredient-recipe_id}}',
            '{{%recipe_ingredient}}'
        );

        // drops foreign key for table `{{%item}}`
        $this->dropForeignKey(
            '{{%fk-recipe_ingredient-item_id}}',
            '{{%recipe_ingredient}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            '{{%idx-recipe_ingredient-item_id}}',
            '{{%recipe_ingredient}}'
        );

        $this->dropTable('{{%recipe_ingredient}}');
    }
}
