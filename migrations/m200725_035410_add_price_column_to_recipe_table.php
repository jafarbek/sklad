<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%recipe}}`.
 */
class m200725_035410_add_price_column_to_recipe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%recipe}}', 'price', $this->decimal(20, 3));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%recipe}}', 'price');
    }
}
