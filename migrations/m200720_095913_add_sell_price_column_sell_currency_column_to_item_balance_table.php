<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%item_balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%references}}`
 */
class m200720_095913_add_sell_price_column_sell_currency_column_to_item_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%item_balance}}', 'sell_price', $this->decimal(20, 4));
        $this->addColumn('{{%item_balance}}', 'sell_currency', $this->integer());

        // creates index for column `sell_currency`
        $this->createIndex(
            '{{%idx-item_balance-sell_currency}}',
            '{{%item_balance}}',
            'sell_currency'
        );

        // add foreign key for table `{{%references}}`
        $this->addForeignKey(
            '{{%fk-item_balance-sell_currency}}',
            '{{%item_balance}}',
            'sell_currency',
            '{{%references}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%references}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-sell_currency}}',
            '{{%item_balance}}'
        );

        // drops index for column `sell_currency`
        $this->dropIndex(
            '{{%idx-item_balance-sell_currency}}',
            '{{%item_balance}}'
        );

        $this->dropColumn('{{%item_balance}}', 'sell_price');
        $this->dropColumn('{{%item_balance}}', 'sell_currency');
    }
}
