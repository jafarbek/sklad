<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document_doc_number_alter_unique}}`.
 */
class m200714_110740_alter_document_doc_number_alter_unique_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('document', 'doc_number', $this->string(255)->notNull()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('document', 'doc_number', $this->string(255)->notNull());
    }
}
