<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%item_balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%item_balance}}`
 */
class m200724_170437_add_production_id_column_to_item_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%item_balance}}', 'production_id', $this->integer());

        // creates index for column `production_id`
        $this->createIndex(
            '{{%idx-item_balance-production_id}}',
            '{{%item_balance}}',
            'production_id'
        );

        // add foreign key for table `{{%item_balance}}`
        $this->addForeignKey(
            '{{%fk-item_balance-production_id}}',
            '{{%item_balance}}',
            'production_id',
            '{{%production}}',
            'id',

            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%item_balance}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-production_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `production_id`
        $this->dropIndex(
            '{{%idx-item_balance-production_id}}',
            '{{%item_balance}}'
        );

        $this->dropColumn('{{%item_balance}}', 'production_id');
    }
}
