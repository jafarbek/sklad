<?php

use app\components\MenuActivate;
use yii\helpers\Url;
use app\components\PermissionHelper as P;


$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$module = Yii::$app->controller->module->id;

$slug = $this->context->slug ?? null;
?>

<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper" >
    <!--begin::Menu Container-->
    <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1"
         data-menu-dropdown-timeout="500">
        <!--begin::Menu Nav-->
        <ul class="menu-nav main_li">


            <li class="menu-item menu-item-submenu <?=MenuActivate::modules('admin', 'structure')?> " aria-haspopup="true" data-menu-toggle="hover">
                <a href="javascript:" class="menu-link menu-toggle left_breadcrumb">
                    <i class="menu-icon flaticon2-browser-1"></i>
                    <span class="menu-text"><?=Yii::t('app','Organization Structure');?></span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">

                        <li class="menu-item <?=MenuActivate::controllers('department')?> <?php if(!P::can('department/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/structure/department/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?=Yii::t('app', 'Department');?></span>
                            </a>
                        </li>

                        <li class="menu-item <?=MenuActivate::controllers('users')?> <?php if(!P::can('users/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/admin/users/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?=Yii::t('app', 'Users');?></span>
                            </a>
                        </li>

                        <li class="menu-item <?=MenuActivate::controllers('user-department')?> <?php if(!P::can('user-department/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/admin/user-department/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?=Yii::t('app', 'User department');?></span>
                            </a>
                        </li>




                        <li class="menu-item <?=MenuActivate::controllers('auth-item')?> <?php if(!P::can('auth-item/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/admin/auth-item/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?=Yii::t('app', 'Rules')?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="menu-item menu-item-submenu <?=MenuActivate::modules('warehouse')?> " aria-haspopup="true" data-menu-toggle="hover">
                <a href="javascript:" class="menu-link menu-toggle left_breadcrumb">
                    <i class="menu-icon flaticon-home"></i>
                    <span class="menu-text"><?= Yii::t('app', 'Warehouse management') ?></span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="menu-submenu">

                    <ul class="menu-subnav">

                        <li class="menu-item menu-item-submenu <?=MenuActivate::controllers('item-category', 'item')?> " aria-haspopup="true"
                            data-menu-toggle="hover">
                            <a href="javascript:" class="menu-link menu-toggle">
                                <i class="menu-icon flaticon2-open-box"></i>
                                <span class="menu-text"><?= Yii::t('app', 'Products'); ?></span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="menu-submenu">
                                <i class="menu-arrow"></i>
                                <ul class="menu-subnav">


                                    <li class="menu-item <?=MenuActivate::controllers('item-category')?> <?php if(!P::can('item-category/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                                        <a href="<?= Url::to(['/warehouse/item-category/index']); ?>"
                                           class="menu-link">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text"><?= Yii::t('app', 'Product сategories') ?></span>
                                        </a>
                                    </li>


                                    <li class="menu-item <?=MenuActivate::controllers('item')?> <?php if(!P::can('item/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                                        <a href="<?= Url::to(['/warehouse/item/index']); ?>"
                                           class="menu-link">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text"><?= Yii::t('app', 'Products list') ?></span>
                                        </a>
                                    </li>


                                </ul>
                            </div>
                        </li>

                        <li class="menu-item <?=MenuActivate::controllers('department-area')?> <?php if(!P::can('department-area/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/warehouse/department-area/index']); ?>"
                               class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Department Area') ?></span>
                            </a>
                        </li>
                        <li class="menu-item <?=MenuActivate::controllersSlug($slug,'document/incoming')?> <?php if(!P::can('document/incoming/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/warehouse/document/index', 'slug' => 'incoming']); ?>"
                               class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Incoming') ?></span>
                            </a>
                        </li>

                        <li class="menu-item <?=MenuActivate::controllersSlug($slug,'document/outgoing')?> <?php if(!P::can('document/outgoing/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/warehouse/document/index', 'slug' => 'outgoing']); ?>"
                               class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Outgoing') ?></span>
                            </a>
                        </li>



                        <li class="menu-item <?=MenuActivate::controllersSlug($slug,'document/moving')?> <?php if(!P::can('document/moving/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/warehouse/document/index', 'slug' => 'moving']); ?>"
                               class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Moving') ?></span>
                            </a>
                        </li>

                        <li class="menu-item <?=MenuActivate::controllers('item-balance')?>   <?php if(!P::can('item-balance/index')) {echo 'offcanvas';} ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/warehouse/item-balance/index']); ?>" class="menu-link  ">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Report') ?></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>
            <li class="menu-item menu-item-submenu <?=MenuActivate::modules('Manufacture')?> " aria-haspopup="true" data-menu-toggle="hover">
                <a href="javascript:" class="menu-link menu-toggle left_breadcrumb">
                    <i class="menu-icon flaticon-home"></i>
                    <span class="menu-text"><?= Yii::t('app', 'Manufacture') ?></span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="menu-submenu">
                    <ul class="menu-subnav">
                        <li class="menu-item <?=MenuActivate::controllers('test')?> <?php if(!P::can('default/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/manufactures/default/index']); ?>"
                               class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Department Area') ?></span>
                            </a>
                        </li>
                        <li class="menu-item " aria-haspopup="true"><?/*=MenuActivate::controllers('test')*/?><?php /*if(!P::can('default/index')) { echo 'offcanvas'; } */?>
                            <a href="<?= Url::to(['/manufactures/production/index']); ?>"
                               class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Production') ?></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="menu-item menu-item-submenu <?=MenuActivate::modules( 'manuals')?> " aria-haspopup="true" data-menu-toggle="hover">
                <a href="javascript:" class="menu-link menu-toggle left_breadcrumb">
                    <i class="menu-icon flaticon2-layers-2"></i>
                    <span class="menu-text"><?= Yii::t('app', 'All references') ?></span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">





                        <li class="menu-item <?=MenuActivate::controllers('references-type')?> <?=MenuActivate::controllers('references')?> <?php if(!P::can('references-type/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/manuals/references-type/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'References') ?></span>
                            </a>
                        </li>


                        <li class="menu-item  <?=MenuActivate::controllers('bank')?> <?php if(!P::can('bank/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/manuals/bank/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Banks') ?></span>
                            </a>
                        </li>

                        <li class="menu-item <?=MenuActivate::controllers('regions')?> <?php if(!P::can('regions/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/manuals/regions/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Regions') ?></span>
                            </a>
                        </li>


                        <li class="menu-item <?=MenuActivate::controllers('countries')?> <?php if(!P::can('countries/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/manuals/countries/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Countries') ?></span>
                            </a>
                        </li>



                        <li class="menu-item <?=MenuActivate::controllers('license')?> <?php if(!P::can('license/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/manuals/license/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'License') ?></span>
                            </a>
                        </li>

                        <li class="menu-item  <?=MenuActivate::controllers('exchange-rate')?> <?php if(!P::can('exchange-rate/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/manuals/exchange-rate/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Exchange Rate') ?></span>
                            </a>
                        </li>

                        <li class="menu-item <?=MenuActivate::controllers('contragent')?> <?php if(!P::can('contragent/index')) { echo 'offcanvas'; } ?>" aria-haspopup="true">
                            <a href="<?= Url::to(['/manuals/contragent/index']); ?>" class="menu-link">
                                <i class="menu-bullet menu-bullet-dot">
                                    <span></span>
                                </i>
                                <span class="menu-text"><?= Yii::t('app', 'Contragent') ?></span>
                            </a>
                        </li>


                    </ul>
                </div>
            </li>

        </ul>
        <!--end::Menu Nav-->
    </div>
    <!--end::Menu Container-->
</div>