<div class="d-flex flex-column flex-root">
    <!--begin::Error-->

    <div class=" flex-row-fluid flex-column bgi-size-cover bgi-position-center bgi-no-repeat p-25" style="background-image: url(<?=\yii\helpers\Url::base()?>/asset/media/error/bg1.jpg);">
        <!--begin::Content-->
        <h1 class="font-weight-boldest text-dark-75 mt-15" style="font-size: 10rem">403</h1>
        <p class="font-size-h3 text-dark-100 text-danger ml-5 font-weight-normal"><?=Yii::t('app', 'Ruxsat mavjud emas!')?></p>
        <!--end::Content-->
    </div>
    <!--end::Error-->
</div>