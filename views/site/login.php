<?php
use yii\helpers\Url;

$this->title = 'Sign In';
?>
<div class="d-flex flex-column flex-root">
    <div class="login login-5 login-signin-on d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid" style="background-image: url(<?=Url::base(); ?>/asset/media/bg/bg-2.jpg);">
            <div class="login-form text-center text-white p-7 position-relative overflow-hidden">
                <div class="d-flex flex-center mb-15">
                    <img src="<?=Url::base().'/img/'.$info->img_login; ?>" class="max-h-75px" alt="<?=$info->img_login; ?>" />
                </div>
                <div class="login-signin">
                    <div class="mb-20">
                        <h3 class="opacity-40 font-weight-normal"><?=$info->title; ?></h3>
                        <p class="opacity-40"><?=$info->content; ?></p>
                    </div>
                    <?php $form = \yii\bootstrap\ActiveForm::begin([
                        'options' => [
                            'id' => 'kt_login_signin_form',
                            'class' => 'form',
                        ],
                    ]); ?>
                    <div class="form-group">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8', 'autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Username')])->label(false); ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8', 'placeholder' => Yii::t('app', 'Password')])->label(false); ?>
                    </div>
                    <div class="form-group d-flex flex-wrap justify-content-between align-items-center px-8 opacity-60">
                      <label class="checkbox checkbox-outline checkbox-white text-white m-0" style="color: white!important;">
                            <input type="checkbox" name="remember"><?=Yii::t('app', 'Remember me')?>
                            <span></span>
                        </label>
                    </div>
                    <div class="form-group text-center mt-10">
                        <?= yii\helpers\Html::submitButton(Yii::t('app', 'Sign in'), ['class' => 'btn btn-primary', 'name' => 'login-button', 'id' => 'kt_login_signin_submit', 'class' => 'btn btn-pill btn-outline-primary opacity-90 px-20 py-3']) ?>
                    </div>
                    <?php \yii\bootstrap\ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>